Stokes Drifter
=============

This git repository contains work on the Stokes drifter, a device that 
will be released into the ocean to measure the Stokes drift.

What is git?
============

git is a distributed version control system. Version control tracks 
changes made to files. This means that any changes can be rolled back,
if necessary, and we have a record of who did what. For a brief 
tutorial of git, visit https://try.github.io/. Graphical frontends,
such as [sourcetree](https://www.sourcetreeapp.com/) are available if
you feel that would be more comfortable.

What exactly is here?
=====================

## README.md
This very file. It contains an introduction to the project and is
written in markdown.

## old
This directory contains what the previous team accomplished.

## docs
Documentation

### contacts.txt
A list of people involved in the project

### parts.md
A description of parts we will need

## brd
Circuit board designs

### lib
KiCAD libraries for the circuit boards.

### prelim
The first attempt a board. It has issues.

### proto_brd1
This is the first board that we are going to get built. It has
extra headers and though-hole parts where we could use them.
All holes are 40 mils, and traces are 30 mils.

Google Docs
===========  
Some documents are on Google Docs

* [Needs assessment](https://docs.google.com/document/d/1HGf9x9BkGAQYWP_bf07ZOLgIPubbl0NUlqfR0x1ctJ0/edit)
