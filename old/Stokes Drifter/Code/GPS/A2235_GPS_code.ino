/*
  6-12-12
  Aaron Weiss
  SparkFun Electronics, Beerware

  Example GPS Parser based off of arduiniana.org TinyGPS examples.

  Parses NMEA sentences from an LS2060 running at 9600bps into readable
  values for latitude, longitude, elevation, date, time, course, and
  speed. Use 115200 baud for your serial port baud rate

  Uses the NewSoftSerial library for serial communication with your GPS,
  so connect your GPS TX and RX pin to any digital pin on the Arduino,
  just be sure to define which pins you are using on the Arduino to
  communicate with the GPS module.

*/
//#include <SoftwareSerial.h>
#include <TinyGPS.h>
//#include <TinyGPS++.h>\



#define rxPin 19
#define txPin 18

// This is the serial rate for your terminal program. It must be this
// fast because we need to print everything before a new sentence
// comes in. If you slow it down, the messages might not be valid and
// you will likely get checksum errors.
// Set this value equal to the baud rate of your terminal program
#define TERMBAUD  115200

// Set this value equal to the baud rate of your GPS
#define GPSBAUD  4800

//ON_OFF TOGGLE connected to WAKEUP for self start..//
//pinMode(8,OUTPUT); //sets pin 8 as an output for the ON_OFF toggle
//digitalWrite(8, LOW); //sets pin 8 low, as a default.


// Create an instance of the TinyGPS object
TinyGPS gps;
// Initialize the NewSoftSerial library to the pins you defined above

//SoftwareSerial uart_gps(RXPIN, TXPIN);

// This is where you declare prototypes for the functions that will be
// using the TinyGPS library.
void getgps(TinyGPS &gps);
void printFloat(double f, int digits = 2);

// In the setup function, you need to initialize two serial ports; the
// standard hardware serial port (Serial()) to communicate with your
// terminal program an another serial port (NewSoftSerial()) for your
// GPS.
void setup()
{
  Serial.begin(TERMBAUD);
  Serial1.begin(4800);

  delay(1000);

  // pinMode(rxPin, INPUT);
  // pinMode(txPin, OUTPUT);

  // Sets baud rate of your terminal program
  Serial.print("Testing TinyGPS library v. ");
  Serial.println(TinyGPS::library_version());
  Serial.println();
  Serial.print("Sizeof(gpsobject) = ");
  Serial.println(sizeof(TinyGPS));
  Serial.println();
}

// This is the main loop of the code. All it does is check for data on
// the RX pin of the ardiuno, makes sure the data is valid NMEA sentences,
// then jumps to the getgps() function.

void loop()
{
  bool newdata = false;
  unsigned long start = millis();

  // Every 5 seconds we print an update

  delay(5000);
  if (Serial1.available()) {
    char c = Serial1.read();
    // Serial.print(c);  // uncomment to see raw GPS data
    if (gps.encode(c)) {
      newdata = true;
      // break;  // uncomment to print new data immediately!

    }
  }

  if (newdata) {
    Serial.println("Acquired Data");
    Serial.println("-------------");
    getgps(gps);
    Serial.println("-------------");
    Serial.println();
  }
}
  // The getgps function will get and print the values we want.
  void getgps(TinyGPS & gps)
  {
    // To get all of the data into varialbes that you can use in your code,
    // all you need to do is define variables and query the object for the
    // data. To see the complete list of functions see keywords.txt file in
    // the TinyGPS and NewSoftSerial libs.

    // Define the variables that will be used
    float latitude, longitude;
    // Then call this function
    gps.f_get_position(&latitude, &longitude);
    // You can now print variables latitude and longitude
    Serial.print("Lat/Long: ");
    Serial.print(latitude, 5);
    Serial.print(", ");
    Serial.println(longitude, 5);

    // Same goes for date and time
    int year;
    byte month, day, hour, minute, second, hundredths;
    gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths);
    // Print data and time
    Serial.print("Date: "); Serial.print(month, DEC); Serial.print("/");
    Serial.print(day, DEC); Serial.print("/"); Serial.print(year);
    Serial.print("  Time: "); Serial.print(hour, DEC); Serial.print(":");
    Serial.print(minute, DEC); Serial.print(":"); Serial.print(second, DEC);
    Serial.print("."); Serial.println(hundredths, DEC);
    //Since month, day, hour, minute, second, and hundr

    // Here you can print the altitude and course values directly since
    // there is only one value for the function
    Serial.print("Altitude (meters): "); Serial.println(gps.f_altitude());
    // Same goes for course
    Serial.print("Course (degrees): "); Serial.println(gps.f_course());
    // And same goes for speed
    Serial.print("Speed(kmph): "); Serial.println(gps.f_speed_kmph());
    //Serial.println();

    // Here you can print statistics on the sentences.
    unsigned long chars;
    unsigned short sentences, failed_checksum;
    gps.stats(&chars, &sentences, &failed_checksum);
    //Serial.print("Failed Checksums: ");Serial.print(failed_checksum);
    //Serial.println(); Serial.println();

    // Here you can print the number of satellites in view
    Serial.print("Satellites: ");
    Serial.println(gps.satellites());
  }

