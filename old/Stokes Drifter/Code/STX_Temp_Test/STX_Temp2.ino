void setup() {
  
  Serial.begin(9600); 
  Serial3.begin(9600, SERIAL_8N1);
  
  unsigned char STX_requestESN[] = {
  0xAA, 0x05, 0x01, 0x50, 0xD5 };
  byte STXesn[32]; 
  char buffer[200];
  
int STXrx = 14; //Receive from STX into pin 14 on Due 
int STXtx = 15; //Transmit out to STX from Due pin 15 
int STXrts = 22; // RTS signal to STX 
int STXcts = 23; // CTS signal to STX
int STXrst = 24; // Reset signal to STX 

// --------- Initial States -------------
// // pinMode(STXrx, INPUT_PULLUP);
//  digitalWrite(STXrx, LOW);
//
//  digitalWrite(STXtx, LOW); 
//  //pinMode(STXtx, OUTPUT); 
//  
//  //pinMode(STXcts, INPUT);       
//  digitalWrite(STXcts, LOW);
//  
//  digitalWrite(STXrts, LOW);   
//  //pinMode(STXrts, OUTPUT); 
//
//  digitalWrite(STXrst, LOW);
//  //pinMode(STXrst, OUTPUT);  
//  

  Serial.println("STX Power ON"); 
   delay(1000); 
  
//----------- Power-up States ------------

  pinMode(STXrx, INPUT_PULLUP);
  digitalWrite(STXrx, HIGH);

  digitalWrite(STXtx, HIGH); 
  pinMode(STXtx, OUTPUT); 
  
  pinMode(STXcts, INPUT);       
  //digitalWrite(STXcts, HIGH);
  
  digitalWrite(STXrts, HIGH);   
  pinMode(STXrts, OUTPUT); 

  digitalWrite(STXrst, HIGH);
  pinMode(STXrst, OUTPUT);  

  Serial.println("STX is set up");
  delay(3000);
  //________________________________________

   digitalWrite(STXrts, LOW); 
        Serial.println("RTS Low  ");
    while (HIGH == digitalRead(STXcts));
        Serial.println("CTS Low  "); 
        Serial.println();
  //Send request to STX
  for(int i = 0 ; i < 5 ; i++)
    Serial3.print(char(STX_requestESN[i])); 
    
 digitalWrite(STXrts, HIGH); 
  while (LOW == digitalRead(STXcts)); 

Serial3.readBytes(STXesn, 32);

  //For debugging
  Serial.print("STX ESN: ");
  for(int x = 0 ; x < 32 ; x++) {
    sprintf(buffer, "%02X ", (byte)STXesn[x]);
    Serial.print(buffer);
  }
  Serial.println();

  delay(1000);
  
//________________________________________

}

void loop() {
  

}
