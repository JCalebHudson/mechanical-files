/**************************************************
 ************  STX3 CONTROL ROUTINES **************
 **************************************************
CREATED BY: Michael Prutsman 
  for FAMU-FSU ECE Senior Design Project
  Team E#6 Stokes Drifters 2014-2015
  DATE: March 7, 2015
  
   Modeled after the "Run SPOT Run" code created 
     by Nathan Seidle: 
       12-22-2011
       Spark Fun Electronics 2012
       Public Domain 

  FUNCTION: Designed for the Arduino DUE with ARM
    processor, this code controls the Globalstar
    STX3 Modem via UART Serial control. The code
    runs a setup subroutine in which all pins are 
    assigned according to predetermined standards.
    The setup routine also sets up the STX for 
    operation in the proper mode, including chan, 
    number of bursts, and min and max delay btwn
    bursts of data. 
    A STXgetESN subroutine requests the ESN from
    the STX modem and listens for a response via 
    serial. This is primarily for testing purposes.
    All serial commands and requests are stored
    as unsigned chars and are created prior to 
    the void setup() routine. 

  PIN ASSIGNMENTS:
     ____STX_____|_____ARDUINO____
    |            |                |
    |    VDIG    |       3V3      |
    |    GND     |       GND      |
    |    TX      |       14(RX)   |
    |    RX      |       15(TX)   |
    |    RTS     |       22       |
    |    CTS     |       23       |
    |    RST     |       24       |
    |    TEST1   |       26       |
    |    TEST2   |       27       |
    |    ant_pwr |       28       |
    |____________|________________|
    
 */

// All the following are variables needed to use STX
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// --- STX Connections ---
int STXrx = 14; //Receive from STX into pin 14 on Due 
int STXtx = 15; //Transmit out to STX from Due pin 15 
int STXrts = 22; // RTS signal to STX 
int STXcts = 23; // CTS signal to STX
int STXrst = 24; // Reset signal to STX 
// int STXpge = 25; 
int STX_Test1 = 26; 
int STX_Test2 = 27; 
int STXant_pwr = 28; 

int sentTries = 0; //Starts at 0 and goes to 1, then to 2, then unit shuts down.

unsigned char STX_requestUnitID[] = { 
  0xAA, 0x03, 0x01 };
unsigned char STX_requestStatus[] = { 
  0xAA, 0x03, 0x52 };
unsigned char STX_requestESN[] = {
  0xAA, 0x05, 0x01, 0x50, 0xD5 }; 
unsigned char STX_setup[] = {
  0xAA, 0x0E, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x18, 0x30, 0x00, 0xCE, 0x9C };  // [Channel A, 3 Burst, min 120sec, max 240sec] 
unsigned char STX_requestSetup[] = { 
  0xAA, 0x05, 0x07, 0x66, 0xB0 };
unsigned char DrifterOut[] = { 0xAA, 0x00, 0x00, 0x00 };

char buffer[200]; //Used for status sprintf

int unitStatus = 0;  // status of unit from checkStatus routine 

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 

void setup() {
  Serial3.begin(9600, SERIAL_8N1);
  Serial.begin(9600);
  int tries;
 // for(tries = 0 ; tries < 3 ; tries++){

    STXsetup(); //Init the IO pins for the STX
    STXcheckStatus(); //Check for a valid response

 //    if(unitStatus != 255) break; //255 = STX didn't respond

 //   Serial.print("STX failed to respond.");
 // }

//   if(tries == 3) { //Give up!
//    Serial.print("Please check connections.");
//    
//    if(1){ //Infini-blink error
//      digitalWrite(8, HIGH);
//      delay(1000);
//      digitalWrite(8, LOW);
//      delay(1000);
//    }
//  }
  
}

void loop() {

    STXgetESN(); 
    delay(1000); 
    STXsendData();
    delay(1000);
                //  
                //   if(STXcheckStatus()) {
                //
                //    Serial.print("[Unit Status:");
                //     if(unitStatus == 0) Serial.print("Waiting Cmd]");
                //    else  if(unitStatus == 0x07) Serial.print("GPS Search]");
                //    else  if(unitStatus == 0x0F) Serial.print("GPS Lock]");
                //    else  if(unitStatus == 0x06) Serial.print("Xmit to Sats]");
                //    else Serial.print("No response]");
                //
                //     if(gpsStatus == 0) Serial.print(" (No location)");
                //    else  if(gpsStatus == 1) Serial.print(" (Location good)");
                //
                //    sprintf(buffer, " [Sats:%02d] [Next Xmit:%02d] [Try#:%02d] [#ofPowerOns:%02d]", satsInView, timeToNextCheckin, sentTries, numberOfPowerOns);
                //    Serial.println(buffer);
                //
                //  }
                //  else {
                //    Serial.println("STX failed to respond to status check");
                //    unitStatus = 0xFF; //Error mode
                //  }
                //
                //  //Let's send a new message!
                //   if(unitStatus == 0) {
                //    String msgToSend = "Hello Stars! Msg#:" + String(messageNumber); //This is the message to send
                //    //String msgToSend = "Test"; //For testing
                //
                //    int msgLength = 8 + msgToSend.length(); //There are 8 data bytes before the message begins 
                //    
                //    byte STXmsg[100];
                //    
                //    STXmsg[0] = 0xAA; //Preamble
                //    STXmsg[1] = msgLength; //Frame length
                //    STXmsg[2] = 0x26; //Transmit
                //    STXmsg[3] = 0x01; //0x01 = I'm ok, 0x04 = Help, 0x40 = Track?)
                //    STXmsg[4] = 0x00; //Unknown
                //    STXmsg[5] = 0x01; //Unknown
                //    STXmsg[6] = 0x00; //Unknown
                //    STXmsg[7] = 0x01; //Unknown
                //    for(int x = 0 ; x < msgToSend.length() ; x++)
                //      STXmsg[8 + x] = msgToSend[x]; //Attach message onto frame
                //      
                //    //Serial.print("Message:");
                //    for(int i = 0 ; i < msgLength ; i++) {
                //      softSTX.print(char(STXmsg[i])); //Push message request
                //
                //      //sprintf(buffer, "%02X ", STXmsg[i]);
                //      //Serial.print(buffer);
                //    }
                //    //Serial.println();
                //
                //    Serial.println("Message sent");
                //    
                //    messageNumber++; //Advance to the next message number
                //  }
                //
                //  delay(1000); //Check status every second
}

//Sets up the IO pins for the STX
void STXsetup(void){
  
  pinMode(STXrx, INPUT_PULLUP);
  digitalWrite(STXrx, HIGH);

  digitalWrite(STXtx, HIGH);     // < - - - - - - - - - - - - - - - - - - - - - SHOULD THIS BE HIGH? 
  pinMode(STXtx, OUTPUT); 
  
  pinMode(STXcts, INPUT_PULLUP);        // CTS output of STX for signaling that STX is ready for message 
  digitalWrite(STXcts, HIGH);
  
  digitalWrite(STXrts, HIGH);   // RTS input of STX for transmission preparation
  pinMode(STXrts, OUTPUT); 

  digitalWrite(STXrst, LOW);
  pinMode(STXrst, OUTPUT);  
 
  pinMode(STXant_pwr, INPUT);   // Open Collector control for Sat. antenna power switch
  digitalWrite(STXant_pwr, LOW);
  
  Serial.println("STX is set up");
  
  //=-=-=-=-=-=-=-=-=-STX Setup (0x06)=-=-=-=-=-=-=-=-=-=-=\\
  byte STX_setupState[50];
  int vCTS = 0;
  
  // ----- Write Setup State -----\\
    digitalWrite(STXrts, LOW); delay(125);
     delay(500);                                           //   Serial.println("RTS Low");
   // while (HIGH == digitalRead(STXcts));       
                                             //      Serial.println("CTS Low");                                         

  for(int i = 0 ; i < 13 ; i++)
    Serial3.print(char(STX_setup[i])); 
    
                                            //        Serial.println("setup printed");
  
  digitalWrite(STXrts, HIGH);               //        Serial.println("RTS High");
  while (LOW == digitalRead(STXcts));      //         Serial.println("CTS High");
  
   
// ----- Read Setup State -----\\
  digitalWrite(STXrts, LOW); 
  delay(500);
//  while (HIGH == digitalRead(STXcts));                                                   

  for(int i = 0 ; i < 4 ; i++)
    Serial3.print(char(STX_requestSetup[i])); 
  
  digitalWrite(STXrts, HIGH);
  while (LOW == digitalRead(STXcts));
 
  Serial3.readBytes(STX_setupState, 15);
  
    // --- print Setup State 
  Serial.print("Setup State is: ");
  for(int x = 0 ; x < 32 ; x++) {
  sprintf(buffer, "%02X ", (byte)STX_setupState[x]);
  Serial.print(buffer);
  }
   Serial.println();
}

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//Check Status Routine:
//  Request status of 
int STXcheckStatus(void){
  byte STXresponse[50];
  int vCTS = 0;
  
    digitalWrite(STXrts, LOW); 
    delay(500);
    //while (HIGH == digitalRead(STXcts));     // Waits for CTS to go LOW                           //NEED TO ADD TIMER FOR ERROR!!
    
    for(int i = 0 ; i < 3 ; i++)
    Serial3.print(char(STX_requestStatus[i])); 
  
  digitalWrite(STXrts, HIGH);
  while (LOW == digitalRead(STXcts));        // Waits for CTS to go HIGH 
  Serial3.readBytes(STXresponse, 32);

  //For debugging
  Serial.print("Status Array: ");
  for(int x = 0 ; x < 32 ; x++) {
    sprintf(buffer, "%02X ", (byte)STXresponse[x]);
    Serial.print(buffer);
  }
  Serial.println();

   if(STXresponse[0] == 0xAA) { //We have found the header!

//    numberOfPowerOns = STXresponse[6];

    unitStatus = STXresponse[7];

//    timeToNextCheckin = (STXresponse[11] << 8) | STXresponse[12];

    sentTries = STXresponse[19];  //Starts at 0 and goes to 1, then to 2, then unit shuts down.

    return(1); //We got a good response
  }
  else {
 //   numberOfPowerOns = 0;

    unitStatus = 255; 

 //   timeToNextCheckin = 0;

    sentTries = 0; 

    return(0); //We didn't get a response in time!
  }
}


void STXgetESN(void){
  byte STXesn[10];
  int vCTS = 0;
  
    digitalWrite(STXrts, LOW); 
    delay(500);
     //while (HIGH == digitalRead(STXcts));
    
  //Send request to STX
  for(int i = 0 ; i < 5 ; i++)
    Serial3.print(char(STX_requestESN[i])); 
    
 digitalWrite(STXrts, HIGH);
  while (LOW == digitalRead(STXcts));

Serial3.readBytes(STXesn, 32);

  //For debugging
  Serial.print("STX ESN: ");
  for(int x = 0 ; x < 32 ; x++) {
    sprintf(buffer, "%02X ", (byte)STXesn[x]);
    Serial.print(buffer);
  }
  Serial.println();
}

void STXsendData(void){
  byte STXresponse[10];
 // int Drifterlength = DrifterOut.length();
    int Drifterlength = 36;
    digitalWrite(STXrts, LOW); 
    delay(500);
     //while (HIGH == digitalRead(STXcts));
  byte STXmsg[100]; 
    
  STXmsg[0] = 0xAA; //Preamble
  STXmsg[1] = Drifterlength; //Frame length
  STXmsg[2] = 0x26; //Transmit
  STXmsg[3] = 0x01; //0x01 = I'm ok, 0x04 = Help, 0x40 = Track?)
  STXmsg[4] = 0x00; //Unknown
  STXmsg[5] = 0x01; //Unknown
  STXmsg[6] = 0x00; //Unknown
  STXmsg[7] = 0x01; //Unknown
  for(int x = 0 ; x < Drifterlength ; x++)
    STXmsg[8 + x] = DrifterOut[x]; //Attach message onto frame
  
  for(int i = 0 ; i < Drifterlength + 8 ; i++)
    Serial3.print(char(STXmsg[i])); 
    
 digitalWrite(STXrts, HIGH);
while (LOW == digitalRead(STXcts));

Serial3.readBytes(STXresponse, 12);

  //For debugging
  Serial.print("STX Transmitted: ");
  for(int x = 0 ; x < 32 ; x++) {
    sprintf(buffer, "%02X ", (byte)STXresponse[x]);
    Serial.print(buffer);
  }
  Serial.println();
}

//unsigned char SatString(void){ 
  

