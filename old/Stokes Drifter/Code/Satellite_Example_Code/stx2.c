#include "stx2.h"
#include "uart.h"
#include "xitoa.h"

#include <avr/io.h>
#include <util/delay.h>

#define setRTS()	STX2_RTS_PORT |= 1 << STX2_RTS_P
#define clearRTS()	STX2_RTS_PORT &= ~(1 << STX2_RTS_P)
#define cts			(STX2_CTS_PIN & (1 << STX2_CTS_P))

void stx2Init() {
	// set RTS pin to output
	STX2_RTS_DDR 	|= 1 << STX2_RTS_P;
	STX2_RTS_PORT 	|= 1 << STX2_RTS_P;

	// set CTS pin to input
	STX2_CTS_DDR 	&= ~(1 << STX2_CTS_P);
	STX2_CTS_PORT 	&= ~(1 << STX2_CTS_P);
	//STX2_CTS_PORT 	|= 1 << STX2_CTS_P; // pull up

	uartInit(STX2_UART, BAUDRATE(STX2_BAUD));
}

uint8_t stx2WaitForByte() {
	for(uint16_t i = 0; i < 300; i++) {
		if(uartBytePending(STX2_UART)) {
			return 1;
		}
		_delay_us(100);
	}
	return 0;
}

#define CRC_OK	0xF0B8

uint16_t crc16(uint16_t crc, uint8_t *ptr, int length)
{
  auto uint16_t i;
  while(length--)
  {
    crc = crc ^ (uint16_t) *ptr++;
    for(i=0;i<8;i++)
    {
      if(crc & 0x0001)
      crc = (crc >> 1) ^ 0x8408;
      else
      crc >>= 1;
      }
  }
  return crc;
}

uint8_t stx2ReadResponse(uint8_t * response, uint8_t max_size) {
	uint8_t ret;
	ret = stx2WaitForByte();
	if(!ret) {
		return 0;
	}
	response[0] = uartReadByte(STX2_UART); 	// preamble
	if(response[0] != 0xAA) {
		return 0;
	}
	ret = stx2WaitForByte();
	if(!ret) {
		return 0;
	}
	response[1] = uartReadByte(STX2_UART);	// size
	if(response[1] > max_size) {
		return 0;
	}
	for(uint8_t i = 2; i < response[1]; i++) {
		ret = stx2WaitForByte();
		if(!ret) {
			return 0;
		} else {
			response[i] = uartReadByte(STX2_UART);
		}
	}
	return response[1]; 					// size
}

uint8_t stx2CheckCRC(uint8_t * message) {
	uint16_t crc = crc16(0xFFFF, message, message[1]);
	return crc == CRC_OK;
}

uint8_t stx2Send(uint8_t * msg) {
	uint8_t ret;
	uint8_t buffer[32];

	if(!cts) {
		xputs(PSTR("CTS is low...\n"));
		return 0;
	}

	xputs(PSTR("Sending: "));
	for(uint8_t i = 0; i < msg[1]; i++) {
		xprintf(PSTR("%02X "), msg[i]);
	}
	xputs(PSTR("\n"));
	clearRTS();
	for(uint16_t i = 0; i < 21; i++) {
		if(!cts) {
			break;
		}
		_delay_ms(1);
	}
	if(cts) {
		setRTS();
		xputs(PSTR("Failure\n"));
		return 0;
	} else {
		xputs(PSTR("Success\n"));
	}

	xputs(PSTR("Sending message... "));
	uartSendArray(STX2_UART, msg, msg[1]);
	setRTS();
	xputs(PSTR("Done\n"));

	xputs(PSTR("Waiting for response... "));

	ret = stx2ReadResponse(buffer, 32);
	if(ret == 0) {
		xputs(PSTR("Failure\n"));
		return 0;
	} else {
		xputs(PSTR("Success\n"));
	}
	xputs(PSTR("Response: "));
	for(uint8_t i = 0; i < ret; i++) {
		xprintf(PSTR("%02X "), buffer[i]);
	}
	xputs(PSTR("CRC: "));
	if(stx2CheckCRC(buffer)) {
		xputs(PSTR("OK\n"));
	} else {
		xputs(PSTR("Bad\n"));
	}
	return 1;
}

uint8_t stx2Setup(uint8_t num_tries, uint16_t min_interval_s, uint16_t max_interval_s) {
	// AA 0E 06 00 00 00 00 00 03 3C 78 00 32 74

	uint8_t msg[14];
	msg[0]  = 0xAA;		// preamble
	msg[1]  = 14;		// size
	msg[2]  = 0x06;		// command: setup
	msg[3]  = 0x00;		// deprecated
	msg[4]  = 0x00;		// deprecated
	msg[5]  = 0x00;		// deprecated
	msg[6]  = 0x00;		// deprecated
	msg[7]  = 0x00;		// RF channel: A
	msg[8]  = num_tries;
	msg[9]  = min_interval_s / 5;
	msg[10] = max_interval_s / 5;
	msg[11] = 0x00; 	// power level (fixed at 20dBm)

	uint16_t crc = ~crc16(0xFFFF, msg, msg[1]-2);

	msg[12] = (uint8_t)(crc & 0xFF);
	msg[13] = (uint8_t)(crc >> 8);

	return stx2Send(msg);
}

uint8_t stx2SendEncodedPosition(stx2_message_t type, uint8_t * lat, uint8_t * lon) {
	//AA 0E 00 00 3D DD 85 C6 C5 5D 01 00 EC 78

	uint8_t msg[14];
	msg[0]  = 0xAA;		// preamble
	msg[1]  = 14;		// size
	msg[2]  = 0x00;		// command: transmit message
	msg[3]  = 0x00;		// unknown
	msg[4]  = lat[0];
	msg[5]  = lat[1];
	msg[6]  = lat[2];
	msg[7]  = lon[0];
	msg[8]  = lon[1];
	msg[9]  = lon[2];
	msg[10] = type;
	msg[11] = 0x00; 	// unknown

	uint16_t crc = ~crc16(0xFFFF, msg, msg[1]-2);

	msg[12] = (uint8_t)(crc & 0xFF);
	msg[13] = (uint8_t)(crc >> 8);

	return stx2Send(msg);
}
