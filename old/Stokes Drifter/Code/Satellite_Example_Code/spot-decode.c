/*
  This program decodes data encoded as Lat/Lon on findmespot.com into the 
  original 5 bytes that were sent.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

uint32_t decodeLatitude(double latitude) {
		uint32_t decoded = 0;
		if(latitude >= 0.0) {
			decoded = (uint32_t) round(latitude / 90.0 * 8388608.0);
		} else {
			decoded = (uint32_t) round((latitude + 180.0) / 90.0 * 8388608.0);
		}
		
		return decoded;
}

uint32_t decodeLongitude(double longitude) {
		uint32_t decoded = 0;
		if(longitude >= 0.0) {
			decoded = (uint32_t) round(longitude / 180.0 * 8388608.0);
		} else {
			decoded = (uint32_t) round((longitude + 360.0) / 180.0 * 8388608.0);
		}
		
		return decoded;
}

int main(int argc, char ** argv) {
	if(argc < 3) {
		printf("spot-decode lat lon\n");
		return 0;
	}
	
	double lat = strtod(argv[1], 0);
	double lon = strtod(argv[2], 0);
	
	
	//printf("pos %f, %f\n", lat, lon);
	
	uint32_t lat_decoded = decodeLatitude(lat);
	uint32_t lon_decoded = decodeLongitude(lon);
	
	//printf("%06X %06X\n", lat_decoded, lon_decoded);
	
	uint8_t data[5];
	data[0] = (uint8_t) ((lat_decoded >> 16) & (uint32_t) 0xFF);
	data[1] = (uint8_t) ((lat_decoded >> 8) & 0xFF);
	
	data[2] = (uint8_t) ((lon_decoded >> 16) & 0xFF);
	data[3] = (uint8_t) ((lon_decoded >> 8) & 0xFF);
	
	data[4] = (uint8_t) ((lat_decoded & 0xF0) | ((lon_decoded & 0xF0) >> 4));
	
	// now, data is restored.
	
	/*
	for(int i = 0; i < 5; i++) {
		printf("%02X ", data[i]);
	}
	printf("\n");
	*/
	
	// unmarshall the sensor readings:
	
	uint32_t altitude = ((uint32_t) data[0] << 8) |  ((uint32_t) data[1]);
	altitude *= 2;
	
	uint16_t speed = (uint16_t) data[2];
	float heading = ((float) data[3]) * 360.0 / 256.0;
	int8_t temperature = (int16_t) data[4];
	
	printf("%u %u %.1f %i\n", altitude, speed, heading, temperature);
	
	return 0;
}

