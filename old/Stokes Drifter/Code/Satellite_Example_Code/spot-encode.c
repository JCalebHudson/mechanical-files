// ...


// sends 5 data bytes
uint8_t stx2SendData(stx2_message_t type, uint8_t * data) {
	uint8_t msg[14];
	msg[0]  = 0xAA;		// preamble
	msg[1]  = 14;		  // size
	msg[2]  = 0x00;		// command: transmit message
	msg[3]  = 0x00;		// unknown
	msg[4]  = data[0];
	msg[5]  = data[1];
	msg[6]  = (data[4] & 0xF0) | 0x07; // trick to deal with round-off error
	msg[7]  = data[2];
	msg[8]  = data[3];
	msg[9]  = (data[4] << 4) | 0x07;   // trick to deal with round-off error
	msg[10] = type;
	msg[11] = 0x00; 	// unknown
 
	uint16_t crc = ~crc16(0xFFFF, msg, msg[1]-2);
 
	msg[12] = (uint8_t)(crc & 0xFF);
	msg[13] = (uint8_t)(crc >> 8);
 
	return stx2Send(msg);
}

// send 5 data bytes:
//   2 bytes altitude, 1 byte speed, 1 byte heading, 1 byte temperature
void transmitAltitude() {
		uint16_t altitude = (uint16_t) (gps_info.alt / 2.0); // convert to 2 meters units to squeeze into 16 bits
 
		uint8_t data[5];
 
		data[0] = (uint8_t) ((altitude >> 8) & 0xFF);
		data[1] = (uint8_t) ((altitude >> 0) & 0xFF);
		data[2] = (uint8_t) gps_info.speed;
		data[3] = (uint8_t) (gps_info.heading / 360.0 * 256.0);
		// send whole degrees
		data[4] = (uint8_t) (((uint16_t) temperature >> 1) & 0x00FF);
 
		stx2SendData(OK, data);
}

// ...
