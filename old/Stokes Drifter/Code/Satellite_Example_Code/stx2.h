#include <avr/io.h>

#ifndef STX2_H
#define STX2_H

/* uart settings */
#define STX2_UART	UART0
#define STX2_BAUD	9600

/* request to send pin */
#define STX2_RTS_PORT	PORTE
#define STX2_RTS_DDR	DDRE
#define STX2_RTS_P		PE2

/* clear to send pin */
#define STX2_CTS_PORT	PORTE
#define STX2_CTS_DDR	DDRE
#define STX2_CTS_PIN	PINE
#define STX2_CTS_P		PE3

typedef enum {
	OK = 0x01,
	HELP = 0x04,
	TRACK = 0x40
} stx2_message_t;


void stx2Init();
uint8_t stx2ReadResponse(uint8_t * response, uint8_t max_size);
uint8_t stx2CheckCRC(uint8_t * message);
uint8_t stx2SendEncodedPosition(stx2_message_t type, uint8_t * lat, uint8_t * lon);
uint8_t stx2Setup(uint8_t num_tries, uint16_t min_interval_s, uint16_t max_interval_s);

#endif /* STX2_H */
