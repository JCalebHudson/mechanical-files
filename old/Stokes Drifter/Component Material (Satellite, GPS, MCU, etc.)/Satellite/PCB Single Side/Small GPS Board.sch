<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="selfmade">
<description>&lt;h1&gt;Selfmade Library&lt;/h1&gt;

&lt;p&gt;Eine Sammlung diverser Bauteile, die bislang nicht unter Eagle vorhanden waren.&lt;/p&gt;
&lt;p&gt;Diese Eagle Library ist erstellt und gepflegt von Martin Matysiak (&lt;a href="www.k621.de"&gt;www.k621.de&lt;/a&gt;), bei Fragen oder Anregungen und Ergänzungen bitte eine E-Mail an &lt;a href="mailto:mail@k621.de"&gt;mail@k621.de&lt;/a&gt;. Danke!&lt;/p&gt;</description>
<packages>
<package name="GPS-A2235-H">
<description>&lt;h1&gt;Maestro A2235-H&lt;/h1&gt;

&lt;p&gt;The outer pads are placed as per recommendation in the user manual, even though they seem a little bit too small for the module's pads (drawn in layer tPlace). Size of GPS Antenna approximate, not specified in the user manual.&lt;/p&gt;</description>
<smd name="GND_PAD@1" x="0.635" y="-0.635" dx="1.27" dy="1.27" layer="1"/>
<smd name="GND_PAD@2" x="3.175" y="-0.635" dx="1.27" dy="1.27" layer="1"/>
<smd name="GND_PAD@3" x="5.715" y="-0.635" dx="1.27" dy="1.27" layer="1"/>
<smd name="GND_PAD@4" x="8.255" y="-0.635" dx="1.27" dy="1.27" layer="1"/>
<smd name="GND_PAD@8" x="0.635" y="-12.065" dx="1.27" dy="1.27" layer="1"/>
<smd name="GND_PAD@7" x="3.175" y="-12.065" dx="1.27" dy="1.27" layer="1"/>
<smd name="GND_PAD@6" x="5.715" y="-12.065" dx="1.27" dy="1.27" layer="1"/>
<smd name="GND_PAD@5" x="8.255" y="-12.065" dx="1.27" dy="1.27" layer="1"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="1.67" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.67" x2="-3.81" y2="1.57" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.57" x2="-3.81" y2="0.97" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0.97" x2="-3.81" y2="0.87" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0.87" x2="-3.81" y2="-15.24" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-15.24" x2="12.7" y2="-15.24" width="0.127" layer="21"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<text x="12.7" y="4.445" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="-3.81" y="-17.145" size="1.27" layer="27">&gt;VALUE</text>
<smd name="22" x="12.7" y="1.27" dx="1.2" dy="0.9" layer="1"/>
<smd name="21" x="12.7" y="0" dx="1.2" dy="0.9" layer="1"/>
<smd name="20" x="12.7" y="-1.27" dx="1.2" dy="0.9" layer="1"/>
<smd name="19" x="12.7" y="-2.54" dx="1.2" dy="0.9" layer="1"/>
<smd name="18" x="12.7" y="-3.81" dx="1.2" dy="0.9" layer="1"/>
<smd name="17" x="12.7" y="-5.08" dx="1.2" dy="0.9" layer="1"/>
<smd name="16" x="12.7" y="-6.35" dx="1.2" dy="0.9" layer="1"/>
<smd name="15" x="12.7" y="-7.62" dx="1.2" dy="0.9" layer="1"/>
<smd name="14" x="12.7" y="-8.89" dx="1.2" dy="0.9" layer="1"/>
<smd name="13" x="12.7" y="-10.16" dx="1.2" dy="0.9" layer="1"/>
<smd name="12" x="12.7" y="-11.43" dx="1.2" dy="0.9" layer="1"/>
<smd name="11" x="12.7" y="-12.7" dx="1.2" dy="0.9" layer="1"/>
<smd name="10" x="12.7" y="-13.97" dx="1.2" dy="0.9" layer="1"/>
<smd name="9" x="-3.81" y="-13.97" dx="1.2" dy="0.9" layer="1"/>
<smd name="8" x="-3.81" y="-12.7" dx="1.2" dy="0.9" layer="1"/>
<smd name="7" x="-3.81" y="-11.43" dx="1.2" dy="0.9" layer="1"/>
<smd name="1" x="-3.81" y="1.27" dx="1.2" dy="0.9" layer="1"/>
<smd name="2" x="-3.81" y="0" dx="1.2" dy="0.9" layer="1"/>
<smd name="3" x="-3.81" y="-1.27" dx="1.2" dy="0.9" layer="1"/>
<smd name="4" x="-3.81" y="-2.54" dx="1.2" dy="0.9" layer="1"/>
<smd name="5" x="-3.81" y="-3.81" dx="1.2" dy="0.9" layer="1"/>
<smd name="6" x="-3.81" y="-5.08" dx="1.2" dy="0.9" layer="1"/>
<polygon width="0.127" layer="41">
<vertex x="6.35" y="-6.35" curve="-90"/>
<vertex x="4.445" y="-8.255" curve="-270"/>
</polygon>
<wire x1="-3.81" y1="1.67" x2="-2.81" y2="1.67" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="1.67" x2="-2.81" y2="0.87" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="0.87" x2="-3.81" y2="0.87" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="0.97" x2="-3.51" y2="1.27" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="1.27" x2="-3.81" y2="1.57" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="0.4" x2="-2.81" y2="0.4" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="0.4" x2="-2.81" y2="-0.4" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-0.4" x2="-3.81" y2="-0.4" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-0.3" x2="-3.51" y2="0" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="0" x2="-3.81" y2="0.3" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="-0.87" x2="-2.81" y2="-0.87" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-0.87" x2="-2.81" y2="-1.67" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-1.67" x2="-3.81" y2="-1.67" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-1.57" x2="-3.51" y2="-1.27" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="-1.27" x2="-3.81" y2="-0.97" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="-2.14" x2="-2.81" y2="-2.14" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-2.14" x2="-2.81" y2="-2.94" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-2.94" x2="-3.81" y2="-2.94" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-2.84" x2="-3.51" y2="-2.54" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="-2.54" x2="-3.81" y2="-2.24" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="-3.41" x2="-2.81" y2="-3.41" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-3.41" x2="-2.81" y2="-4.21" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-4.21" x2="-3.81" y2="-4.21" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-4.11" x2="-3.51" y2="-3.81" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="-3.81" x2="-3.81" y2="-3.51" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="-4.68" x2="-2.81" y2="-4.68" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-4.68" x2="-2.81" y2="-5.48" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-5.48" x2="-3.81" y2="-5.48" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-5.38" x2="-3.51" y2="-5.08" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="-5.08" x2="-3.81" y2="-4.78" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="-11.03" x2="-2.81" y2="-11.03" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-11.03" x2="-2.81" y2="-11.83" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-11.83" x2="-3.81" y2="-11.83" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-11.73" x2="-3.51" y2="-11.43" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="-11.43" x2="-3.81" y2="-11.13" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="-12.3" x2="-2.81" y2="-12.3" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-12.3" x2="-2.81" y2="-13.1" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-13.1" x2="-3.81" y2="-13.1" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-13" x2="-3.51" y2="-12.7" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="-12.7" x2="-3.81" y2="-12.4" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.81" y1="-13.57" x2="-2.81" y2="-13.57" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-13.57" x2="-2.81" y2="-14.37" width="0.0635" layer="21"/>
<wire x1="-2.81" y1="-14.37" x2="-3.81" y2="-14.37" width="0.0635" layer="21"/>
<wire x1="-3.81" y1="-14.27" x2="-3.51" y2="-13.97" width="0.0635" layer="21" curve="90"/>
<wire x1="-3.51" y1="-13.97" x2="-3.81" y2="-13.67" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-5.48" x2="11.7" y2="-5.48" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-5.48" x2="11.7" y2="-4.68" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-4.68" x2="12.7" y2="-4.68" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-4.78" x2="12.4" y2="-5.08" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-5.08" x2="12.7" y2="-5.38" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-4.21" x2="11.7" y2="-4.21" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-4.21" x2="11.7" y2="-3.41" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-3.41" x2="12.7" y2="-3.41" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-3.51" x2="12.4" y2="-3.81" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-3.81" x2="12.7" y2="-4.11" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-2.94" x2="11.7" y2="-2.94" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-2.94" x2="11.7" y2="-2.14" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-2.14" x2="12.7" y2="-2.14" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-2.24" x2="12.4" y2="-2.54" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-2.54" x2="12.7" y2="-2.84" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-1.67" x2="11.7" y2="-1.67" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-1.67" x2="11.7" y2="-0.87" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-0.87" x2="12.7" y2="-0.87" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-0.97" x2="12.4" y2="-1.27" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-1.27" x2="12.7" y2="-1.57" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-0.4" x2="11.7" y2="-0.4" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-0.4" x2="11.7" y2="0.4" width="0.0635" layer="21"/>
<wire x1="11.7" y1="0.4" x2="12.7" y2="0.4" width="0.0635" layer="21"/>
<wire x1="12.7" y1="0.3" x2="12.4" y2="0" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="0" x2="12.7" y2="-0.3" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="0.87" x2="11.7" y2="0.87" width="0.0635" layer="21"/>
<wire x1="11.7" y1="0.87" x2="11.7" y2="1.67" width="0.0635" layer="21"/>
<wire x1="11.7" y1="1.67" x2="12.7" y2="1.67" width="0.0635" layer="21"/>
<wire x1="12.7" y1="1.57" x2="12.4" y2="1.27" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="1.27" x2="12.7" y2="0.97" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-13.1" x2="11.7" y2="-13.1" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-13.1" x2="11.7" y2="-12.3" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-12.3" x2="12.7" y2="-12.3" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-12.4" x2="12.4" y2="-12.7" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-12.7" x2="12.7" y2="-13" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-11.83" x2="11.7" y2="-11.83" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-11.83" x2="11.7" y2="-11.03" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-11.03" x2="12.7" y2="-11.03" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-11.13" x2="12.4" y2="-11.43" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-11.43" x2="12.7" y2="-11.73" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-10.56" x2="11.7" y2="-10.56" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-10.56" x2="11.7" y2="-9.76" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-9.76" x2="12.7" y2="-9.76" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-9.86" x2="12.4" y2="-10.16" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-10.16" x2="12.7" y2="-10.46" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-9.29" x2="11.7" y2="-9.29" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-9.29" x2="11.7" y2="-8.49" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-8.49" x2="12.7" y2="-8.49" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-8.59" x2="12.4" y2="-8.89" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-8.89" x2="12.7" y2="-9.19" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-8.02" x2="11.7" y2="-8.02" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-8.02" x2="11.7" y2="-7.22" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-7.22" x2="12.7" y2="-7.22" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-7.32" x2="12.4" y2="-7.62" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-7.62" x2="12.7" y2="-7.92" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-6.75" x2="11.7" y2="-6.75" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-6.75" x2="11.7" y2="-5.95" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-5.95" x2="12.7" y2="-5.95" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-6.05" x2="12.4" y2="-6.35" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-6.35" x2="12.7" y2="-6.65" width="0.0635" layer="21" curve="90"/>
<wire x1="12.7" y1="-14.37" x2="11.7" y2="-14.37" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-14.37" x2="11.7" y2="-13.57" width="0.0635" layer="21"/>
<wire x1="11.7" y1="-13.57" x2="12.7" y2="-13.57" width="0.0635" layer="21"/>
<wire x1="12.7" y1="-13.67" x2="12.4" y2="-13.97" width="0.0635" layer="21" curve="90"/>
<wire x1="12.4" y1="-13.97" x2="12.7" y2="-14.27" width="0.0635" layer="21" curve="90"/>
<wire x1="11.43" y1="1.27" x2="11.43" y2="-13.97" width="0.127" layer="21"/>
<wire x1="11.43" y1="-13.97" x2="-1.27" y2="-13.97" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-13.97" x2="-2.54" y2="-12.7" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="11.43" y2="1.27" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="-15.24" x2="12.7" y2="2.54" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="GPS-A2235-H">
<wire x1="-20.32" y1="20.32" x2="20.32" y2="20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="-20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="-20.32" x2="-20.32" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-20.32" x2="-20.32" y2="20.32" width="0.254" layer="94"/>
<pin name="N-RST" x="-25.4" y="17.78" length="middle" direction="in"/>
<pin name="GPIO3" x="-25.4" y="15.24" length="middle" direction="nc"/>
<pin name="VCC_3V3" x="-25.4" y="12.7" length="middle" direction="pwr"/>
<pin name="WAKEUP" x="-25.4" y="10.16" length="middle" direction="out"/>
<pin name="VOUT" x="-25.4" y="7.62" length="middle" direction="sup"/>
<pin name="GND@1" x="-25.4" y="5.08" length="middle" direction="pwr"/>
<pin name="GPIO6/SPI_CLK" x="-25.4" y="0" length="middle" direction="in"/>
<pin name="GPIO7/SPI_CS" x="-25.4" y="-2.54" length="middle" direction="in"/>
<pin name="GND@2" x="-25.4" y="-5.08" length="middle" direction="pwr"/>
<pin name="GPIO2" x="25.4" y="-5.08" length="middle" direction="nc" rot="R180"/>
<pin name="HOST-I2C_CLK" x="25.4" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="GPIO4" x="25.4" y="0" length="middle" direction="in" rot="R180"/>
<pin name="TM_GPIO5" x="25.4" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="I2C_CLK/GPIO1" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="I2C_DIO/GPIO0" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="ON_OFF" x="25.4" y="10.16" length="middle" direction="in" rot="R180"/>
<pin name="EXTINT/GPIO8" x="25.4" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="TX0" x="25.4" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="RX0" x="25.4" y="17.78" length="middle" direction="in" rot="R180"/>
<pin name="GND@3" x="-2.54" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@4" x="0" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@5" x="2.54" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@6" x="5.08" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@7" x="7.62" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@8" x="10.16" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@9" x="12.7" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@10" x="15.24" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R90"/>
<text x="20.32" y="22.86" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-20.32" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="ANT_SW" x="-25.4" y="-15.24" length="middle" direction="in"/>
<pin name="VANT" x="-25.4" y="-12.7" length="middle" direction="pwr"/>
<pin name="ANT_EXT" x="-25.4" y="-10.16" length="middle" direction="in"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GPS-A2235-H" prefix="IC">
<description>&lt;h1&gt;Maestro A2235-H&lt;/h1&gt;

&lt;p&gt;GPS Antenna Receiver Module. Available at e.g. Mouser or Farnell (Element14).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.specialty-designs.com/gpsmanual.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Design according to User Manual V1.4. Noticable difference: older versions did not feature pins for an external antenna.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="GPS-A2235-H" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GPS-A2235-H">
<connects>
<connect gate="G$1" pin="ANT_EXT" pad="10"/>
<connect gate="G$1" pin="ANT_SW" pad="12"/>
<connect gate="G$1" pin="EXTINT/GPIO8" pad="20"/>
<connect gate="G$1" pin="GND@1" pad="6"/>
<connect gate="G$1" pin="GND@10" pad="GND_PAD@8"/>
<connect gate="G$1" pin="GND@2" pad="9"/>
<connect gate="G$1" pin="GND@3" pad="GND_PAD@1"/>
<connect gate="G$1" pin="GND@4" pad="GND_PAD@2"/>
<connect gate="G$1" pin="GND@5" pad="GND_PAD@3"/>
<connect gate="G$1" pin="GND@6" pad="GND_PAD@4"/>
<connect gate="G$1" pin="GND@7" pad="GND_PAD@5"/>
<connect gate="G$1" pin="GND@8" pad="GND_PAD@6"/>
<connect gate="G$1" pin="GND@9" pad="GND_PAD@7"/>
<connect gate="G$1" pin="GPIO2" pad="13"/>
<connect gate="G$1" pin="GPIO3" pad="2"/>
<connect gate="G$1" pin="GPIO4" pad="15"/>
<connect gate="G$1" pin="GPIO6/SPI_CLK" pad="7"/>
<connect gate="G$1" pin="GPIO7/SPI_CS" pad="8"/>
<connect gate="G$1" pin="HOST-I2C_CLK" pad="14"/>
<connect gate="G$1" pin="I2C_CLK/GPIO1" pad="17"/>
<connect gate="G$1" pin="I2C_DIO/GPIO0" pad="18"/>
<connect gate="G$1" pin="N-RST" pad="1"/>
<connect gate="G$1" pin="ON_OFF" pad="19"/>
<connect gate="G$1" pin="RX0" pad="22"/>
<connect gate="G$1" pin="TM_GPIO5" pad="16"/>
<connect gate="G$1" pin="TX0" pad="21"/>
<connect gate="G$1" pin="VANT" pad="11"/>
<connect gate="G$1" pin="VCC_3V3" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
<connect gate="G$1" pin="WAKEUP" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA12-1W">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="12.7" y1="1.524" x2="12.7" y2="4.318" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.524" x2="10.16" y2="1.524" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.524" x2="10.16" y2="4.318" width="0.1524" layer="21"/>
<wire x1="10.16" y1="4.318" x2="12.7" y2="4.318" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.524" x2="15.24" y2="4.318" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.524" x2="15.24" y2="1.524" width="0.1524" layer="21"/>
<wire x1="12.7" y1="4.318" x2="15.24" y2="4.318" width="0.1524" layer="21"/>
<wire x1="11.43" y1="5.08" x2="11.43" y2="9.779" width="0.6604" layer="21"/>
<wire x1="13.97" y1="5.08" x2="13.97" y2="9.779" width="0.6604" layer="21"/>
<wire x1="10.16" y1="1.524" x2="7.62" y2="1.524" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.524" x2="7.62" y2="4.318" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.524" x2="5.08" y2="1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.524" x2="5.08" y2="4.318" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.318" x2="7.62" y2="4.318" width="0.1524" layer="21"/>
<wire x1="7.62" y1="4.318" x2="10.16" y2="4.318" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.524" x2="2.54" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.54" y1="4.318" x2="5.08" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="1.524" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="4.318" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.524" x2="2.54" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="1.524" x2="2.54" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0" y1="4.318" x2="2.54" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="9.779" width="0.6604" layer="21"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="9.779" width="0.6604" layer="21"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="9.779" width="0.6604" layer="21"/>
<wire x1="6.35" y1="5.08" x2="6.35" y2="9.779" width="0.6604" layer="21"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="9.779" width="0.6604" layer="21"/>
<wire x1="0" y1="1.524" x2="-2.54" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.524" x2="-5.08" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.524" x2="-5.08" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.524" x2="-7.62" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.524" x2="-7.62" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.318" x2="-5.08" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.318" x2="-2.54" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.524" x2="-10.16" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="4.318" x2="-7.62" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.524" x2="-12.7" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.524" x2="-15.24" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.524" x2="-15.24" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="4.318" x2="-12.7" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.524" x2="-10.16" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.524" x2="-10.16" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="4.318" x2="-10.16" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="5.08" x2="-13.97" y2="9.779" width="0.6604" layer="21"/>
<wire x1="-11.43" y1="5.08" x2="-11.43" y2="9.779" width="0.6604" layer="21"/>
<wire x1="-8.89" y1="5.08" x2="-8.89" y2="9.779" width="0.6604" layer="21"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="9.779" width="0.6604" layer="21"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="9.779" width="0.6604" layer="21"/>
<pad name="12" x="13.97" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-13.97" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-15.24" y="-2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.335" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">1</text>
<text x="-10.795" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">2</text>
<text x="-8.255" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">3</text>
<text x="-5.715" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">4</text>
<text x="-3.175" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">5</text>
<text x="-0.635" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">6</text>
<text x="1.905" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">7</text>
<text x="4.445" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">8</text>
<text x="6.985" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">9</text>
<text x="9.525" y="1.905" size="1.27" layer="21" ratio="10" rot="R90">10</text>
<text x="12.065" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">11</text>
<text x="14.605" y="1.905" size="1.27" layer="21" ratio="10" rot="R90">12</text>
<text x="-7.62" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="11.0998" y1="4.318" x2="11.7602" y2="5.08" layer="21"/>
<rectangle x1="13.6398" y1="4.318" x2="14.3002" y2="5.08" layer="21"/>
<rectangle x1="-1.6002" y1="4.318" x2="-0.9398" y2="5.08" layer="21"/>
<rectangle x1="0.9398" y1="4.318" x2="1.6002" y2="5.08" layer="21"/>
<rectangle x1="3.4798" y1="4.318" x2="4.1402" y2="5.08" layer="21"/>
<rectangle x1="6.0198" y1="4.318" x2="6.6802" y2="5.08" layer="21"/>
<rectangle x1="8.5598" y1="4.318" x2="9.2202" y2="5.08" layer="21"/>
<rectangle x1="-14.3002" y1="4.318" x2="-13.6398" y2="5.08" layer="21"/>
<rectangle x1="-11.7602" y1="4.318" x2="-11.0998" y2="5.08" layer="21"/>
<rectangle x1="-9.2202" y1="4.318" x2="-8.5598" y2="5.08" layer="21"/>
<rectangle x1="-6.6802" y1="4.318" x2="-6.0198" y2="5.08" layer="21"/>
<rectangle x1="-4.1402" y1="4.318" x2="-3.4798" y2="5.08" layer="21"/>
<rectangle x1="13.6398" y1="0.8636" x2="14.3002" y2="1.524" layer="21"/>
<rectangle x1="11.0998" y1="0.8636" x2="11.7602" y2="1.524" layer="21"/>
<rectangle x1="8.5598" y1="0.8636" x2="9.2202" y2="1.524" layer="21"/>
<rectangle x1="13.6398" y1="-0.3302" x2="14.3002" y2="0.8636" layer="51"/>
<rectangle x1="11.0998" y1="-0.3302" x2="11.7602" y2="0.8636" layer="51"/>
<rectangle x1="8.5598" y1="-0.3302" x2="9.2202" y2="0.8636" layer="51"/>
<rectangle x1="6.0198" y1="0.8636" x2="6.6802" y2="1.524" layer="21"/>
<rectangle x1="3.4798" y1="0.8636" x2="4.1402" y2="1.524" layer="21"/>
<rectangle x1="6.0198" y1="-0.3302" x2="6.6802" y2="0.8636" layer="51"/>
<rectangle x1="3.4798" y1="-0.3302" x2="4.1402" y2="0.8636" layer="51"/>
<rectangle x1="0.9398" y1="0.8636" x2="1.6002" y2="1.524" layer="21"/>
<rectangle x1="0.9398" y1="-0.3302" x2="1.6002" y2="0.8636" layer="51"/>
<rectangle x1="-1.6002" y1="0.8636" x2="-0.9398" y2="1.524" layer="21"/>
<rectangle x1="-1.6002" y1="-0.3302" x2="-0.9398" y2="0.8636" layer="51"/>
<rectangle x1="-4.1402" y1="0.8636" x2="-3.4798" y2="1.524" layer="21"/>
<rectangle x1="-6.6802" y1="0.8636" x2="-6.0198" y2="1.524" layer="21"/>
<rectangle x1="-9.2202" y1="0.8636" x2="-8.5598" y2="1.524" layer="21"/>
<rectangle x1="-4.1402" y1="-0.3302" x2="-3.4798" y2="0.8636" layer="51"/>
<rectangle x1="-6.6802" y1="-0.3302" x2="-6.0198" y2="0.8636" layer="51"/>
<rectangle x1="-9.2202" y1="-0.3302" x2="-8.5598" y2="0.8636" layer="51"/>
<rectangle x1="-11.7602" y1="0.8636" x2="-11.0998" y2="1.524" layer="21"/>
<rectangle x1="-14.3002" y1="0.8636" x2="-13.6398" y2="1.524" layer="21"/>
<rectangle x1="-11.7602" y1="-0.3302" x2="-11.0998" y2="0.8636" layer="51"/>
<rectangle x1="-14.3002" y1="-0.3302" x2="-13.6398" y2="0.8636" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA12-1">
<wire x1="3.81" y1="-15.24" x2="-1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="15.24" x2="2.54" y2="15.24" width="0.6096" layer="94"/>
<wire x1="1.27" y1="12.7" x2="2.54" y2="12.7" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="17.78" x2="-1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="17.78" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="17.78" x2="3.81" y2="17.78" width="0.4064" layer="94"/>
<text x="-1.27" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="18.542" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="7.62" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA12-1W" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA12-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA12-1W">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="selfmade" deviceset="GPS-A2235-H" device=""/>
<part name="SV1" library="con-lstb" deviceset="MA12-1W" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="50.8" y="66.04"/>
<instance part="SV1" gate="1" x="111.76" y="63.5" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VCC_3V3"/>
<wire x1="25.4" y1="78.74" x2="-2.54" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="78.74" x2="-2.54" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="17.78" x2="101.6" y2="17.78" width="0.1524" layer="91"/>
<wire x1="101.6" y1="17.78" x2="101.6" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="10"/>
<wire x1="101.6" y1="53.34" x2="104.14" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="WAKEUP"/>
<wire x1="25.4" y1="76.2" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<wire x1="17.78" y1="76.2" x2="17.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="17.78" y1="33.02" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
<wire x1="83.82" y1="33.02" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="ON_OFF"/>
<wire x1="83.82" y1="76.2" x2="76.2" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GND@2"/>
<wire x1="25.4" y1="60.96" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND@1"/>
<wire x1="25.4" y1="71.12" x2="7.62" y2="71.12" width="0.1524" layer="91"/>
<wire x1="7.62" y1="71.12" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND@10"/>
<wire x1="7.62" y1="60.96" x2="7.62" y2="38.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="40.64" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="38.1" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<junction x="7.62" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="GND@9"/>
<wire x1="63.5" y1="38.1" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="60.96" y1="38.1" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="38.1" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<wire x1="53.34" y1="38.1" x2="50.8" y2="38.1" width="0.1524" layer="91"/>
<wire x1="50.8" y1="38.1" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="48.26" y1="38.1" x2="7.62" y2="38.1" width="0.1524" layer="91"/>
<wire x1="63.5" y1="40.64" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<junction x="63.5" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="GND@8"/>
<wire x1="60.96" y1="40.64" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<junction x="60.96" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="GND@7"/>
<wire x1="58.42" y1="40.64" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<junction x="58.42" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="GND@6"/>
<wire x1="55.88" y1="40.64" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<junction x="55.88" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="GND@5"/>
<wire x1="53.34" y1="40.64" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="53.34" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="GND@4"/>
<wire x1="50.8" y1="40.64" x2="50.8" y2="38.1" width="0.1524" layer="91"/>
<junction x="50.8" y="38.1"/>
<pinref part="IC1" gate="G$1" pin="GND@3"/>
<wire x1="48.26" y1="40.64" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<junction x="48.26" y="38.1"/>
<junction x="7.62" y="60.96"/>
<wire x1="7.62" y1="38.1" x2="7.62" y2="15.24" width="0.1524" layer="91"/>
<wire x1="7.62" y1="15.24" x2="104.14" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="11"/>
<wire x1="104.14" y1="15.24" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="ANT_EXT"/>
<wire x1="25.4" y1="55.88" x2="10.16" y2="55.88" width="0.1524" layer="91"/>
<wire x1="10.16" y1="55.88" x2="10.16" y2="25.4" width="0.1524" layer="91"/>
<wire x1="10.16" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<wire x1="93.98" y1="25.4" x2="93.98" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="7"/>
<wire x1="93.98" y1="60.96" x2="104.14" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RX0"/>
<pinref part="SV1" gate="1" pin="1"/>
<wire x1="76.2" y1="83.82" x2="104.14" y2="83.82" width="0.1524" layer="91"/>
<wire x1="104.14" y1="83.82" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TX0"/>
<wire x1="76.2" y1="81.28" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
<wire x1="101.6" y1="81.28" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="2"/>
<wire x1="101.6" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VOUT"/>
<wire x1="25.4" y1="73.66" x2="0" y2="73.66" width="0.1524" layer="91"/>
<wire x1="0" y1="73.66" x2="0" y2="20.32" width="0.1524" layer="91"/>
<wire x1="0" y1="20.32" x2="99.06" y2="20.32" width="0.1524" layer="91"/>
<wire x1="99.06" y1="20.32" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="8"/>
<wire x1="99.06" y1="58.42" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="I2C_DIO/GPIO0"/>
<wire x1="76.2" y1="73.66" x2="96.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="96.52" y1="73.66" x2="96.52" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="3"/>
<wire x1="96.52" y1="71.12" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="I2C_CLK/GPIO1"/>
<wire x1="76.2" y1="71.12" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
<wire x1="91.44" y1="71.12" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="4"/>
<wire x1="91.44" y1="68.58" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="ANT_SW"/>
<wire x1="25.4" y1="50.8" x2="15.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="15.24" y1="50.8" x2="15.24" y2="30.48" width="0.1524" layer="91"/>
<wire x1="15.24" y1="30.48" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
<wire x1="88.9" y1="30.48" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="5"/>
<wire x1="88.9" y1="66.04" x2="104.14" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VANT"/>
<wire x1="25.4" y1="53.34" x2="12.7" y2="53.34" width="0.1524" layer="91"/>
<wire x1="12.7" y1="53.34" x2="12.7" y2="27.94" width="0.1524" layer="91"/>
<wire x1="12.7" y1="27.94" x2="91.44" y2="27.94" width="0.1524" layer="91"/>
<wire x1="91.44" y1="27.94" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="6"/>
<wire x1="91.44" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPIO6/SPI_CLK"/>
<wire x1="25.4" y1="66.04" x2="2.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="2.54" y1="66.04" x2="2.54" y2="22.86" width="0.1524" layer="91"/>
<wire x1="2.54" y1="22.86" x2="96.52" y2="22.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="22.86" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="9"/>
<wire x1="96.52" y1="58.42" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<wire x1="104.14" y1="58.42" x2="104.14" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
