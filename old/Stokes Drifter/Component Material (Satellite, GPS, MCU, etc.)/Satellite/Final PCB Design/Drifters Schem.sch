<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="minicircuits">
<description>&lt;b&gt;RF/IF MICROWAVE COMPONENTS&lt;/b&gt;&lt;p&gt;
www.minicircuits.com&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DG983-1">
<description>DG983-1
Source: http://www.minicircuits.com .. HSWA2-30DR+.pdf</description>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.2032" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.2032" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.2032" layer="51"/>
<smd name="X" x="0" y="0" dx="1.78" dy="1.78" layer="1" stop="no"/>
<smd name="1" x="-1.775" y="1" dx="0.66" dy="0.31" layer="1" stop="no"/>
<smd name="2" x="-1.775" y="0.5" dx="0.66" dy="0.31" layer="1" stop="no"/>
<smd name="3" x="-1.775" y="0" dx="0.66" dy="0.31" layer="1" stop="no"/>
<smd name="4" x="-1.775" y="-0.5" dx="0.66" dy="0.31" layer="1" stop="no"/>
<smd name="5" x="-1.775" y="-1" dx="0.66" dy="0.31" layer="1" stop="no"/>
<smd name="6" x="-1" y="-1.775" dx="0.66" dy="0.31" layer="1" rot="R90" stop="no"/>
<smd name="7" x="-0.5" y="-1.775" dx="0.66" dy="0.31" layer="1" rot="R90" stop="no"/>
<smd name="8" x="0" y="-1.775" dx="0.66" dy="0.31" layer="1" rot="R90" stop="no"/>
<smd name="9" x="0.5" y="-1.775" dx="0.66" dy="0.31" layer="1" rot="R90" stop="no"/>
<smd name="10" x="1" y="-1.775" dx="0.66" dy="0.31" layer="1" rot="R90" stop="no"/>
<smd name="11" x="1.775" y="-1" dx="0.66" dy="0.31" layer="1" rot="R180" stop="no"/>
<smd name="12" x="1.775" y="-0.5" dx="0.66" dy="0.31" layer="1" rot="R180" stop="no"/>
<smd name="13" x="1.775" y="0" dx="0.66" dy="0.31" layer="1" rot="R180" stop="no"/>
<smd name="14" x="1.775" y="0.5" dx="0.66" dy="0.31" layer="1" rot="R180" stop="no"/>
<smd name="15" x="1.775" y="1" dx="0.66" dy="0.31" layer="1" rot="R180" stop="no"/>
<smd name="16" x="1" y="1.775" dx="0.66" dy="0.31" layer="1" rot="R270" stop="no"/>
<smd name="17" x="0.5" y="1.775" dx="0.66" dy="0.31" layer="1" rot="R270" stop="no"/>
<smd name="18" x="0" y="1.775" dx="0.66" dy="0.31" layer="1" rot="R270" stop="no"/>
<smd name="19" x="-0.5" y="1.775" dx="0.66" dy="0.31" layer="1" rot="R270" stop="no"/>
<smd name="20" x="-1" y="1.775" dx="0.66" dy="0.31" layer="1" rot="R270" stop="no"/>
<rectangle x1="-2" y1="1.5" x2="-1.5" y2="2" layer="1"/>
<rectangle x1="-2.15" y1="0.8" x2="-1.4" y2="1.2" layer="29"/>
<rectangle x1="-2.15" y1="0.3" x2="-1.4" y2="0.7" layer="29"/>
<rectangle x1="-2.15" y1="-0.2" x2="-1.4" y2="0.2" layer="29"/>
<rectangle x1="-2.15" y1="-0.7" x2="-1.4" y2="-0.3" layer="29"/>
<rectangle x1="-2.15" y1="-1.2" x2="-1.4" y2="-0.8" layer="29"/>
<rectangle x1="-1.375" y1="-1.975" x2="-0.625" y2="-1.575" layer="29" rot="R90"/>
<rectangle x1="-0.875" y1="-1.975" x2="-0.125" y2="-1.575" layer="29" rot="R90"/>
<rectangle x1="-0.375" y1="-1.975" x2="0.375" y2="-1.575" layer="29" rot="R90"/>
<rectangle x1="0.125" y1="-1.975" x2="0.875" y2="-1.575" layer="29" rot="R90"/>
<rectangle x1="0.625" y1="-1.975" x2="1.375" y2="-1.575" layer="29" rot="R90"/>
<rectangle x1="1.4" y1="-1.2" x2="2.15" y2="-0.8" layer="29" rot="R180"/>
<rectangle x1="1.4" y1="-0.7" x2="2.15" y2="-0.3" layer="29" rot="R180"/>
<rectangle x1="1.4" y1="-0.2" x2="2.15" y2="0.2" layer="29" rot="R180"/>
<rectangle x1="1.4" y1="0.3" x2="2.15" y2="0.7" layer="29" rot="R180"/>
<rectangle x1="1.4" y1="0.8" x2="2.15" y2="1.2" layer="29" rot="R180"/>
<rectangle x1="0.625" y1="1.575" x2="1.375" y2="1.975" layer="29" rot="R270"/>
<rectangle x1="0.125" y1="1.575" x2="0.875" y2="1.975" layer="29" rot="R270"/>
<rectangle x1="-0.375" y1="1.575" x2="0.375" y2="1.975" layer="29" rot="R270"/>
<rectangle x1="-0.875" y1="1.575" x2="-0.125" y2="1.975" layer="29" rot="R270"/>
<rectangle x1="-1.375" y1="1.575" x2="-0.625" y2="1.975" layer="29" rot="R270"/>
<rectangle x1="-0.95" y1="-0.95" x2="0.95" y2="0.95" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="HSWA2-30DR+">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="RF1" x="-10.16" y="7.62" length="short" direction="pas"/>
<pin name="RF2" x="-10.16" y="2.54" length="short" direction="pas"/>
<pin name="RFCOM" x="10.16" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="C1" x="-10.16" y="-2.54" length="short" direction="pas"/>
<pin name="C2" x="-10.16" y="-5.08" length="short" direction="pas"/>
<pin name="VDD" x="10.16" y="-2.54" length="short" direction="in" rot="R180"/>
<pin name="GND" x="10.16" y="-5.08" length="short" direction="pwr" rot="R180"/>
</symbol>
<symbol name="GND14">
<wire x1="-17.78" y1="-2.54" x2="20.32" y2="-2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-2.54" x2="20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="20.32" y1="5.08" x2="-17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="-17.78" y1="5.08" x2="-17.78" y2="-2.54" width="0.254" layer="94"/>
<text x="-17.78" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND@1" x="-15.24" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@2" x="-12.7" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@3" x="-10.16" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@4" x="-7.62" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@5" x="-5.08" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@6" x="-2.54" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@7" x="0" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@8" x="2.54" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@9" x="5.08" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@10" x="7.62" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@11" x="10.16" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@12" x="12.7" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@13" x="15.24" y="-5.08" length="short" direction="pwr" rot="R90"/>
<pin name="GND@14" x="17.78" y="-5.08" length="short" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HSWA2-30DR+" prefix="IC">
<description>&lt;b&gt;SPDT RF SWITCH&lt;/b&gt; Absorptive RF Switch with internal driver&lt;p&gt;
Source: http://www.minicircuits.com .. HSWA2-30DR+.pdf</description>
<gates>
<gate name="G$1" symbol="HSWA2-30DR+" x="0" y="0"/>
<gate name="P" symbol="GND14" x="40.64" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="DG983-1">
<connects>
<connect gate="G$1" pin="C1" pad="17"/>
<connect gate="G$1" pin="C2" pad="16"/>
<connect gate="G$1" pin="GND" pad="X"/>
<connect gate="G$1" pin="RF1" pad="3"/>
<connect gate="G$1" pin="RF2" pad="13"/>
<connect gate="G$1" pin="RFCOM" pad="8"/>
<connect gate="G$1" pin="VDD" pad="20"/>
<connect gate="P" pin="GND@1" pad="1"/>
<connect gate="P" pin="GND@10" pad="12"/>
<connect gate="P" pin="GND@11" pad="14"/>
<connect gate="P" pin="GND@12" pad="15"/>
<connect gate="P" pin="GND@13" pad="18"/>
<connect gate="P" pin="GND@14" pad="19"/>
<connect gate="P" pin="GND@2" pad="2"/>
<connect gate="P" pin="GND@3" pad="4"/>
<connect gate="P" pin="GND@4" pad="5"/>
<connect gate="P" pin="GND@5" pad="6"/>
<connect gate="P" pin="GND@6" pad="7"/>
<connect gate="P" pin="GND@7" pad="9"/>
<connect gate="P" pin="GND@8" pad="10"/>
<connect gate="P" pin="GND@9" pad="11"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="STX3">
<packages>
<package name="STX3">
<smd name="1" x="-8.89" y="10.16" dx="2.032" dy="1.9304" layer="1"/>
<smd name="2" x="-8.89" y="7.62" dx="2.032" dy="1.9304" layer="1"/>
<smd name="3" x="-8.89" y="5.08" dx="2.032" dy="1.9304" layer="1"/>
<smd name="4" x="-8.89" y="2.54" dx="2.032" dy="1.9304" layer="1"/>
<smd name="5" x="-8.89" y="0" dx="2.032" dy="1.9304" layer="1"/>
<smd name="6" x="-8.89" y="-2.54" dx="2.032" dy="1.9304" layer="1"/>
<smd name="7" x="-8.89" y="-5.08" dx="2.032" dy="1.9304" layer="1"/>
<smd name="8" x="-8.89" y="-7.62" dx="2.032" dy="1.9304" layer="1"/>
<smd name="9" x="-8.89" y="-10.16" dx="2.032" dy="1.9304" layer="1"/>
<smd name="25" x="8.89" y="10.16" dx="2.032" dy="1.9304" layer="1"/>
<smd name="24" x="8.89" y="7.62" dx="2.032" dy="1.9304" layer="1"/>
<smd name="23" x="8.89" y="5.08" dx="2.032" dy="1.9304" layer="1"/>
<smd name="22" x="8.89" y="2.54" dx="2.032" dy="1.9304" layer="1"/>
<smd name="21" x="8.89" y="0" dx="2.032" dy="1.9304" layer="1"/>
<smd name="20" x="8.89" y="-2.54" dx="2.032" dy="1.9304" layer="1"/>
<smd name="19" x="8.89" y="-5.08" dx="2.032" dy="1.9304" layer="1"/>
<smd name="18" x="8.89" y="-7.62" dx="2.032" dy="1.9304" layer="1"/>
<smd name="17" x="8.89" y="-10.16" dx="2.032" dy="1.9304" layer="1"/>
<smd name="32" x="-7.62" y="12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="31" x="-5.08" y="12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="30" x="-2.54" y="12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="29" x="0" y="12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="28" x="2.54" y="12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="27" x="5.08" y="12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="26" x="7.62" y="12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="10" x="-7.62" y="-12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="11" x="-5.08" y="-12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="12" x="-2.54" y="-12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="16" x="7.62" y="-12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="15" x="5.08" y="-12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="14" x="2.54" y="-12.7" dx="2.032" dy="1.9304" layer="1"/>
<smd name="13" x="0" y="-12.7" dx="2.032" dy="1.9304" layer="1"/>
<text x="-2.54" y="5.08" size="1.27" layer="21">STX3</text>
</package>
<package name="GSANTENNA">
<polygon width="1" layer="1">
<vertex x="-35" y="20"/>
<vertex x="0" y="20"/>
<vertex x="10" y="10"/>
<vertex x="10" y="-20"/>
<vertex x="-25" y="-20"/>
<vertex x="-35" y="-10"/>
</polygon>
<pad name="P$1" x="0" y="0" drill="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="STX3">
<pin name="CTS" x="-7.62" y="30.48" length="middle" rot="R270"/>
<pin name="RTS" x="-17.78" y="30.48" length="middle" rot="R270"/>
<pin name="NC" x="-30.48" y="27.94" length="middle"/>
<pin name="NC2" x="-30.48" y="25.4" length="middle"/>
<pin name="NC3" x="-30.48" y="22.86" length="middle"/>
<pin name="NC4" x="-30.48" y="20.32" length="middle"/>
<pin name="VRF" x="5.08" y="5.08" length="middle" rot="R90"/>
<pin name="GND" x="2.54" y="5.08" length="middle" rot="R90"/>
<pin name="GND2" x="0" y="5.08" length="middle" rot="R90"/>
<pin name="GND3" x="-2.54" y="5.08" length="middle" rot="R90"/>
<pin name="GND4" x="-5.08" y="5.08" length="middle" rot="R90"/>
<pin name="GND5" x="-7.62" y="5.08" length="middle" rot="R90"/>
<pin name="GND6" x="-10.16" y="5.08" length="middle" rot="R90"/>
<pin name="RFOUT" x="-12.7" y="5.08" length="middle" rot="R90"/>
<pin name="GND7" x="-15.24" y="5.08" length="middle" rot="R90"/>
<pin name="GND8" x="-17.78" y="5.08" length="middle" rot="R90"/>
<pin name="GND9" x="-20.32" y="5.08" length="middle" rot="R90"/>
<pin name="GND10" x="-22.86" y="5.08" length="middle" rot="R90"/>
<pin name="GND11" x="-25.4" y="5.08" length="middle" rot="R90"/>
<pin name="GND12" x="-5.08" y="30.48" length="middle" rot="R270"/>
<pin name="NC5" x="-30.48" y="17.78" length="middle"/>
<pin name="NC6" x="-30.48" y="15.24" length="middle"/>
<pin name="NC7" x="-30.48" y="12.7" length="middle"/>
<pin name="PWR_EN" x="7.62" y="15.24" length="middle" rot="R180"/>
<pin name="NC8" x="-30.48" y="10.16" length="middle"/>
<pin name="TXD" x="-15.24" y="30.48" length="middle" rot="R270"/>
<pin name="RXD" x="-12.7" y="30.48" length="middle" rot="R270"/>
<pin name="TEST2" x="7.62" y="25.4" length="middle" rot="R180"/>
<pin name="TEST1" x="7.62" y="22.86" length="middle" rot="R180"/>
<pin name="PGE" x="7.62" y="20.32" length="middle" rot="R180"/>
<pin name="RESET" x="7.62" y="17.78" length="middle" rot="R180"/>
<pin name="VDIG" x="-10.16" y="30.48" length="middle" rot="R270"/>
<wire x1="-30.48" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="30.48" width="0.254" layer="94"/>
<wire x1="7.62" y1="30.48" x2="-30.48" y2="30.48" width="0.254" layer="94"/>
<wire x1="-30.48" y1="30.48" x2="-30.48" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="GSANTENNA">
<pin name="RF_IN" x="0" y="0" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STX3" uservalue="yes">
<description>GlobalStar STX3 Satellite Transmitter Module</description>
<gates>
<gate name="G$1" symbol="STX3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="STX3">
<connects>
<connect gate="G$1" pin="CTS" pad="1"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="GND10" pad="18"/>
<connect gate="G$1" pin="GND11" pad="19"/>
<connect gate="G$1" pin="GND12" pad="20"/>
<connect gate="G$1" pin="GND2" pad="9"/>
<connect gate="G$1" pin="GND3" pad="10"/>
<connect gate="G$1" pin="GND4" pad="11"/>
<connect gate="G$1" pin="GND5" pad="12"/>
<connect gate="G$1" pin="GND6" pad="13"/>
<connect gate="G$1" pin="GND7" pad="15"/>
<connect gate="G$1" pin="GND8" pad="16"/>
<connect gate="G$1" pin="GND9" pad="17"/>
<connect gate="G$1" pin="NC" pad="3"/>
<connect gate="G$1" pin="NC2" pad="4"/>
<connect gate="G$1" pin="NC3" pad="5"/>
<connect gate="G$1" pin="NC4" pad="6"/>
<connect gate="G$1" pin="NC5" pad="21"/>
<connect gate="G$1" pin="NC6" pad="22"/>
<connect gate="G$1" pin="NC7" pad="23"/>
<connect gate="G$1" pin="NC8" pad="25"/>
<connect gate="G$1" pin="PGE" pad="30"/>
<connect gate="G$1" pin="PWR_EN" pad="24"/>
<connect gate="G$1" pin="RESET" pad="31"/>
<connect gate="G$1" pin="RFOUT" pad="14"/>
<connect gate="G$1" pin="RTS" pad="2"/>
<connect gate="G$1" pin="RXD" pad="27"/>
<connect gate="G$1" pin="TEST1" pad="29"/>
<connect gate="G$1" pin="TEST2" pad="28"/>
<connect gate="G$1" pin="TXD" pad="26"/>
<connect gate="G$1" pin="VDIG" pad="32"/>
<connect gate="G$1" pin="VRF" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GSANTENNA">
<gates>
<gate name="G$1" symbol="GSANTENNA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GSANTENNA">
<connects>
<connect gate="G$1" pin="RF_IN" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Drifters">
<packages>
<package name="MCX-PCB">
<smd name="P$1" x="0" y="1.5" dx="1" dy="1" layer="1"/>
<smd name="RF" x="0" y="-1.5" dx="1" dy="1" layer="1"/>
<smd name="P$3" x="-1.45" y="0" dx="1.05" dy="2" layer="1"/>
<smd name="P$4" x="1.45" y="0" dx="1.05" dy="2" layer="1"/>
<wire x1="-1.4" y1="1.4" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.9" x2="-1.4" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-1.3" x2="-1.3" y2="-1.4" width="0.127" layer="51"/>
<wire x1="-1.3" y1="-1.4" x2="1.4" y2="-1.4" width="0.127" layer="51"/>
<wire x1="1.4" y1="-1.4" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="1.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="1.4" y1="1.4" x2="0.3" y2="1.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="1.4" x2="-1.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="1.45" x2="-0.3" y2="1.55" width="0.127" layer="51"/>
<wire x1="-0.3" y1="1.55" x2="0.3" y2="1.55" width="0.127" layer="51"/>
<wire x1="0.3" y1="1.55" x2="0.3" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.55" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.55" y1="0.9" x2="-1.55" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.55" y1="-0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.5" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.9" x2="1.5" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-2.3" y1="2.3" x2="-2.3" y2="-2.3" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-2.3" x2="2.3" y2="-2.3" width="0.127" layer="21"/>
<wire x1="2.3" y1="-2.3" x2="2.3" y2="2.3" width="0.127" layer="21"/>
<wire x1="2.3" y1="2.3" x2="-2.3" y2="2.3" width="0.127" layer="21"/>
<circle x="-2.8" y="2.05" radius="0.05" width="0.8128" layer="21"/>
<text x="2.55" y="0.85" size="1.27" layer="25">&gt;NAME</text>
<text x="2.6" y="-1.05" size="1" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MCX">
<pin name="GND@1" x="-8.89" y="0" length="middle" direction="pwr"/>
<pin name="GND@2" x="0" y="7.62" length="middle" direction="pwr" rot="R270"/>
<pin name="GND@3" x="10.16" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="RF" x="0" y="-5.08" length="middle" rot="R90"/>
<text x="-12.2" y="6.7" size="2.54" layer="95">&gt;NAME</text>
<text x="-12.2" y="4.16" size="2.032" layer="96">&gt;VALUE</text>
<wire x1="-1" y1="-3.62" x2="2.08" y2="-3.62" width="0.254" layer="94" curve="-270"/>
<wire x1="-4.08" y1="-3.54" x2="-4.064" y2="5.08" width="0.254" layer="94"/>
<wire x1="-4.064" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.16" y2="-3.58" width="0.254" layer="94"/>
<wire x1="5.16" y1="-3.58" x2="2.12" y2="-3.58" width="0.254" layer="94"/>
<wire x1="-4.08" y1="-3.54" x2="-1.04" y2="-3.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCX-THRU-PCB" prefix="RFCON" uservalue="yes">
<description>Design for PCB mounted MCX Jack.
http://www.molex.com/pdm_docs/sd/734120110_sd.pdf

Mouser: 538-73412-0110

Designed for FSU-FAMU COE ECE Senior Design Team #6 Stokes Drifter 2015
By Michael Prutsman</description>
<gates>
<gate name="G$1" symbol="MCX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MCX-PCB">
<connects>
<connect gate="G$1" pin="GND@1" pad="P$1"/>
<connect gate="G$1" pin="GND@2" pad="P$3"/>
<connect gate="G$1" pin="GND@3" pad="P$4"/>
<connect gate="G$1" pin="RF" pad="RF"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA12-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-14.605" y1="1.27" x2="-13.335" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="1.27" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-0.635" x2="-13.335" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-1.27" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-1.27" x2="-12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="1.27" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-0.635" x2="-14.605" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-1.27" x2="-14.605" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-1.27" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="13.335" y2="1.27" width="0.1524" layer="21"/>
<wire x1="13.335" y1="1.27" x2="14.605" y2="1.27" width="0.1524" layer="21"/>
<wire x1="14.605" y1="1.27" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-0.635" x2="14.605" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="14.605" y1="-1.27" x2="13.335" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="15.24" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-13.97" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-15.24" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-14.351" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="13.335" y="1.651" size="1.27" layer="21" ratio="10">12</text>
<text x="1.397" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
</package>
<package name="MA04-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.635" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.334" y="-0.635" size="1.27" layer="21" ratio="10">4</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA12-1">
<wire x1="3.81" y1="-15.24" x2="-1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="15.24" x2="2.54" y2="15.24" width="0.6096" layer="94"/>
<wire x1="1.27" y1="12.7" x2="2.54" y2="12.7" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="17.78" x2="-1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="17.78" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="17.78" x2="3.81" y2="17.78" width="0.4064" layer="94"/>
<text x="-1.27" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="18.542" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="7.62" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MA04-1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA12-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA12-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA12-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA04-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA04-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="minicircuits" deviceset="HSWA2-30DR+" device=""/>
<part name="U$1" library="STX3" deviceset="STX3" device=""/>
<part name="RFCON1" library="Drifters" deviceset="MCX-THRU-PCB" device=""/>
<part name="U$2" library="STX3" deviceset="GSANTENNA" device=""/>
<part name="TO-DUE" library="con-lstb" deviceset="MA12-1" device=""/>
<part name="POWERPINS" library="con-lstb" deviceset="MA04-1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="101.6" y="22.86" rot="R180"/>
<instance part="U$1" gate="G$1" x="17.78" y="22.86" rot="R270"/>
<instance part="RFCON1" gate="G$1" x="101.6" y="73.66"/>
<instance part="U$2" gate="G$1" x="127" y="15.24" rot="R180"/>
<instance part="TO-DUE" gate="1" x="53.34" y="-12.7" rot="R90"/>
<instance part="POWERPINS" gate="1" x="81.28" y="-12.7" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<wire x1="45.72" y1="33.02" x2="48.26" y2="33.02" width="0.1524" layer="91"/>
<wire x1="48.26" y1="33.02" x2="81.28" y2="33.02" width="0.1524" layer="91"/>
<wire x1="81.28" y1="33.02" x2="81.28" y2="25.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="25.4" x2="81.28" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-5.08" x2="81.28" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="81.28" y1="25.4" x2="91.44" y2="25.4" width="0.1524" layer="91"/>
<junction x="81.28" y="25.4"/>
<pinref part="IC1" gate="G$1" pin="VDD"/>
<junction x="91.44" y="25.4"/>
<wire x1="91.44" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<pinref part="POWERPINS" gate="1" pin="3"/>
<junction x="81.28" y="-5.08"/>
<pinref part="U$1" gate="G$1" pin="VDIG"/>
<junction x="48.26" y="33.02"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="109.22" y1="27.94" x2="111.76" y2="27.94" width="0.1524" layer="91"/>
<wire x1="111.76" y1="27.94" x2="114.3" y2="27.94" width="0.1524" layer="91"/>
<wire x1="114.3" y1="27.94" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
<wire x1="114.3" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="43.18" x2="53.34" y2="2.54" width="0.1524" layer="91"/>
<wire x1="53.34" y1="2.54" x2="48.26" y2="2.54" width="0.1524" layer="91"/>
<wire x1="48.26" y1="2.54" x2="48.26" y2="-5.08" width="0.1524" layer="91"/>
<junction x="48.26" y="-5.08"/>
<pinref part="IC1" gate="G$1" pin="C2"/>
<junction x="111.76" y="27.94"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="C1"/>
<wire x1="111.76" y1="25.4" x2="116.84" y2="25.4" width="0.1524" layer="91"/>
<wire x1="116.84" y1="25.4" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
<wire x1="116.84" y1="45.72" x2="50.8" y2="45.72" width="0.1524" layer="91"/>
<wire x1="50.8" y1="45.72" x2="50.8" y2="5.08" width="0.1524" layer="91"/>
<wire x1="50.8" y1="5.08" x2="45.72" y2="5.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="5.08" x2="45.72" y2="-5.08" width="0.1524" layer="91"/>
<junction x="45.72" y="-5.08"/>
<junction x="111.76" y="25.4"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="RFCON1" gate="G$1" pin="GND@3"/>
<wire x1="111.76" y1="73.66" x2="109.22" y2="73.66" width="0.1524" layer="91"/>
<wire x1="109.22" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<wire x1="111.76" y1="73.66" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<wire x1="114.3" y1="73.66" x2="114.3" y2="83.82" width="0.1524" layer="91"/>
<wire x1="88.9" y1="83.82" x2="101.6" y2="83.82" width="0.1524" layer="91"/>
<wire x1="101.6" y1="83.82" x2="114.3" y2="83.82" width="0.1524" layer="91"/>
<wire x1="88.9" y1="83.82" x2="88.9" y2="73.66" width="0.1524" layer="91"/>
<wire x1="88.9" y1="73.66" x2="88.9" y2="35.56" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-10.16" x2="78.74" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-5.08" x2="78.74" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-2.54" x2="78.74" y2="7.62" width="0.1524" layer="91"/>
<wire x1="78.74" y1="7.62" x2="17.78" y2="7.62" width="0.1524" layer="91"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<wire x1="17.78" y1="20.32" x2="22.86" y2="20.32" width="0.1524" layer="91"/>
<wire x1="22.86" y1="20.32" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<wire x1="17.78" y1="20.32" x2="17.78" y2="22.86" width="0.1524" layer="91"/>
<junction x="17.78" y="20.32"/>
<wire x1="17.78" y1="22.86" x2="22.86" y2="22.86" width="0.1524" layer="91"/>
<wire x1="22.86" y1="22.86" x2="25.4" y2="22.86" width="0.1524" layer="91"/>
<wire x1="17.78" y1="22.86" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<junction x="17.78" y="22.86"/>
<wire x1="17.78" y1="25.4" x2="22.86" y2="25.4" width="0.1524" layer="91"/>
<wire x1="22.86" y1="25.4" x2="25.4" y2="25.4" width="0.1524" layer="91"/>
<wire x1="17.78" y1="25.4" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<junction x="17.78" y="25.4"/>
<wire x1="17.78" y1="27.94" x2="22.86" y2="27.94" width="0.1524" layer="91"/>
<wire x1="22.86" y1="27.94" x2="25.4" y2="27.94" width="0.1524" layer="91"/>
<wire x1="17.78" y1="27.94" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<junction x="17.78" y="27.94"/>
<wire x1="17.78" y1="30.48" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="30.48" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<wire x1="17.78" y1="30.48" x2="17.78" y2="33.02" width="0.1524" layer="91"/>
<junction x="17.78" y="30.48"/>
<wire x1="17.78" y1="33.02" x2="22.86" y2="33.02" width="0.1524" layer="91"/>
<wire x1="22.86" y1="33.02" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<junction x="17.78" y="33.02"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="38.1" width="0.1524" layer="91"/>
<wire x1="17.78" y1="38.1" x2="22.86" y2="38.1" width="0.1524" layer="91"/>
<wire x1="22.86" y1="38.1" x2="25.4" y2="38.1" width="0.1524" layer="91"/>
<wire x1="17.78" y1="38.1" x2="17.78" y2="40.64" width="0.1524" layer="91"/>
<junction x="17.78" y="38.1"/>
<wire x1="17.78" y1="40.64" x2="22.86" y2="40.64" width="0.1524" layer="91"/>
<wire x1="22.86" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="17.78" y1="40.64" x2="17.78" y2="43.18" width="0.1524" layer="91"/>
<junction x="17.78" y="40.64"/>
<wire x1="17.78" y1="43.18" x2="22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="22.86" y1="43.18" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<wire x1="17.78" y1="43.18" x2="17.78" y2="45.72" width="0.1524" layer="91"/>
<junction x="17.78" y="43.18"/>
<wire x1="17.78" y1="45.72" x2="22.86" y2="45.72" width="0.1524" layer="91"/>
<wire x1="22.86" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="17.78" y1="45.72" x2="17.78" y2="48.26" width="0.1524" layer="91"/>
<junction x="17.78" y="45.72"/>
<wire x1="17.78" y1="48.26" x2="22.86" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
<junction x="22.86" y="20.32"/>
<pinref part="U$1" gate="G$1" pin="GND2"/>
<junction x="22.86" y="22.86"/>
<pinref part="U$1" gate="G$1" pin="GND3"/>
<junction x="22.86" y="25.4"/>
<pinref part="U$1" gate="G$1" pin="GND4"/>
<junction x="22.86" y="27.94"/>
<pinref part="U$1" gate="G$1" pin="GND5"/>
<junction x="22.86" y="30.48"/>
<pinref part="U$1" gate="G$1" pin="GND6"/>
<junction x="22.86" y="33.02"/>
<pinref part="U$1" gate="G$1" pin="GND7"/>
<junction x="22.86" y="38.1"/>
<pinref part="U$1" gate="G$1" pin="GND8"/>
<junction x="22.86" y="40.64"/>
<pinref part="U$1" gate="G$1" pin="GND9"/>
<junction x="22.86" y="43.18"/>
<pinref part="U$1" gate="G$1" pin="GND10"/>
<junction x="22.86" y="45.72"/>
<pinref part="U$1" gate="G$1" pin="GND11"/>
<junction x="22.86" y="48.26"/>
<wire x1="22.86" y1="48.26" x2="25.4" y2="48.26" width="0.1524" layer="91"/>
<wire x1="93.98" y1="27.94" x2="91.44" y2="27.94" width="0.1524" layer="91"/>
<wire x1="91.44" y1="27.94" x2="86.36" y2="27.94" width="0.1524" layer="91"/>
<wire x1="86.36" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="27.94" x2="78.74" y2="7.62" width="0.1524" layer="91"/>
<junction x="78.74" y="7.62"/>
<wire x1="88.9" y1="35.56" x2="86.36" y2="35.56" width="0.1524" layer="91"/>
<wire x1="86.36" y1="35.56" x2="86.36" y2="27.94" width="0.1524" layer="91"/>
<junction x="86.36" y="27.94"/>
<junction x="109.22" y="73.66"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<junction x="91.44" y="27.94"/>
<pinref part="POWERPINS" gate="1" pin="4"/>
<junction x="78.74" y="-5.08"/>
<wire x1="45.72" y1="27.94" x2="48.26" y2="27.94" width="0.1524" layer="91"/>
<wire x1="48.26" y1="27.94" x2="48.26" y2="12.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="12.7" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<junction x="17.78" y="12.7"/>
<pinref part="U$1" gate="G$1" pin="GND12"/>
<junction x="48.26" y="27.94"/>
<wire x1="78.74" y1="-2.54" x2="83.82" y2="-2.54" width="0.1524" layer="91"/>
<junction x="78.74" y="-2.54"/>
<wire x1="83.82" y1="-2.54" x2="83.82" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="POWERPINS" gate="1" pin="2"/>
<junction x="83.82" y="-5.08"/>
<wire x1="83.82" y1="-5.08" x2="83.82" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="88.9" y1="73.66" x2="96.52" y2="73.66" width="0.1524" layer="91"/>
<junction x="88.9" y="73.66"/>
<junction x="96.52" y="73.66"/>
<wire x1="101.6" y1="83.82" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<junction x="101.6" y="83.82"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="RFCON1" gate="G$1" pin="RF"/>
<wire x1="101.6" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="68.58" x2="127" y2="20.32" width="0.1524" layer="91"/>
<wire x1="127" y1="20.32" x2="111.76" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="RF2"/>
<junction x="111.76" y="20.32"/>
<wire x1="111.76" y1="20.32" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
<junction x="101.6" y="68.58"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="109.22" y1="15.24" x2="111.76" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="RF1"/>
<junction x="111.76" y="15.24"/>
<wire x1="111.76" y1="15.24" x2="127" y2="15.24" width="0.1524" layer="91"/>
<junction x="127" y="15.24"/>
<pinref part="U$2" gate="G$1" pin="RF_IN"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="25.4" y1="35.56" x2="22.86" y2="35.56" width="0.1524" layer="91"/>
<wire x1="22.86" y1="35.56" x2="12.7" y2="35.56" width="0.1524" layer="91"/>
<wire x1="12.7" y1="35.56" x2="12.7" y2="55.88" width="0.1524" layer="91"/>
<wire x1="12.7" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="83.82" y1="55.88" x2="83.82" y2="17.78" width="0.1524" layer="91"/>
<wire x1="83.82" y1="17.78" x2="91.44" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="RFCOM"/>
<junction x="91.44" y="17.78"/>
<wire x1="91.44" y1="17.78" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="RFOUT"/>
<junction x="22.86" y="35.56"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="45.72" y1="40.64" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="63.5" y1="40.64" x2="63.5" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="RTS"/>
<junction x="48.26" y="40.64"/>
<pinref part="TO-DUE" gate="1" pin="2"/>
<junction x="63.5" y="-5.08"/>
<wire x1="63.5" y1="-5.08" x2="63.5" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="45.72" y1="30.48" x2="48.26" y2="30.48" width="0.1524" layer="91"/>
<wire x1="48.26" y1="30.48" x2="66.04" y2="30.48" width="0.1524" layer="91"/>
<wire x1="66.04" y1="30.48" x2="66.04" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="CTS"/>
<junction x="48.26" y="30.48"/>
<pinref part="TO-DUE" gate="1" pin="1"/>
<junction x="66.04" y="-5.08"/>
<wire x1="66.04" y1="-5.08" x2="66.04" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="45.72" y1="38.1" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="48.26" y1="38.1" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="60.96" y1="38.1" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="TXD"/>
<junction x="48.26" y="38.1"/>
<pinref part="TO-DUE" gate="1" pin="3"/>
<junction x="60.96" y="-5.08"/>
<wire x1="60.96" y1="-5.08" x2="60.96" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="58.42" y1="35.56" x2="58.42" y2="33.02" width="0.1524" layer="91"/>
<wire x1="45.72" y1="35.56" x2="48.26" y2="35.56" width="0.1524" layer="91"/>
<wire x1="48.26" y1="35.56" x2="58.42" y2="35.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="33.02" x2="58.42" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="RXD"/>
<junction x="48.26" y="35.56"/>
<pinref part="TO-DUE" gate="1" pin="4"/>
<junction x="58.42" y="-5.08"/>
<wire x1="58.42" y1="-5.08" x2="58.42" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="VRF"/>
<wire x1="22.86" y1="17.78" x2="12.7" y2="17.78" width="0.1524" layer="91"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="0" width="0.1524" layer="91"/>
<wire x1="12.7" y1="0" x2="86.36" y2="0" width="0.1524" layer="91"/>
<wire x1="86.36" y1="0" x2="86.36" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="POWERPINS" gate="1" pin="1"/>
<junction x="86.36" y="-5.08"/>
<wire x1="86.36" y1="-5.08" x2="86.36" y2="-10.16" width="0.1524" layer="91"/>
<junction x="22.86" y="17.78"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="33.02" y1="17.78" x2="33.02" y2="10.16" width="0.1524" layer="91"/>
<wire x1="33.02" y1="10.16" x2="55.88" y2="10.16" width="0.1524" layer="91"/>
<wire x1="55.88" y1="10.16" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="TO-DUE" gate="1" pin="5"/>
<junction x="55.88" y="-5.08"/>
<wire x1="55.88" y1="-5.08" x2="55.88" y2="-10.16" width="0.1524" layer="91"/>
<junction x="33.02" y="17.78"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="35.56" y1="-5.08" x2="35.56" y2="17.78" width="0.1524" layer="91"/>
<junction x="35.56" y="17.78"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
