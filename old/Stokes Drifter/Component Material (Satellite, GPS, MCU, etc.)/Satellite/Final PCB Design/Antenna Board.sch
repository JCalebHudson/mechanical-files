<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-coax">
<description>&lt;b&gt;Coax Connectors&lt;/b&gt;&lt;p&gt;
Radiall  and M/A COM.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SMA-SMD">
<description>&lt;b&gt;SMA 50 Ohm Straight Jack Receptacle - Surface Mount&lt;/b&gt;&lt;p&gt;
www.johnsoncomponents.com&lt;br&gt;
Source:  http://catalog.digikey.com .. 142-0711-201, 205, 202, 206.pdf</description>
<wire x1="-1.1" y1="3.2" x2="1.1" y2="3.2" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-3.2" x2="-1.1" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="-3.1999" y1="3.1999" x2="3.2" y2="3.2" width="0.2032" layer="51"/>
<wire x1="3.2" y1="3.2" x2="3.1999" y2="-3.1999" width="0.2032" layer="51"/>
<wire x1="3.1999" y1="-3.1999" x2="-3.2" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-3.2" x2="-3.1999" y2="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="1.73" dy="1.73" layer="1" roundness="100"/>
<smd name="2@2" x="-2.375" y="-2.6" dx="2.35" dy="1.9" layer="1"/>
<smd name="2@1" x="-2.375" y="2.6" dx="2.35" dy="1.9" layer="1"/>
<smd name="2@5" x="-2.8" y="0" dx="1.5" dy="3.4" layer="1"/>
<smd name="2@4" x="2.375" y="2.6" dx="2.35" dy="1.9" layer="1" rot="R180"/>
<smd name="2@3" x="2.375" y="-2.6" dx="2.35" dy="1.9" layer="1" rot="R180"/>
<smd name="2@6" x="2.8" y="0" dx="1.5" dy="3.4" layer="1" rot="R180"/>
<text x="-2.54" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BU-BNC">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.254" x2="-0.762" y2="0.254" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.254" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.254" x2="-2.54" y2="-0.254" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMA-SMD" prefix="X">
<description>&lt;b&gt;SMA 50 Ohm Straight Jack Receptacle - Surface Mount&lt;/b&gt;&lt;p&gt;
www.johnsoncomponents.com&lt;br&gt;
Source:  http://catalog.digikey.com .. 142-0711-201, 205, 202, 206.pdf</description>
<gates>
<gate name="G$1" symbol="BU-BNC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMA-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2@1 2@2 2@3 2@4 2@5 2@6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Drifters">
<packages>
<package name="MCX-PCB">
<smd name="P$1" x="0" y="1.5" dx="1" dy="1" layer="1"/>
<smd name="RF" x="0" y="-1.5" dx="1" dy="1" layer="1"/>
<smd name="P$3" x="-1.45" y="0" dx="1.05" dy="2" layer="1"/>
<smd name="P$4" x="1.45" y="0" dx="1.05" dy="2" layer="1"/>
<wire x1="-1.4" y1="1.4" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.9" x2="-1.4" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-1.3" x2="-1.3" y2="-1.4" width="0.127" layer="51"/>
<wire x1="-1.3" y1="-1.4" x2="1.4" y2="-1.4" width="0.127" layer="51"/>
<wire x1="1.4" y1="-1.4" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="1.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="1.4" y1="1.4" x2="0.3" y2="1.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="1.4" x2="-1.4" y2="1.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="1.45" x2="-0.3" y2="1.55" width="0.127" layer="51"/>
<wire x1="-0.3" y1="1.55" x2="0.3" y2="1.55" width="0.127" layer="51"/>
<wire x1="0.3" y1="1.55" x2="0.3" y2="1.4" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.55" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.55" y1="0.9" x2="-1.55" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.55" y1="-0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.5" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.9" x2="1.5" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-2.3" y1="2.3" x2="-2.3" y2="-2.3" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-2.3" x2="2.3" y2="-2.3" width="0.127" layer="21"/>
<wire x1="2.3" y1="-2.3" x2="2.3" y2="2.3" width="0.127" layer="21"/>
<wire x1="2.3" y1="2.3" x2="-2.3" y2="2.3" width="0.127" layer="21"/>
<circle x="-2.8" y="2.05" radius="0.05" width="0.8128" layer="21"/>
<text x="2.55" y="0.85" size="1.27" layer="25">&gt;NAME</text>
<text x="2.6" y="-1.05" size="1" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MCX">
<pin name="GND@1" x="-8.89" y="0" length="middle" direction="pwr"/>
<pin name="GND@2" x="0" y="7.62" length="middle" direction="pwr" rot="R270"/>
<pin name="GND@3" x="10.16" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="RF" x="0" y="-5.08" length="middle" rot="R90"/>
<text x="-12.2" y="6.7" size="2.54" layer="95">&gt;NAME</text>
<text x="-12.2" y="4.16" size="2.032" layer="96">&gt;VALUE</text>
<wire x1="-1" y1="-3.62" x2="2.08" y2="-3.62" width="0.254" layer="94" curve="-270"/>
<wire x1="-4.08" y1="-3.54" x2="-4.064" y2="5.08" width="0.254" layer="94"/>
<wire x1="-4.064" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.16" y2="-3.58" width="0.254" layer="94"/>
<wire x1="5.16" y1="-3.58" x2="2.12" y2="-3.58" width="0.254" layer="94"/>
<wire x1="-4.08" y1="-3.54" x2="-1.04" y2="-3.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCX-THRU-PCB" prefix="RFCON" uservalue="yes">
<description>Design for PCB mounted MCX Jack.
http://www.molex.com/pdm_docs/sd/734120110_sd.pdf

Mouser: 538-73412-0110

Designed for FSU-FAMU COE ECE Senior Design Team #6 Stokes Drifter 2015
By Michael Prutsman</description>
<gates>
<gate name="G$1" symbol="MCX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MCX-PCB">
<connects>
<connect gate="G$1" pin="GND@1" pad="P$1"/>
<connect gate="G$1" pin="GND@2" pad="P$3"/>
<connect gate="G$1" pin="GND@3" pad="P$4"/>
<connect gate="G$1" pin="RF" pad="RF"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="STX3">
<packages>
<package name="GSANTENNA">
<polygon width="1" layer="1">
<vertex x="-35" y="20"/>
<vertex x="0" y="20"/>
<vertex x="10" y="10"/>
<vertex x="10" y="-20"/>
<vertex x="-25" y="-20"/>
<vertex x="-35" y="-10"/>
</polygon>
<pad name="P$1" x="0" y="0" drill="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="GSANTENNA">
<pin name="RF_IN" x="0" y="0" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GSANTENNA">
<gates>
<gate name="G$1" symbol="GSANTENNA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GSANTENNA">
<connects>
<connect gate="G$1" pin="RF_IN" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="con-coax" deviceset="SMA-SMD" device=""/>
<part name="RFCON1" library="Drifters" deviceset="MCX-THRU-PCB" device=""/>
<part name="U$1" library="STX3" deviceset="GSANTENNA" device=""/>
<part name="JP1" library="pinhead" deviceset="PINHD-1X1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="X1" gate="G$1" x="0" y="27.94"/>
<instance part="RFCON1" gate="G$1" x="0" y="43.18" rot="R90"/>
<instance part="U$1" gate="G$1" x="30.48" y="35.56"/>
<instance part="JP1" gate="G$1" x="15.24" y="53.34"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="2.54" y1="43.18" x2="5.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="5.08" y1="43.18" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<wire x1="25.4" y1="43.18" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<wire x1="25.4" y1="35.56" x2="30.48" y2="35.56" width="0.1524" layer="91"/>
<wire x1="30.48" y1="35.56" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<wire x1="0" y1="27.94" x2="2.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="2.54" y1="27.94" x2="25.4" y2="27.94" width="0.1524" layer="91"/>
<wire x1="25.4" y1="27.94" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<junction x="25.4" y="35.56"/>
<pinref part="RFCON1" gate="G$1" pin="RF"/>
<junction x="5.08" y="43.18"/>
<pinref part="X1" gate="G$1" pin="1"/>
<junction x="2.54" y="27.94"/>
<pinref part="U$1" gate="G$1" pin="RF_IN"/>
<junction x="30.48" y="35.56"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<pinref part="RFCON1" gate="G$1" pin="GND@3"/>
<wire x1="12.7" y1="53.34" x2="0" y2="53.34" width="0.1524" layer="91"/>
<wire x1="0" y1="53.34" x2="-7.62" y2="53.34" width="0.1524" layer="91"/>
<junction x="0" y="53.34"/>
<pinref part="RFCON1" gate="G$1" pin="GND@2"/>
<wire x1="-7.62" y1="53.34" x2="-7.62" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="43.18" x2="-7.62" y2="35.56" width="0.1524" layer="91"/>
<junction x="-7.62" y="43.18"/>
<wire x1="0" y1="35.56" x2="-7.62" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="35.56" x2="-7.62" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="22.86" x2="2.54" y2="22.86" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="2.54" y1="22.86" x2="2.54" y2="25.4" width="0.1524" layer="91"/>
<junction x="-7.62" y="35.56"/>
<junction x="0" y="35.56"/>
<junction x="2.54" y="25.4"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
