Microcontroller
===============
## ATmega168P ##
The ATmega168P was chosen because it is similar to and cheaper than
the ATmega328. Because the ATmega328P is the chip used in popular
Arduino boards, there are large number of libraries available that
make working with it easier. The ATmega168P is almost identical (only
having less memory) ts the ATmega328P, so most Arduino libraries
should work on it. The chip I would like to use is the ATtiny841, but
it is not available in PDIP packaging, and a breakout board is not
readily available. I think we will require a crystal to effectively
operate UART.
## USBasp ##
The USBasp is a 10 pin ISP programmer. It is available from many
Chinese sellers on ebay for about $2.
## MCU Comparisons ##
    |-------------+------+------------+-----------+------------+----------+-----------+----------------|
    | Part name   | pins | flash (Kb) | SRAM (Kb) | EEPROM (b) | cost (1) | cost (50) | PDIP available |
    |-------------+------+------------+-----------+------------+----------+-----------+----------------|
    | ATtiny441   |   14 |          4 |      0.25 |        256 |      1.4 |      0.99 | no             |
    | ATtiny841   |   14 |          8 |      0.25 |        512 |     1.52 |      1.07 | no             |
    | ATtiny2313A |   20 |          2 |      0.12 |        128 |     1.42 |      1.00 | yes            |
    | ATtiny4313  |   20 |          4 |      0.25 |        256 |     1.64 |      1.15 | yes            |
    | ATmega168PA |   32 |         16 |         1 |        512 |     3.01 |      2.11 | yes            |
    | ATmega328P  |   32 |         32 |         2 |       1024 |     3.43 |      2.41 | yes            |
    |-------------+------+------------+-----------+------------+----------+-----------+----------------|
    
ATmegaXXXPB series are backward code and mostly pin compatable with
ATmegaXXXPA. They just four extra GPIO, are cheaper, can wake from
UART, and support Atmel QTouch.

    |-------------+------+------------+-----------+------------+----------+------------+----------|
    | Part name   | GPIO | flash (Kb) | SRAM (Kb) | EEPROM (b) | cost (1) | cost (100) | V_CC min |
    |-------------+------+------------+-----------+------------+----------+------------+----------|
    | ATmega48PB  |   27 |          4 |       0.5 |        256 |     1.45 |       0.96 |      1.8 |
    | ATmega88PB  |   27 |          8 |         1 |        512 |     1.59 |       1.05 |      1.8 |
    | ATmega168PB |   27 |         16 |         1 |        512 |     1.76 |       1.24 |      1.8 |
    |-------------+------+------------+-----------+------------+----------+------------+----------|
    
    |-------------------------+--------+------+-------+------+-------|
    | ATmegaXXXPA             |        |      |       |      |       |
    |-------------------------+--------+------+-------+------+-------|
    | Condition               | f      | V_CC |   typ |  max | units |
    |-------------------------+--------+------+-------+------+-------|
    | Active                  | 1 MHz  |    2 |   0.2 | 0.55 | mA    |
    | Active                  | 4 MHz  |    3 |   1.2 |  3.5 | mA    |
    | Idle                    | 1 MHz  |    2 |  0.03 |  0.5 | mA    |
    | Idle                    | 4 MHz  |    3 |  0.21 |  1.5 | mA    |
    | Power-save              | 32 kHz |  1.8 |  0.75 |      | uA    |
    | Power-save              | 32 kHz |    3 |   0.9 |      | uA    |
    | Power-down, WDT         |        |    3 |   3.9 |   15 | uA    |
    | Power-down, no WDT      |        |    3 |   0.1 |    2 | uA    |
    |-------------------------+--------+------+-------+------+-------|
    |-------------------------+--------+------+-------+------+-------|
    | ATmega48PB/88PB, T=85°C |        |      |       |      |       |
    |-------------------------+--------+------+-------+------+-------|
    | Condition               | f      | V_CC |   typ |  max | units |
    |-------------------------+--------+------+-------+------+-------|
    | Active                  | 1 MHz  |    2 |  0.21 |  0.5 | mA    |
    | Active                  | 4 MHz  |    3 |  1.27 |  2.5 | mA    |
    | Idle                    | 1 MHz  |    2 | 0.035 | 0.15 | mA    |
    | Idle                    | 4 MHz  |    3 |  0.22 |  0.7 | mA    |
    | Power-save              | 32 kHz |  1.8 |  1.42 |      | uA    |
    | Power-save              | 32 kHz |    3 |  1.62 |      | uA    |
    | Power-down, WDT         |        |    3 |  2.62 |    8 | uA    |
    | Power-down, no WDT      |        |    3 |  0.53 |    2 | uA    |
    |-------------------------+--------+------+-------+------+-------|

Orientation sensor
==================
## MMA8452Q accelerometer ##
This part was chosen was by the previous team. It is cheap (~$1), and
we already have one on a breakout board.  It communicates with the
microcontroller on an i2c interface.  Tilt switches were investigated,
but were not much cheaper and require time to settle, making them
unsuited to use in the ocean. I see no reason to change this part.

I looked for an accelerometer in a QFP, because that would be easier to solder,
but I couldn't find one.

    |----------+----------+------------+------------+----------|
    | Name     | cost (1) | cost (100) | cost (500) | V_CC min |
    |----------+----------+------------+------------+----------|
    | MMA8452Q |     1.43 |       1.15 |       1.02 |     1.95 |
    |----------+----------+------------+------------+----------|
    
I_DD varies with OCR. Read datasheet for more information.

Temperature sensor
==================
## LMT87LP ##
This is an analog temperature sensor chosen by the previous team. The
drifter will have a temperature sensor on each side. However, the one
on on the "top" should not be trusted because it will be in direct
sunlight.

    |-----------+---------+-----------+------------+------------+----------+--------------+--------------|
    | Name      |precs.   |  cost (1) | cost (100) | cost (500) | V_CC min | min I_S (uA) | max I_S (uA) |
    |-----------+---------+-----------+------------+------------+----------+--------------+--------------|
    | LMT87LP   |  0.3  C |      1.00 |       0.70 |       0.53 |      2.7 |          5.4 |          8.1 |
    |-----------+---------+-----------+------------+------------+----------+--------------+--------------|
    | LMT70YFQT |  0.05 C |      3.19 |       1.73 |       1.33 |      2.0 |         -0.5 |          0.5 |
    |-----------+---------+-----------+------------+------------+----------+--------------+--------------|

## Thermistor ##
Thermistors are temperature sensitive resistors. They can achieve good
precision, but will require tuning. A simple voltage divider can be used to
measure the temperature. Precision resistors and thermistors can be somewhat
expensive.

Power supply
============
The previous team decided to have the device completely battery
powered. The downside to this approach is that the battery will have
to last for the entire three months at sea, making the device larger
and more expensive.  A solar powered device will require a much
smaller battery. Charging circuitry will have to be designed to charge
the battery. The complexity and efficiency of charging circuitry can
vary quite a bit.  The previous team calculated that the device needed
60Ah for its entire voyage.

GPS
===
NMEA is an easy to understand data format. 

    |--------------+----------+------------+----------+----------------+---------------+-----------+--------------+---------------+---------+--------------------------------------------------------------|
    | Name         | cost (1) | cost (100) | V_CC min | searching (mA) | tracking (mA) | Peak (mA) | standby (uA) | cold TTFF (s) | Package | notes                                                        |
    |--------------+----------+------------+----------+----------------+---------------+-----------+--------------+---------------+---------+--------------------------------------------------------------|
    | 2A2200-A     |    13.60 |      11.69 |      3.0 |             52 |            41 |        69 |          325 |            35 | module  | * requires external antenna                                  |
    |              |          |            |          |                |               |           |              |               |         | * outputs NMEA formatted data                                |
    |              |          |            |          |                |               |           |              |               |         | * i2c, spi, or UART interfaces available                     |
    |--------------+----------+------------+----------+----------------+---------------+-----------+--------------+---------------+---------+--------------------------------------------------------------|
    | A2235-H      |    14.42 |      13.27 |      3.0 |             36 |            22 |        42 |          900 |            38 | module  | * Used by last team                                          |
    |              |          |            |          |                |               |           |              |               |         | * Has onboard patch antenna and support for external antenna |
    |              |          |            |          |                |               |           |              |               |         | * outputs NMEA formatted data                                |
    |              |          |            |          |                |               |           |              |               |         | * i2c, spi, or UART interfaces available                     |
    |--------------+----------+------------+----------+----------------+---------------+-----------+--------------+---------------+---------+--------------------------------------------------------------|
    | RXM-GPS-RM-B |    19.34 |      18.34 |      3.0 |             14 |            12 |        44 |          135 |            32 | module  | * Good documentation (and explanation of NMEA format)        |
    |              |          |            |          |                |               |           |              |               |         | * Requires antenna (active or passive)                       |
    |              |          |            |          |                |               |           |              |               |         | * UART interface                                             |
    |--------------+----------+------------+----------+----------------+---------------+-----------+--------------+---------------+---------+--------------------------------------------------------------|

    
Satellite modem
===============
## Globalstar STX3 ##
The satellite modem transmits data to Globalstar's LEO satellite
network.

    |------+---------+------------+----------+---------+---------------+-------------+--------------+------------|
    | Name | cost(1) | cost (100) | VDIG min | VRF min | transmit (mA) | active (mA) | standby (uA) | sleep (uA) |
    |------+---------+------------+----------+---------+---------------+-------------+--------------+------------|
    | STX3 |         |            |      2.0 |     3.0 |           325 |         2.3 |           12 |          8 |
    |------+---------+------------+----------+---------+---------------+-------------+--------------+------------|

Antennas
========
The GPS and modem will each require two antennas, one on each side
(top and bottom) of the drifter. A dual wavelength antenna is
available. Each antenna will need a ~60x60mm ground plane. 1615 MHz
antennas must be special ordered from the factory, so prices listed
below may be inaccurate.

    |------------------+----------+------------+-----------+--------------------|
    | Name             | cost (1) | cost (100) | cost(500) | Frequency          |
    |------------------+----------+------------+-----------+--------------------|
    | PA251615025SALF  |          |       5.99 |      5.15 | 1615 MHz           |
    | PA45-1615-1575SA |          |      16.67 |     14.80 | 1615 MHz, 1575 Mhz |
    |------------------+----------+------------+-----------+--------------------|

## PA25-1615-025SA ##
It is a patch antenna that operates at 1615MHz, with 25MHz bandwidth,
3dB gain, and LHCP polarization. It requires a 60x60mm ground plane.

## RF switches##
We will need an RF switch to switch between the two antennas

### HSWA2-30DR+ RF switch ###
This device can used to switch between antennas. Order from
http://www.minicircuits.com

    |-------------+----------+-----------+------------+----------+---------------+---------------+---------|
    | Name        | cost (1) | cost (50) | cost (100) | V_CC min | I_DD typ (uA) | I_DD max (uA) | package |
    |-------------+----------+-----------+------------+----------+---------------+---------------+---------|
    | HSWA2-30DR+ |          |      2.45 |       1.95 |      2.7 |             8 | 20*           | QFN     |
    |-------------+----------+-----------+------------+----------+---------------+---------------+---------|
    | HMC574AMS8E |     6.35 |      5.20 |       4.69 |      3.0 |           0.5 |               | MSOP-8  |
    |-------------+----------+-----------+------------+----------+---------------+---------------+---------|
    * At control frequency of 1kHz. 56 uA at 50kHz

HSWA2-30DR+ is too small. Look at MASWSS0176TR-3000.



Multiplexer/demultiplexer
=========================
The GPS and the satellite modem both communicate over UART. Because the
microcontroller only has one UART interface, we will need a multiplexer/
demultiplexer to switch between the two. This should not be an issue
because they do not need to operate at the same time. While the
microcontroller has UART hardware, flow control is left to the programmer.

    |---------------+----------+------------+---------------+----------+--------------------+-------------------|
    | Name          | cost (1) | cost (100) | Configuration | V_CC min | I_CC/I_DD typ (uA) | I_CC/IDD max (uA) |
    |---------------+----------+------------+---------------+----------+--------------------+-------------------|
    | SN74LVC1G3157 |     0.41 |      0.160 |         1x1:2 |     1.65 |                0.1 |                10 |
    | 74LVC1G53     |     0.42 |      0.202 |         1x1:2 |     1.65 |                0.1 |                10 |
    | TC4W53FU      |     0.50 |      0.185 |         1x1:2 |        3 |               0.01 |                10 |
    | SN74CBTLV3253 |     0.92 |      0.568 |         2x1:4 |      2.3 |                  1 |                10 |
    | FSAL200       |     1.30 |      0.801 |         2x1:4 |      3.0 |                    |                 1 |
    |---------------+----------+------------+---------------+----------+--------------------+-------------------|
 
EEPROM
======
It may be desirable to have extra memory. With extra memory, we may be
able to store more data and compress it so that we will not need to
transmit as often. Devices that communicate on i2c are cheap and
plentiful.

    |----------+----------+------------+-----------+----------+---------------------+---------------------+--------------------+--------------------+---------------------+---------------------+------------|
    | Name     | cost (1) | cost (100) | size (Kb) | V_CC min | I_CC stdby typ (uA) | I_CC stdby max (uA) | I_CC read typ (mA) | I_CC read max (mA) | I_CC write typ (mA) | I_CC write max (mA) | I_CCS (uA) |
    |----------+----------+------------+-----------+----------+---------------------+---------------------+--------------------+--------------------+---------------------+---------------------+------------|
    | 24AA04   |     0.27 |       0.20 |         4 |      1.7 |                     |                     |               0.05 |                  1 |                 0.1 |                   3 |       0.01 |
    | 24AA08   |     0.29 |       0.22 |         8 |      1.8 |                     |                     |               0.05 |                  1 |                 0.1 |                   3 |       0.01 |
    | 24AA16   |     0.32 |       0.24 |        16 |      1.8 |                     |                     |               0.01 |                  1 |                     |                   3 |       0.01 |
    | 24LC32AT |     0.39 |       0.30 |        32 |      1.7 |                     |                     |               0.05 |                  1 |                 0.1 |                   3 |       0.01 |
    | 24AA64T  |     0.44 |       0.33 |        64 |      1.7 |                0.01 |                   1 |               0.05 |                0.4 |                 0.1 |                   3 |       0.01 |
    |----------+----------+------------+-----------+----------+---------------------+---------------------+--------------------+--------------------+---------------------+---------------------+------------|

Transistors Switches
====================
Tranistor switches will be need to switch components on and off to reduce power consumption.

    |-------------+----------+------------+------------+-----------+---------+-------------|
    | Name        | cost (1) | cost (100) | cost (500) | IDSS (uA) | Vgs(th) | I_D max (A) |
    |-------------+----------+------------+------------+-----------+---------+-------------|
    | NTR4003NT3G |     0.27 |       0.12 |       0.08 |       1.0 |    1.4V |       0.40  |
    |-------------+----------+------------+------------+-----------+---------+-------------|


Batteries
========
We need a batteries able to supply 2.7Ah.

    |----------------+----------+------------+------------+------------+--------------------+---------------------+--------------------+-------------------+----------------|
    | Name           | cost (1) | cost (100) | Length(mm) | Height(mm) | Max. Discharge(mA) |           Weight(g) |       Capacity(Ah) | Output Voltage(V) |  Rechargeable? |
    |----------------+----------+------------+------------+------------+--------------------+---------------------+--------------------+-------------------+----------------|
    |UHE-ER14505-s   |     3.56 |       2.75 |       50.5 |       14.5 |              100mA |               17.75 |                2.4 |               3.6 |             No |              
    |----------------+----------+------------+------------+------------+--------------------+---------------------+--------------------+-------------------+----------------| 
    


Hull
====
The drifter will be enclosed in a plastic hull. It should be
transparent and not interfere with the satellite or GPS signals.
