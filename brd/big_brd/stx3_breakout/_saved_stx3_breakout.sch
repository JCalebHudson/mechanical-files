EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:stx3_breakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L STX3 MOD1
U 1 1 577EB848
P 5550 3050
F 0 "MOD1" H 5450 2400 60  0000 C CNN
F 1 "STX3" H 5950 2400 60  0000 C CNN
F 2 "globalstar:STX3" H 6650 3150 60  0001 C CNN
F 3 "" H 6650 3150 60  0000 C CNN
F 4 "Satellite modem" H 5700 3700 60  0000 C CNN "Function"
	1    5550 3050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 577EB907
P 6350 3700
F 0 "#PWR01" H 6350 3450 50  0001 C CNN
F 1 "GND" H 6350 3550 50  0000 C CNN
F 2 "" H 6350 3700 50  0000 C CNN
F 3 "" H 6350 3700 50  0000 C CNN
	1    6350 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2500 6350 3700
Connection ~ 6350 2600
Connection ~ 6350 2700
Connection ~ 6350 2800
Connection ~ 6350 2900
Connection ~ 6350 3000
Connection ~ 6350 3100
Connection ~ 6350 3200
Connection ~ 6350 3300
Connection ~ 6350 3400
Connection ~ 6350 3500
Connection ~ 6350 3600
Wire Wire Line
	4550 3450 4350 3450
Wire Wire Line
	4550 2700 4550 3450
Wire Wire Line
	4550 2700 5100 2700
Wire Wire Line
	4350 3350 4500 3350
Wire Wire Line
	4500 3350 4500 2600
Wire Wire Line
	4500 2600 5100 2600
Wire Wire Line
	4350 3250 4450 3250
Wire Wire Line
	4450 3250 4450 2500
Wire Wire Line
	4450 2500 5100 2500
Wire Wire Line
	4350 3150 4600 3150
Wire Wire Line
	4600 3150 4600 3600
Wire Wire Line
	4600 3600 5100 3600
Wire Wire Line
	4350 3050 4650 3050
Wire Wire Line
	4650 3050 4650 3500
Wire Wire Line
	4650 3500 5100 3500
Wire Wire Line
	4350 2950 4700 2950
Wire Wire Line
	4700 2950 4700 3300
Wire Wire Line
	4700 3300 5100 3300
Wire Wire Line
	4350 2850 4750 2850
Wire Wire Line
	4750 2850 4750 3200
Wire Wire Line
	4750 3200 5100 3200
Wire Wire Line
	4350 2750 4800 2750
Wire Wire Line
	4800 2750 4800 3100
Wire Wire Line
	4800 3100 5100 3100
Wire Wire Line
	4350 2650 4850 2650
Wire Wire Line
	4850 2650 4850 3000
Wire Wire Line
	4850 3000 5100 3000
Wire Wire Line
	4350 2550 4950 2550
Wire Wire Line
	4950 2550 4950 2900
Wire Wire Line
	4950 2900 5100 2900
$Comp
L CONN_01X11 P1
U 1 1 577EC020
P 4150 3050
F 0 "P1" H 4150 3650 50  0000 C CNN
F 1 "CONN_01X11" V 4250 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x11" H 4150 3050 50  0001 C CNN
F 3 "" H 4150 3050 50  0000 C CNN
	1    4150 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 3600 6550 3600
Wire Wire Line
	6550 3600 6550 4000
Wire Wire Line
	6550 4000 5200 4000
Wire Wire Line
	5200 4000 5200 3700
Wire Wire Line
	5200 3700 4450 3700
Wire Wire Line
	4450 3700 4450 3550
Wire Wire Line
	4450 3550 4350 3550
$EndSCHEMATC
