EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:drifter-rescue
LIBS:drifter-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 4
Title "Stokes Drifter"
Date "2017-02-15"
Rev "v2.2"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X03 P7
U 1 1 5894F3DA
P 4400 1850
F 0 "P7" H 4400 2050 50  0000 C CNN
F 1 "CONN_01X03" V 4500 1850 50  0000 C CNN
F 2 "lib:JST_ZH-3pos-top-through_hole" H 4400 1850 50  0001 C CNN
F 3 "http://www.jst-mfg.com/product/pdf/eng/eZH.pdf" H 4400 1850 50  0001 C CNN
F 4 "probe1 connector" H 4400 1850 60  0001 C CNN "Function"
F 5 "JST" H 4400 1850 60  0001 C CNN "MFG"
F 6 "S3B-ZR(LF)(SN)" H 4400 1850 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=218787382&uq=636228409335947958" H 4400 1850 60  0001 C CNN "Buy"
F 8 "Requires mating connector and crimp terminals" H 4400 1850 60  0001 C CNN "Notes"
	1    4400 1850
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 5894F3EC
P 3550 2000
F 0 "R19" V 3650 1950 50  0000 C CNN
F 1 "10k" V 3550 2000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3480 2000 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C237.pdf" H 3550 2000 50  0001 C CNN
F 4 "probe1 voltage divider" V 3550 2000 60  0001 C CNN "Function"
F 5 "Panasonic Electronic Components" V 3550 2000 60  0001 C CNN "MFG"
F 6 "ERA-3ARB103V" V 3550 2000 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/ERA-3ARB103V/P10KBDCT-ND/3073245" V 3550 2000 60  0001 C CNN "Buy"
F 8 "0.1% accurate,  low temperature drift" V 3550 2000 60  0001 C CNN "Notes"
	1    3550 2000
	-1   0    0    1   
$EndComp
Text Notes 2800 1550 0    60   ~ 0
Drive VDivide low to\nuse voltage divider\n(for thermistor\ntemperature probes)
$Comp
L GND #PWR040
U 1 1 5894F3F5
P 3700 2900
F 0 "#PWR040" H 3700 2650 50  0001 C CNN
F 1 "GND" H 3700 2750 50  0000 C CNN
F 2 "" H 3700 2900 50  0000 C CNN
F 3 "" H 3700 2900 50  0000 C CNN
	1    3700 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR041
U 1 1 5894F3FB
P 3700 2250
F 0 "#PWR041" H 3700 2000 50  0001 C CNN
F 1 "GND" H 3700 2100 50  0000 C CNN
F 2 "" H 3700 2250 50  0000 C CNN
F 3 "" H 3700 2250 50  0000 C CNN
	1    3700 2250
	1    0    0    -1  
$EndComp
Text HLabel 3000 1850 0    60   Input ~ 0
Probe1
Text HLabel 3000 2450 0    60   Input ~ 0
Probe2
Text HLabel 3000 2950 0    60   Input ~ 0
VDivide
$Comp
L R R17
U 1 1 5898FA34
P 3050 4950
F 0 "R17" V 3150 4950 50  0000 C CNN
F 1 "22k" V 3050 4950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2980 4950 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3050 4950 50  0001 C CNN
F 4 "battery voltage divider (top)" V 3050 4950 60  0001 C CNN "Function"
F 5 "Yageo" V 3050 4950 60  0001 C CNN "MFG"
F 6 "RC0603FR-0722KL" V 3050 4950 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-0722KL/311-22.0KHRCT-ND/730003" V 3050 4950 60  0001 C CNN "Buy"
	1    3050 4950
	-1   0    0    1   
$EndComp
$Comp
L R R18
U 1 1 5898FA3C
P 3050 5350
F 0 "R18" V 3150 5350 50  0000 C CNN
F 1 "5.1k" V 3050 5350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2980 5350 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3050 5350 50  0001 C CNN
F 4 "battery voltage divider (bottom)" V 3050 5350 60  0001 C CNN "Function"
F 5 "Yageo" V 3050 5350 60  0001 C CNN "MFG"
F 6 "RC0603FR-075K1L" V 3050 5350 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-075K1L/311-5.10KHRCT-ND/730215" V 3050 5350 60  0001 C CNN "Buy"
	1    3050 5350
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR042
U 1 1 5898FA44
P 3050 5600
F 0 "#PWR042" H 3050 5350 50  0001 C CNN
F 1 "GND" H 3050 5450 50  0000 C CNN
F 2 "" H 3050 5600 50  0000 C CNN
F 3 "" H 3050 5600 50  0000 C CNN
	1    3050 5600
	1    0    0    -1  
$EndComp
Text HLabel 2500 5150 0    60   Input ~ 0
Batt_health
Text GLabel 7500 3500 2    60   Input ~ 0
PWR_EN
Text GLabel 3200 3950 2    60   Input ~ 0
VBATT
Text GLabel 4050 1000 2    60   Input ~ 0
PWR_EN
Wire Wire Line
	3550 2150 3100 2150
Connection ~ 3100 2750
Wire Wire Line
	3550 2750 3100 2750
Connection ~ 3550 1850
Connection ~ 3550 2750
Wire Wire Line
	3700 2550 3700 2900
Connection ~ 3550 2150
Wire Wire Line
	3100 2150 3100 2950
Connection ~ 3550 2450
Wire Wire Line
	3000 2450 4200 2450
Wire Wire Line
	3000 1850 4200 1850
Wire Wire Line
	3250 2800 3700 2800
Connection ~ 3700 2800
Wire Wire Line
	3250 2500 3250 2450
Connection ~ 3250 2450
Wire Wire Line
	3250 1900 3250 1850
Connection ~ 3250 1850
Wire Wire Line
	3250 2200 3700 2200
Wire Wire Line
	3100 2950 3000 2950
Wire Wire Line
	3050 5500 3050 5600
Connection ~ 3050 5150
Wire Wire Line
	3050 5100 3050 5200
Wire Wire Line
	3050 4750 3050 4800
Wire Wire Line
	2500 5150 3050 5150
Connection ~ 2650 5150
Wire Wire Line
	2650 5550 3050 5550
Connection ~ 3050 5550
Wire Wire Line
	3050 3950 3050 4350
Wire Wire Line
	3050 3950 3200 3950
Wire Wire Line
	4200 2550 3700 2550
Wire Wire Line
	4200 1950 3700 1950
Wire Wire Line
	3700 1950 3700 2250
Wire Wire Line
	4000 1000 4000 2350
Wire Wire Line
	4000 1000 4050 1000
Wire Wire Line
	4000 2350 4200 2350
Wire Wire Line
	4200 1750 4000 1750
Connection ~ 4000 1750
Connection ~ 3700 2200
$Comp
L AP3012 IC5
U 1 1 589AEB06
P 8300 2050
F 0 "IC5" H 8500 2250 60  0000 C CNN
F 1 "AP3012" H 8100 2250 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 8500 2450 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/AP3012KTR-E1/AP3012KTR-E1DI-ND/4470845" H 8500 2450 60  0001 C CNN
F 4 "26V adjustable SMPS" H 8300 2050 60  0001 C CNN "Function"
F 5 "Diodes Incorporated" H 8300 2050 60  0001 C CNN "MFG"
F 6 "AP3012KTR-G1" H 8300 2050 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/AP3012KTR-E1/AP3012KTR-E1DI-ND/4470845" H 8300 2050 60  0001 C CNN "Buy"
F 8 "Value" H 8300 2050 60  0001 C CNN "Notes"
	1    8300 2050
	-1   0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 589AEB0E
P 8400 1700
F 0 "L1" V 8350 1700 50  0000 C CNN
F 1 "10uH" V 8500 1700 50  0000 C CNN
F 2 "lib:CDRH2D11HP" V 8600 1400 50  0001 C CNN
F 3 "http://products.sumida.com/products/pdf/CDRH2D11HP.pdf" H 8400 1700 50  0001 C CNN
F 4 "buzzer inductor" V 8400 1700 60  0001 C CNN "Function"
F 5 "Sumida America Components Inc." V 8400 1700 60  0001 C CNN "MFG"
F 6 "CDRH2D11/HPNP-100NC" V 8400 1700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/sumida-america-components-inc/CDRH2D11-HPNP-100NC/308-1576-1-ND/2620848" V 8400 1700 60  0001 C CNN "Buy"
	1    8400 1700
	0    -1   -1   0   
$EndComp
$Comp
L R R13
U 1 1 589AEB2F
P 9200 1900
F 0 "R13" V 9300 1900 50  0000 C CNN
F 1 "20k" V 9200 1900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 9130 1900 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 9200 1900 50  0001 C CNN
F 4 "fix buzzer voltage" V 9200 1900 60  0001 C CNN "Function"
F 5 "Yageo" V 9200 1900 60  0001 C CNN "MFG"
F 6 "RC0603FR-0720KL" V 9200 1900 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-0720KL/311-20.0KHRCT-ND/729987" V 9200 1900 60  0001 C CNN "Buy"
	1    9200 1900
	-1   0    0    1   
$EndComp
Text Notes 8300 2850 0    60   ~ 0
Populate resistors OR pot to set voltage\nVout = 1.25*(1+R100/R200) < 29V\nR100 = R200*(Vout/1.25-1)
Wire Wire Line
	8950 2100 9250 2100
Wire Wire Line
	8800 2350 8850 2350
Wire Wire Line
	8050 2350 8500 2350
Wire Wire Line
	7750 2350 7850 2350
Wire Wire Line
	9150 1700 9900 1700
Wire Wire Line
	9400 1700 9400 1950
Wire Wire Line
	9200 1750 9200 1700
Connection ~ 9200 1700
Wire Wire Line
	9200 2050 9200 2150
Connection ~ 9200 2100
Wire Wire Line
	9400 2500 9400 2250
Wire Wire Line
	9200 2500 9200 2450
Wire Wire Line
	9700 1700 9700 1950
Connection ~ 9400 1700
Connection ~ 9700 2500
Connection ~ 9400 2500
Connection ~ 9700 1700
Wire Wire Line
	7650 2500 9900 2500
Wire Wire Line
	7650 1950 7800 1950
Connection ~ 7650 1700
Wire Wire Line
	8950 2100 8950 2150
Wire Wire Line
	8950 2150 8850 2150
Connection ~ 9200 2500
Wire Wire Line
	9700 2500 9700 2250
Connection ~ 8850 2500
Connection ~ 7650 1950
Wire Wire Line
	8850 1950 8900 1950
Wire Wire Line
	8900 1950 8900 1700
Connection ~ 8900 1700
Wire Wire Line
	8900 2050 8850 2050
Wire Wire Line
	8900 2500 8900 2050
Connection ~ 8900 2500
Wire Wire Line
	9900 1700 9900 1950
Wire Wire Line
	9900 1950 10000 1950
Wire Wire Line
	9900 2150 10000 2150
Wire Wire Line
	9900 2500 9900 2150
Text HLabel 7600 2050 0    60   Input ~ 0
Buzz
$Comp
L GND #PWR043
U 1 1 589AEB7C
P 7650 2600
F 0 "#PWR043" H 7650 2350 50  0001 C CNN
F 1 "GND" H 7650 2450 50  0000 C CNN
F 2 "" H 7650 2600 50  0000 C CNN
F 3 "" H 7650 2600 50  0000 C CNN
	1    7650 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1700 7650 1700
Wire Wire Line
	8700 1700 8950 1700
Wire Wire Line
	7750 2050 7750 2350
Connection ~ 7750 2150
Wire Wire Line
	7750 2150 7800 2150
Wire Wire Line
	7750 2050 7600 2050
Wire Wire Line
	7650 2450 7650 2600
Connection ~ 7650 2500
Wire Wire Line
	8850 2350 8850 2500
Text Label 9250 1700 0    60   ~ 0
buzz_voltage
Text Label 8100 2350 0    60   ~ 0
buzz_LED
$Comp
L Buzzer BZ1
U 1 1 589AEB8E
P 10100 2050
F 0 "BZ1" H 10250 2100 50  0000 L CNN
F 1 "Buzzer" H 10250 2000 50  0000 L CNN
F 2 "lib:9175-700_two-pin" V 10075 2150 50  0001 C CNN
F 3 "http://cirlw3yrzg4afc1x47lv2k18.wpengine.netdna-cdn.com/wp-content/uploads/2016/12/PB-2211-data.pdf" V 10075 2150 50  0001 C CNN
F 4 "Soberton Inc." H 10100 2050 60  0001 C CNN "MFG"
F 5 "Buzzer" H 10100 2050 60  0001 C CNN "Function"
F 6 "PB-2211" H 10100 2050 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329319&uq=636227649280904830" H 10100 2050 60  0001 C CNN "Buy"
	1    10100 2050
	1    0    0    -1  
$EndComp
Text Label 8950 2100 0    60   ~ 0
buzz_FB
Text Label 8850 1700 1    60   ~ 0
buzz_coil
Text GLabel 7750 1100 2    60   Input ~ 0
PWR_EN
Wire Wire Line
	7650 1100 7650 2150
Wire Wire Line
	7750 1100 7650 1100
Wire Wire Line
	7500 3500 7400 3500
$Comp
L POT RV1
U 1 1 589BDABF
P 8200 4250
F 0 "RV1" V 8025 4250 50  0000 C CNN
F 1 "POT" V 8100 4250 50  0000 C CNN
F 2 "lib:Bourns-TC33X" H 8200 4250 50  0001 C CNN
F 3 "http://www.bourns.com/docs/Product-Datasheets/TC33.pdf" H 8200 4250 50  0001 C CNN
F 4 "adjust noise level" V 8200 4250 60  0001 C CNN "Function"
F 5 "Bourns Inc." V 8200 4250 60  0001 C CNN "MFG"
F 6 "TC33X-2-503E" V 8200 4250 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/bourns-inc/TC33X-2-503E/TC33X-503ECT-ND/612915" V 8200 4250 60  0001 C CNN "Buy"
F 8 "DNP" V 8200 4250 60  0001 C CNN "Notes"
	1    8200 4250
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R11
U 1 1 589BDB58
P 8200 4500
F 0 "R11" H 8230 4520 50  0000 L CNN
F 1 "51k" H 8230 4460 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 8200 4500 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 8200 4500 50  0001 C CNN
F 4 "Yageo" H 8200 4500 60  0001 C CNN "MFG"
F 5 "RC0603FR-0751KL" H 8200 4500 60  0001 C CNN "part_num"
F 6 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-0751KL/311-51.0KHRCT-ND/730229" H 8200 4500 60  0001 C CNN "Buy"
	1    8200 4500
	0    1    1    0   
$EndComp
$Comp
L GND #PWR044
U 1 1 589BDF49
P 7400 4600
F 0 "#PWR044" H 7400 4350 50  0001 C CNN
F 1 "GND" H 7400 4450 50  0000 C CNN
F 2 "" H 7400 4600 50  0000 C CNN
F 3 "" H 7400 4600 50  0000 C CNN
	1    7400 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 4500 7400 4600
Connection ~ 7400 4250
Wire Wire Line
	7950 4250 8050 4250
Wire Wire Line
	8100 4500 8000 4500
Wire Wire Line
	8000 4500 8000 4250
Connection ~ 8000 4250
Wire Wire Line
	8350 4250 8500 4250
Wire Wire Line
	8400 4500 8300 4500
Wire Wire Line
	8400 4050 8400 4500
Connection ~ 8400 4250
Wire Wire Line
	8200 4100 8200 4050
Wire Wire Line
	8200 4050 8400 4050
Text GLabel 3700 5050 0    60   Input ~ 0
PWR_EN
Text HLabel 7700 3950 2    60   Input ~ 0
noise_out
Wire Wire Line
	7700 3950 7400 3950
Connection ~ 7400 3950
Text HLabel 8500 4250 2    60   Input ~ 0
noise_gen
Text Label 7650 1700 0    60   ~ 0
buzz_pwr
Text Label 4000 1550 0    60   ~ 0
sens_pwr
Text Label 7400 3500 2    60   ~ 0
noise_pwr
Wire Wire Line
	3350 4550 3550 4550
Wire Wire Line
	3450 4450 3450 4550
Connection ~ 3450 4550
Wire Wire Line
	3450 4150 3450 4100
Wire Wire Line
	3450 4100 3050 4100
Connection ~ 3050 4100
Wire Wire Line
	3750 4850 3750 5050
$Comp
L GND #PWR045
U 1 1 58A4D485
P 4450 5050
F 0 "#PWR045" H 4450 4800 50  0001 C CNN
F 1 "GND" H 4450 4900 50  0000 C CNN
F 2 "" H 4450 5050 50  0000 C CNN
F 3 "" H 4450 5050 50  0000 C CNN
	1    4450 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4550 4450 4550
Wire Wire Line
	4450 4550 4450 5050
Wire Wire Line
	3750 5050 3700 5050
Wire Wire Line
	4200 4900 4450 4900
Connection ~ 4450 4900
Wire Wire Line
	3900 4900 3750 4900
Connection ~ 3750 4900
$Comp
L Q_PMOS_GSD Q4
U 1 1 58A58E56
P 3150 4550
F 0 "Q4" H 3050 4700 50  0000 L CNN
F 1 "Q_PMOS_GSD" V 3400 4300 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3350 4650 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds31861.pdf" H 3150 4550 50  0001 C CNN
F 4 "Switch power to other parts" H 3150 4550 60  0001 C CNN "Function"
F 5 "Diodes Incorporated" H 3150 4550 60  0001 C CNN "MFG"
F 6 "DMG1013UW-7" H 3150 4550 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/diodes-incorporated/DMG1013UW-7/DMG1013UW-7DICT-ND/2183252" H 3150 4550 60  0001 C CNN "Buy"
	1    3150 4550
	-1   0    0    1   
$EndComp
$Comp
L POT RV2
U 1 1 58A59CCC
P 9400 2100
F 0 "RV2" V 9225 2100 50  0000 C CNN
F 1 "POT" V 9300 2100 50  0000 C CNN
F 2 "lib:Bourns-TC33X" H 9400 2100 50  0001 C CNN
F 3 "http://www.bourns.com/docs/Product-Datasheets/TC33.pdf" H 9400 2100 50  0001 C CNN
F 4 "Bourns Inc." V 9400 2100 60  0001 C CNN "MFG"
F 5 "TC33X-2-503E" V 9400 2100 60  0001 C CNN "part_num"
F 6 "http://www.digikey.com/product-detail/en/bourns-inc/TC33X-2-503E/TC33X-503ECT-ND/612915" V 9400 2100 60  0001 C CNN "Buy"
F 7 "DNP" V 9400 2100 60  0001 C CNN "Notes"
F 8 "adjust buzzer loudness" V 9400 2100 60  0001 C CNN "Function"
	1    9400 2100
	-1   0    0    1   
$EndComp
$Comp
L Led_Small_drifter D7
U 1 1 58A67872
P 7950 2350
F 0 "D7" H 8000 2300 50  0000 L CNN
F 1 "Led_Small_drifter" H 7700 2450 50  0001 L CNN
F 2 "lib:3.2x2.7mm_LED" V 7950 2350 50  0001 C CNN
F 3 "http://www.cree.com/~/media/Files/Cree/LED%20Components%20and%20Modules/HB/Data%20Sheets/CLM1BRKWAKW1084.pdf" V 7950 2350 50  0001 C CNN
F 4 "Buzzer LED indicator" H 7950 2350 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329314&uq=636227661034218865" H 7950 2350 60  0001 C CNN "Buy"
F 6 "DNP, Amber" H 7950 2350 60  0001 C CNN "Notes"
F 7 "Cree Inc." H 7950 2350 60  0001 C CNN "MFG"
F 8 "CLM1B-AKW-CUAVB253" H 7950 2350 60  0001 C CNN "part_num"
	1    7950 2350
	-1   0    0    1   
$EndComp
$Comp
L C C10
U 1 1 58A60DBB
P 2650 5350
F 0 "C10" V 2600 5200 50  0000 L CNN
F 1 "1uF" V 2700 5150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2688 5200 50  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" H 2650 5350 50  0001 C CNN
F 4 "batt health smoothing" V 2650 5350 60  0001 C CNN "Function"
F 5 "Yageo" V 2650 5350 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB105" V 2650 5350 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" V 2650 5350 60  0001 C CNN "Buy"
	1    2650 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 5200 2650 5150
Wire Wire Line
	2650 5500 2650 5550
$Comp
L C C11
U 1 1 58A61429
P 9700 2100
F 0 "C11" V 9650 1950 50  0000 L CNN
F 1 "1uF" V 9750 1900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 9738 1950 50  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" H 9700 2100 50  0001 C CNN
F 4 "26V SMPS smoothing" V 9700 2100 60  0001 C CNN "Field4"
F 5 "Yageo" V 9700 2100 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB105" V 9700 2100 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" V 9700 2100 60  0001 C CNN "Buy"
	1    9700 2100
	-1   0    0    1   
$EndComp
$Comp
L C_Small C26
U 1 1 58A66ED8
P 7400 4400
F 0 "C26" H 7410 4470 50  0000 L CNN
F 1 "100nF" H 7410 4320 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7400 4400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 7400 4400 50  0001 C CNN
F 4 "noise circuit" H 7400 4400 60  0001 C CNN "Function"
F 5 "Yageo" H 7400 4400 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 7400 4400 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 7400 4400 60  0001 C CNN "Buy"
	1    7400 4400
	1    0    0    -1  
$EndComp
$Comp
L C C35
U 1 1 58A6CC61
P 3250 2050
F 0 "C35" H 3100 2150 50  0000 L CNN
F 1 "4.7uF" H 3200 1850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3288 1900 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X5R_4V-to-50V_23.pdf" H 3250 2050 50  0001 C CNN
F 4 "Probe1 smoothing" V 3250 2050 60  0001 C CNN "Function"
F 5 "Yageo" V 3250 2050 60  0001 C CNN "MFG"
F 6 "CC0603KRX5R5BB475" V 3250 2050 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX5R5BB475/311-1521-1-ND/2833827" V 3250 2050 60  0001 C CNN "Buy"
	1    3250 2050
	1    0    0    -1  
$EndComp
$Comp
L C C36
U 1 1 58A6D2C5
P 3250 2650
F 0 "C36" H 3100 2750 50  0000 L CNN
F 1 "4.7uF" H 3200 2450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3288 2500 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X5R_4V-to-50V_23.pdf" H 3250 2650 50  0001 C CNN
F 4 "Probe2 smoothing" V 3250 2650 60  0001 C CNN "Function"
F 5 "Yageo" V 3250 2650 60  0001 C CNN "MFG"
F 6 "CC0603KRX5R5BB475" V 3250 2650 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX5R5BB475/311-1521-1-ND/2833827" V 3250 2650 60  0001 C CNN "Buy"
	1    3250 2650
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P8
U 1 1 58A7A4EA
P 4400 2450
F 0 "P8" H 4400 2650 50  0000 C CNN
F 1 "CONN_01X03" V 4500 2450 50  0000 C CNN
F 2 "lib:JST_ZH-3pos-top-through_hole" H 4400 2450 50  0001 C CNN
F 3 "http://www.jst-mfg.com/product/pdf/eng/eZH.pdf" H 4400 2450 50  0001 C CNN
F 4 "probe2 connector" H 4400 2450 60  0001 C CNN "Function"
F 5 "JST" H 4400 2450 60  0001 C CNN "MFG"
F 6 "S3B-ZR(LF)(SN)" H 4400 2450 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=218787382&uq=636228409335947958" H 4400 2450 60  0001 C CNN "Buy"
F 8 "Requires mating connector and crimp terminals" H 4400 2450 60  0001 C CNN "Notes"
	1    4400 2450
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 58A7DEFF
P 8650 2350
F 0 "R12" V 8750 2350 50  0000 C CNN
F 1 "120" V 8650 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 8580 2350 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 8650 2350 50  0001 C CNN
F 4 "LED resistor" V 8650 2350 60  0001 C CNN "Function"
F 5 "Yageo" V 8650 2350 60  0001 C CNN "MFG"
F 6 "RC0603JR-07120RL" V 8650 2350 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-07120RL/311-120GRCT-ND/729653" V 8650 2350 60  0001 C CNN "Buy"
	1    8650 2350
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 58A7E40F
P 7800 4250
F 0 "R10" V 7900 4250 50  0000 C CNN
F 1 "1k" V 7800 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7730 4250 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 7800 4250 50  0001 C CNN
F 4 "LED resistor" V 7800 4250 60  0001 C CNN "Function"
F 5 "Yageo" V 7800 4250 60  0001 C CNN "MFG"
F 6 "RC0603FR-071KL" V 7800 4250 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-071KL/311-1.00KHRCT-ND/729790" V 7800 4250 60  0001 C CNN "Buy"
	1    7800 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 4250 7650 4250
$Comp
L R R14
U 1 1 58A7EB4F
P 9200 2300
F 0 "R14" V 9300 2300 50  0000 C CNN
F 1 "1k" V 9200 2300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 9130 2300 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 9200 2300 50  0001 C CNN
F 4 "Fix buzzer voltage" V 9200 2300 60  0001 C CNN "Function"
F 5 "Yageo" V 9200 2300 60  0001 C CNN "MFG"
F 6 "RC0603FR-071KL" V 9200 2300 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-071KL/311-1.00KHRCT-ND/729790" V 9200 2300 60  0001 C CNN "Buy"
	1    9200 2300
	-1   0    0    1   
$EndComp
$Comp
L R R8
U 1 1 58A7F50E
P 7400 3700
F 0 "R8" V 7500 3700 50  0000 C CNN
F 1 "120" V 7400 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7330 3700 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 7400 3700 50  0001 C CNN
F 4 "Noise circuit" V 7400 3700 60  0001 C CNN "Function"
F 5 "Yageo" V 7400 3700 60  0001 C CNN "MFG"
F 6 "RC0603JR-07120RL" V 7400 3700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-07120RL/311-120GRCT-ND/729653" V 7400 3700 60  0001 C CNN "Buy"
	1    7400 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 3500 7400 3550
$Comp
L R R9
U 1 1 58A7FA42
P 7400 4100
F 0 "R9" V 7500 4100 50  0000 C CNN
F 1 "120" V 7400 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7330 4100 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 7400 4100 50  0001 C CNN
F 4 "Noise circuit" V 7400 4100 60  0001 C CNN "Function"
F 5 "Yageo" V 7400 4100 60  0001 C CNN "MFG"
F 6 "RC0603JR-07120RL" V 7400 4100 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-07120RL/311-120GRCT-ND/729653" V 7400 4100 60  0001 C CNN "Buy"
	1    7400 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 4250 7400 4300
Wire Wire Line
	7400 3950 7400 3850
$Comp
L R R25
U 1 1 58A851D0
P 3450 4300
F 0 "R25" H 3350 4150 50  0000 C CNN
F 1 "4.7k" V 3450 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3380 4300 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3450 4300 50  0001 C CNN
F 4 "PMOS pullup" V 3450 4300 60  0001 C CNN "Function"
F 5 "Yageo" V 3450 4300 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 3450 4300 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 3450 4300 60  0001 C CNN "Buy"
	1    3450 4300
	-1   0    0    1   
$EndComp
$Comp
L R R26
U 1 1 58A85A1D
P 4050 4900
F 0 "R26" V 3950 5050 50  0000 C CNN
F 1 "4.7k" V 4050 4900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3980 4900 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 4050 4900 50  0001 C CNN
F 4 "PMOS pulldown" V 4050 4900 60  0001 C CNN "Function"
F 5 "Yageo" V 4050 4900 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 4050 4900 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 4050 4900 60  0001 C CNN "Buy"
	1    4050 4900
	0    1    1    0   
$EndComp
$Comp
L Q_NMOS_GSD Q2
U 1 1 589B821B
P 3750 4650
F 0 "Q2" V 3750 4400 50  0000 L CNN
F 1 "Q_NMOS_GSD" V 4000 4350 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3950 4750 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/BSS316N_Rev2.3.pdf?folderId=db3a3043156fd573011622e10b5c1f67&fileId=db3a304330f686060130ff7ee4b07f16" H 3750 4650 50  0001 C CNN
F 4 "Drive Q4" V 3750 4650 60  0001 C CNN "Function"
F 5 "Diodes Incorporated" V 3750 4650 60  0001 C CNN "MFG"
F 6 "BSS316N" V 3750 4650 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/infineon-technologies/BSS316N-H6327/BSS316N-H6327CT-ND/3196652" V 3750 4650 60  0001 C CNN "Buy"
	1    3750 4650
	0    -1   -1   0   
$EndComp
$Comp
L R R20
U 1 1 58A86F80
P 3550 2600
F 0 "R20" V 3650 2550 50  0000 C CNN
F 1 "10k" V 3550 2600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3480 2600 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDA0000/AOA0000C237.pdf" H 3550 2600 50  0001 C CNN
F 4 "probe2 voltage divider" V 3550 2600 60  0001 C CNN "Function"
F 5 "Panasonic Electronic Components" V 3550 2600 60  0001 C CNN "MFG"
F 6 "ERA-3ARB103V" V 3550 2600 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/ERA-3ARB103V/P10KBDCT-ND/3073245" V 3550 2600 60  0001 C CNN "Buy"
F 8 "0.1% accurate,  low temperature drift" V 3550 2600 60  0001 C CNN "Notes"
	1    3550 2600
	-1   0    0    1   
$EndComp
$Comp
L C C3
U 1 1 58A7C445
P 7650 2300
F 0 "C3" H 7800 2250 50  0000 L CNN
F 1 "1uF" H 7800 2350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7688 2150 50  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" H 7650 2300 50  0001 C CNN
F 4 "Yageo" V 7650 2300 60  0001 C CNN "MFG"
F 5 "CC0603KRX7R7BB105" V 7650 2300 60  0001 C CNN "part_num"
F 6 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" V 7650 2300 60  0001 C CNN "Buy"
F 7 "Value" H 7650 2300 60  0001 C CNN "Notes"
F 8 "VREF decouple" V 7650 2300 60  0001 C CNN "Function"
	1    7650 2300
	-1   0    0    1   
$EndComp
$Comp
L D_Small D8
U 1 1 58A7D57A
P 9050 1700
F 0 "D8" H 9000 1600 50  0000 L CNN
F 1 "D_Small" H 8700 1750 50  0001 L CNN
F 2 "lib:DB2J317" V 9050 1700 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J317_E.pdf" V 9050 1700 50  0001 C CNN
F 4 "29V SMPS Diode" H 9050 1700 60  0001 C CNN "Function"
F 5 "Panasonic Electronic Components" H 9050 1700 60  0001 C CNN "MFG"
F 6 "DB2J31700L" H 9050 1700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/DB2J31700L/DB2J31700LCT-ND/2184712" H 9050 1700 60  0001 C CNN "Buy"
F 8 "Value" H 9050 1700 60  0001 C CNN "Notes"
	1    9050 1700
	-1   0    0    1   
$EndComp
$EndSCHEMATC
