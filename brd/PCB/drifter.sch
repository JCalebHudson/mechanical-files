EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:drifter-rescue
LIBS:drifter-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 4
Title "Stokes Drifter"
Date "2017-02-15"
Rev "v2.2"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X03 P5
U 1 1 57AB6DB3
P 2300 800
F 0 "P5" H 2300 1000 50  0000 C CNN
F 1 "ISP" H 2300 600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 2300 -400 50  0001 C CNN
F 3 "https://cdn.harwin.com/pdfs/Harwin_Product_Catalog_page_207.pdf" H 2300 -400 50  0001 C CNN
F 4 "Programming connector" H 2300 800 60  0001 C CNN "Function"
F 5 "Harwin Inc." H 2300 800 60  0001 C CNN "MFG"
F 6 "M20-9980346" H 2300 800 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/harwin-inc/M20-9980346/952-2121-ND/3728085" H 2300 800 60  0001 C CNN "Buy"
	1    2300 800 
	1    0    0    -1  
$EndComp
Text GLabel 2000 700  0    60   Input ~ 0
MISO
Text GLabel 2000 800  0    60   Input ~ 0
SCK
Text GLabel 2000 900  0    60   Input ~ 0
RST
Text GLabel 2600 800  2    60   Input ~ 0
MOSI
$Comp
L GND #PWR01
U 1 1 57AB6F2D
P 2600 950
F 0 "#PWR01" H 2600 700 50  0001 C CNN
F 1 "GND" H 2600 800 50  0000 C CNN
F 2 "" H 2600 950 50  0000 C CNN
F 3 "" H 2600 950 50  0000 C CNN
	1    2600 950 
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 57AB99A1
P 5400 3400
F 0 "R2" V 5350 3550 50  0000 C CNN
F 1 "4.7k" V 5400 3400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5330 3400 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 5400 3400 50  0001 C CNN
F 4 "programming resistor" V 5400 3400 60  0001 C CNN "Function"
F 5 "Yageo" V 5400 3400 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 5400 3400 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 5400 3400 60  0001 C CNN "Buy"
	1    5400 3400
	0    1    1    0   
$EndComp
$Comp
L cap C13
U 1 1 57AB9D61
P 5950 3700
F 0 "C13" H 5700 3650 60  0000 C CNN
F 1 "15pf" H 6050 3750 60  0000 C CNN
F 2 "Capacitors_SMD:C_0603" H 5950 3700 60  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/MLCC/UPY-GP_NP0_16V-to-50V_14.pdf" H 5950 3700 60  0001 C CNN
F 4 "clock cap" H 5950 3700 60  0001 C CNN "Function"
F 5 "Yageo" H 5950 3700 60  0001 C CNN "MFG"
F 6 "CC0603JRNPO9BN150" H 5950 3700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603JRNPO9BN150/311-1060-1-ND/302970" H 5950 3700 60  0001 C CNN "Buy"
	1    5950 3700
	-1   0    0    1   
$EndComp
Text GLabel 4900 3400 2    60   Input ~ 0
MOSI
Text GLabel 4900 3500 2    60   Input ~ 0
MISO
Text GLabel 4900 3600 2    60   Input ~ 0
SCK
Text GLabel 7450 4550 2    60   Input ~ 0
RST
$Comp
L GND #PWR02
U 1 1 57ABAE3B
P 2750 5400
F 0 "#PWR02" H 2750 5150 50  0001 C CNN
F 1 "GND" H 2750 5250 50  0000 C CNN
F 2 "" H 2750 5400 50  0000 C CNN
F 3 "" H 2750 5400 50  0000 C CNN
	1    2750 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 57ABB603
P 6100 4150
F 0 "#PWR03" H 6100 3900 50  0001 C CNN
F 1 "GND" H 6100 4000 50  0000 C CNN
F 2 "" H 6100 4150 50  0000 C CNN
F 3 "" H 6100 4150 50  0000 C CNN
	1    6100 4150
	1    0    0    -1  
$EndComp
Text GLabel 2250 3400 0    60   Input ~ 0
VREF
Text GLabel 6400 4700 2    60   Input ~ 0
GPS_tx
Text GLabel 6400 4800 2    60   Input ~ 0
GPS_rx
$Comp
L MMA8452Q IC1
U 1 1 57AC5B99
P 950 6850
F 0 "IC1" H 800 7600 60  0000 C CNN
F 1 "MMA8452Q" H 1000 6300 60  0000 C CNN
F 2 "lib:MMA8452Q" V 1300 6650 60  0000 C CNN
F 3 "http://www.nxp.com/assets/documents/data/en/data-sheets/MMA8452Q.pdf" H 950 6850 60  0001 C CNN
F 4 "accelerometer (Detect orientation)" V 1150 7050 60  0001 C CNN "Function"
F 5 "NXP USA Inc." H 950 6850 60  0001 C CNN "MFG"
F 6 "http://www.digikey.com/product-detail/en/nxp-usa-inc/MMA8452QR1/MMA8452QR1CT-ND/3524271" H 950 6850 60  0001 C CNN "Buy"
F 7 "MMA8452QR1" H 950 6850 60  0001 C CNN "part_num"
	1    950  6850
	-1   0    0    1   
$EndComp
Text GLabel 1650 7200 2    60   Input ~ 0
SDA
Text GLabel 1650 7300 2    60   Input ~ 0
SCL
Text GLabel 6600 4350 2    60   Input ~ 0
SDA
Text GLabel 6600 4450 2    60   Input ~ 0
SCL
NoConn ~ 1500 6900
$Comp
L GND #PWR04
U 1 1 57AC7217
P 2150 7550
F 0 "#PWR04" H 2150 7300 50  0001 C CNN
F 1 "GND" H 2150 7400 50  0000 C CNN
F 2 "" H 2150 7550 50  0000 C CNN
F 3 "" H 2150 7550 50  0000 C CNN
	1    2150 7550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 57B2050F
P 1500 2700
F 0 "#PWR05" H 1500 2450 50  0001 C CNN
F 1 "GND" H 1500 2550 50  0000 C CNN
F 2 "" H 1500 2700 50  0000 C CNN
F 3 "" H 1500 2700 50  0000 C CNN
	1    1500 2700
	1    0    0    -1  
$EndComp
$Comp
L D_Small D1
U 1 1 57B2114A
P 1600 1400
F 0 "D1" H 1450 1450 50  0000 L CNN
F 1 "D_Small" H 1250 1450 50  0001 L CNN
F 2 "lib:DB2J317" V 1600 1400 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J317_E.pdf" V 1600 1400 50  0001 C CNN
F 4 "Protection Diode" H 1600 1400 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/DB2J31700L/DB2J31700LCT-ND/2184712" H 1600 1400 60  0001 C CNN "Buy"
F 6 "Panasonic Electronic Components" H 1600 1400 60  0001 C CNN "MFG"
F 7 "DB2J31700L" H 1600 1400 60  0001 C CNN "part_num"
	1    1600 1400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 57B328D4
P 1350 4000
F 0 "#PWR06" H 1350 3750 50  0001 C CNN
F 1 "GND" H 1350 3850 50  0000 C CNN
F 2 "" H 1350 4000 50  0000 C CNN
F 3 "" H 1350 4000 50  0000 C CNN
	1    1350 4000
	1    0    0    -1  
$EndComp
Text GLabel 1650 7000 2    60   Input ~ 0
Accel_int
Text GLabel 5150 4900 2    60   Input ~ 0
Accel_int
Text GLabel 2750 4200 0    60   Input ~ 0
Buzz_EN
$Comp
L VCC #PWR07
U 1 1 57E19D4D
P 7000 5950
F 0 "#PWR07" H 7000 5800 50  0001 C CNN
F 1 "VCC" H 7000 6100 50  0000 C CNN
F 2 "" H 7000 5950 50  0000 C CNN
F 3 "" H 7000 5950 50  0000 C CNN
	1    7000 5950
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR08
U 1 1 582B7CC9
P 1350 3100
F 0 "#PWR08" H 1350 2950 50  0001 C CNN
F 1 "VCC" H 1350 3250 50  0000 C CNN
F 2 "" H 1350 3100 50  0000 C CNN
F 3 "" H 1350 3100 50  0000 C CNN
	1    1350 3100
	1    0    0    -1  
$EndComp
Text GLabel 1350 1350 1    60   Input ~ 0
VBATT
$Comp
L VCC #PWR09
U 1 1 582B9847
P 2600 650
F 0 "#PWR09" H 2600 500 50  0001 C CNN
F 1 "VCC" H 2600 800 50  0000 C CNN
F 2 "" H 2600 650 50  0000 C CNN
F 3 "" H 2600 650 50  0000 C CNN
	1    2600 650 
	1    0    0    -1  
$EndComp
Text GLabel 4750 3950 2    60   Input ~ 0
Probe1
Text GLabel 4750 4050 2    60   Input ~ 0
Probe2
Text GLabel 2750 4400 0    60   Input ~ 0
HALL_SW
Text GLabel 3200 7150 0    60   Input ~ 0
HALL_SW
Text GLabel 2900 7400 0    60   Input ~ 0
VBATT
Text GLabel 3200 7050 0    60   Input ~ 0
3.3_smps_EN
$Comp
L CONN_01X04 P6
U 1 1 5836284D
P 4600 7250
F 0 "P6" H 4600 7500 50  0000 C CNN
F 1 "CONN_01X04" V 4700 7250 50  0000 C CNN
F 2 "lib:conn_2178713-4" H 4600 7250 50  0001 C CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=2178713&DocType=Customer+Drawing&DocLang=English" H 4600 7250 50  0001 C CNN
F 4 "Activation connector" H 4600 7250 60  0001 C CNN "Function"
F 5 "TE Connectivity AMP Connectors" H 4600 7250 60  0001 C CNN "MFG"
F 6 "2178713-4" H 4600 7250 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329306&uq=636226700918293125" H 4600 7250 60  0001 C CNN "Buy"
	1    4600 7250
	1    0    0    -1  
$EndComp
Text GLabel 9100 5700 0    60   Input ~ 0
VREF
Text GLabel 9300 5800 0    60   Input ~ 0
Noise_gen
Text GLabel 5150 5000 2    60   Input ~ 0
Noise_gen
Text GLabel 4750 4250 2    60   Input ~ 0
Batt_health
$Comp
L CONN_01X02 P1
U 1 1 5851D2C5
P 2300 1450
F 0 "P1" H 2300 1600 50  0000 C CNN
F 1 "CONN_01X02" H 2300 1300 50  0000 C CNN
F 2 "lib:9175-700_two-pin" H 2300 1450 50  0001 C CNN
F 3 "http://datasheets.avx.com/CappedIDC_9175-700.pdf" H 2300 1450 50  0001 C CNN
F 4 "Battery connector" H 2300 1450 60  0001 C CNN "Function"
F 5 "AVX Corp." H 2300 1450 60  0001 C CNN "MFG"
F 6 "009175002701106" H 2300 1450 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329313&uq=636227693972232779" H 2300 1450 60  0001 C CNN "Buy"
	1    2300 1450
	1    0    0    -1  
$EndComp
$Comp
L fiducial F1
U 1 1 58581951
P 9250 700
F 0 "F1" H 9250 550 60  0000 C CNN
F 1 "fiducial" H 9250 850 60  0000 C CNN
F 2 "Fiducials:Fiducial_1mm_Dia_2.54mm_Outer_CopperTop" H 9250 700 60  0001 C CNN
F 3 "" H 9250 700 60  0001 C CNN
	1    9250 700 
	1    0    0    -1  
$EndComp
$Comp
L fiducial F3
U 1 1 58581BB4
P 9650 700
F 0 "F3" H 9650 550 60  0000 C CNN
F 1 "fiducial" H 9650 850 60  0000 C CNN
F 2 "Fiducials:Fiducial_1mm_Dia_2.54mm_Outer_CopperTop" H 9650 700 60  0001 C CNN
F 3 "" H 9650 700 60  0001 C CNN
	1    9650 700 
	1    0    0    -1  
$EndComp
$Comp
L fiducial F2
U 1 1 58581CC3
P 9250 1150
F 0 "F2" H 9250 1000 60  0000 C CNN
F 1 "fiducial" H 9250 1300 60  0000 C CNN
F 2 "Fiducials:Fiducial_1mm_Dia_2.54mm_Outer_CopperTop" H 9250 1150 60  0001 C CNN
F 3 "" H 9250 1150 60  0001 C CNN
	1    9250 1150
	1    0    0    -1  
$EndComp
$Comp
L fiducial F4
U 1 1 58581DE4
P 9650 1150
F 0 "F4" H 9650 1000 60  0000 C CNN
F 1 "fiducial" H 9650 1300 60  0000 C CNN
F 2 "Fiducials:Fiducial_1mm_Dia_2.54mm_Outer_CopperTop" H 9650 1150 60  0001 C CNN
F 3 "" H 9650 1150 60  0001 C CNN
	1    9650 1150
	1    0    0    -1  
$EndComp
$Comp
L Hole_mount H1
U 1 1 58582B40
P 10100 700
F 0 "H1" H 10100 600 60  0000 C CNN
F 1 "Hole_mount" H 10150 850 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 10100 700 60  0001 C CNN
F 3 "" H 10100 700 60  0001 C CNN
	1    10100 700 
	1    0    0    -1  
$EndComp
$Comp
L Hole_mount H3
U 1 1 58582D43
P 10100 1500
F 0 "H3" H 10100 1400 60  0000 C CNN
F 1 "Hole_mount" H 10150 1650 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 10100 1500 60  0001 C CNN
F 3 "" H 10100 1500 60  0001 C CNN
	1    10100 1500
	1    0    0    -1  
$EndComp
$Comp
L Hole_mount H4
U 1 1 58582E74
P 10100 1850
F 0 "H4" H 10100 1750 60  0000 C CNN
F 1 "Hole_mount" H 10150 2000 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 10100 1850 60  0001 C CNN
F 3 "" H 10100 1850 60  0001 C CNN
	1    10100 1850
	1    0    0    -1  
$EndComp
$Comp
L Hole_mount H2
U 1 1 58583042
P 10100 1050
F 0 "H2" H 10100 950 60  0000 C CNN
F 1 "Hole_mount" H 10150 1200 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 10100 1050 60  0001 C CNN
F 3 "" H 10100 1050 60  0001 C CNN
	1    10100 1050
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR010
U 1 1 58588BD6
P 3100 7500
F 0 "#PWR010" H 3100 7350 50  0001 C CNN
F 1 "VCC" H 3100 7650 50  0000 C CNN
F 2 "" H 3100 7500 50  0000 C CNN
F 3 "" H 3100 7500 50  0000 C CNN
	1    3100 7500
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR011
U 1 1 586C75DA
P 3100 7300
F 0 "#PWR011" H 3100 7050 50  0001 C CNN
F 1 "GND" H 3100 7150 50  0000 C CNN
F 2 "" H 3100 7300 50  0000 C CNN
F 3 "" H 3100 7300 50  0000 C CNN
	1    3100 7300
	0    1    1    0   
$EndComp
$Sheet
S 7100 5900 850  500 
U 586E974F
F0 "3V3_supply" 60
F1 "file586E974E.sch" 60
F2 "BATT" I L 7100 6200 60 
F3 "GND" I L 7100 6300 60 
F4 "Power_out" I L 7100 6000 60 
F5 "Enable" I L 7100 6100 60 
$EndSheet
$Comp
L PWR_FLAG #FLG012
U 1 1 586DD19E
P 6750 6350
F 0 "#FLG012" H 6750 6445 50  0001 C CNN
F 1 "PWR_FLAG" H 6750 6530 50  0000 C CNN
F 2 "" H 6750 6350 50  0000 C CNN
F 3 "" H 6750 6350 50  0000 C CNN
	1    6750 6350
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR013
U 1 1 586F2BF8
P 7000 6350
F 0 "#PWR013" H 7000 6100 50  0001 C CNN
F 1 "GND" H 7000 6200 50  0000 C CNN
F 2 "" H 7000 6350 50  0000 C CNN
F 3 "" H 7000 6350 50  0000 C CNN
	1    7000 6350
	1    0    0    -1  
$EndComp
Text GLabel 6050 6200 0    60   Input ~ 0
VBATT
$Comp
L PWR_FLAG #FLG014
U 1 1 586F56D3
P 6750 5950
F 0 "#FLG014" H 6750 6045 50  0001 C CNN
F 1 "PWR_FLAG" H 6750 6130 50  0000 C CNN
F 2 "" H 6750 5950 50  0000 C CNN
F 3 "" H 6750 5950 50  0000 C CNN
	1    6750 5950
	1    0    0    -1  
$EndComp
$Comp
L LM4132 IC4
U 1 1 5870C0F1
P 7300 1100
F 0 "IC4" H 7100 1400 60  0000 C CNN
F 1 "LM4132" H 7300 1250 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 7300 1100 60  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm4132.pdf" H 7300 1100 60  0001 C CNN
F 4 "Voltage Reference" H 7300 1100 60  0001 C CNN "Function"
F 5 "1.8V" H 7300 1100 60  0001 C CNN "Notes"
F 6 "Texas Instruments" H 7300 1100 60  0001 C CNN "MFG"
F 7 "LM4132AMF-1.8/NOPB" H 7300 1100 60  0001 C CNN "part_num"
F 8 "http://www.digikey.com/product-detail/en/texas-instruments/LM4132AMF-1.8-NOPB/LM4132AMF-1.8-NOPBCT-ND/1206401" H 7300 1100 60  0001 C CNN "Buy"
	1    7300 1100
	-1   0    0    -1  
$EndComp
Text GLabel 6650 1150 0    60   Input ~ 0
VREF
$Comp
L GND #PWR015
U 1 1 5870C0CC
P 6800 1550
F 0 "#PWR015" H 6800 1300 50  0001 C CNN
F 1 "GND" H 6800 1400 50  0000 C CNN
F 2 "" H 6800 1550 50  0000 C CNN
F 3 "" H 6800 1550 50  0000 C CNN
	1    6800 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 587211F6
P 7850 1250
F 0 "#PWR016" H 7850 1000 50  0001 C CNN
F 1 "GND" H 7850 1100 50  0000 C CNN
F 2 "" H 7850 1250 50  0000 C CNN
F 3 "" H 7850 1250 50  0000 C CNN
	1    7850 1250
	1    0    0    -1  
$EndComp
$Sheet
S 9500 3200 900  1500
U 58932F0A
F0 "RF" 60
F1 "file58932F09.sch" 60
F2 "STX3_transmit" I L 9500 3850 60 
F3 "STX3_tx" I L 9500 3950 60 
F4 "STX3_rx" I L 9500 4050 60 
F5 "STX3_CTS" I L 9500 4150 60 
F6 "STX3_RTS" I L 9500 4250 60 
F7 "GPS_tx" I L 9500 4450 60 
F8 "GPS_rx" I L 9500 4550 60 
F9 "select_module" I L 9500 3750 60 
F10 "select_antenna" I L 9500 3650 60 
$EndSheet
Text GLabel 8400 3750 0    60   Input ~ 0
SAnt2
Text GLabel 8400 3650 0    60   Input ~ 0
SAnt1
Text GLabel 8400 4450 0    60   Input ~ 0
GPS_tx
Text GLabel 8400 4550 0    60   Input ~ 0
GPS_rx
Text GLabel 8400 3950 0    60   Input ~ 0
STX3_tx
Text GLabel 8400 4050 0    60   Input ~ 0
STX3_rx
Text GLabel 8400 4150 0    60   Input ~ 0
STX3_cts
Text GLabel 8400 4250 0    60   Input ~ 0
STX3_rts
Text GLabel 5650 3200 2    60   Input ~ 0
STX3_rts
Text GLabel 5650 3300 2    60   Input ~ 0
STX3_cts
Text GLabel 6550 3400 2    60   Input ~ 0
STX3_rx
Text GLabel 6550 3500 2    60   Input ~ 0
STX3_tx
Text GLabel 6550 3600 2    60   Input ~ 0
STX3_transmit
$Comp
L ATMEGA168PB IC2
U 1 1 57AB6CB0
P 3700 4200
F 0 "IC2" H 3000 5450 50  0000 L BNN
F 1 "ATMEGA328PB" H 3000 2800 50  0000 L BNN
F 2 "Housings_QFP:TQFP-32_7x7mm_Pitch0.8mm" H 3150 2850 50  0001 C CNN
F 3 "http://www.atmel.com/images/atmel-42397-8-bit-avr-microcontroller-atmega328pb_datasheet.pdf" H 3700 4200 60  0001 C CNN
F 4 "Microprocessor" H 3700 4200 60  0001 C CNN "Function"
F 5 "Atmel" H 3700 4200 60  0001 C CNN "MFG"
F 6 "ATmega328PB-AU" H 3700 4200 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/microchip-technology/ATMEGA328PB-AU/ATMEGA328PB-AU-ND/5638812" H 3700 4200 60  0001 C CNN "Buy"
F 8 "Only get in TQFP-32 package" H 3700 4200 60  0001 C CNN "Notes"
	1    3700 4200
	1    0    0    -1  
$EndComp
Text GLabel 5150 5200 2    60   Input ~ 0
SAnt1
Text GLabel 5150 5300 2    60   Input ~ 0
SAnt2
$Comp
L TEST STX3_rts1
U 1 1 58958C7A
P 5300 3150
F 0 "STX3_rts1" H 5250 3450 50  0000 C BNN
F 1 "TEST" H 5250 3400 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 5300 3150 50  0001 C CNN
F 3 "" H 5300 3150 50  0000 C CNN
	1    5300 3150
	1    0    0    -1  
$EndComp
$Comp
L TEST STX3_cts1
U 1 1 58959215
P 5500 3250
F 0 "STX3_cts1" H 5550 3550 50  0000 C BNN
F 1 "TEST" H 5550 3500 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 5500 3250 50  0001 C CNN
F 3 "" H 5500 3250 50  0000 C CNN
	1    5500 3250
	1    0    0    -1  
$EndComp
$Comp
L TEST STX3_Rx1
U 1 1 5895960A
P 6200 3300
F 0 "STX3_Rx1" H 6200 3600 50  0000 C BNN
F 1 "TEST" H 6200 3550 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 6200 3300 50  0001 C CNN
F 3 "" H 6200 3300 50  0000 C CNN
	1    6200 3300
	1    0    0    -1  
$EndComp
$Comp
L TEST STX3_Tx1
U 1 1 589599F8
P 6400 3450
F 0 "STX3_Tx1" H 6400 3750 50  0000 C BNN
F 1 "TEST" H 6400 3700 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 6400 3450 50  0001 C CNN
F 3 "" H 6400 3450 50  0000 C CNN
	1    6400 3450
	1    0    0    -1  
$EndComp
$Comp
L TEST GPS_Tx1
U 1 1 5895AB77
P 5950 4650
F 0 "GPS_Tx1" H 5950 4500 50  0000 C BNN
F 1 "TEST" H 5950 4450 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 5950 4650 50  0001 C CNN
F 3 "" H 5950 4650 50  0000 C CNN
	1    5950 4650
	1    0    0    -1  
$EndComp
$Comp
L TEST GPS_Rx1
U 1 1 5895B123
P 6100 4750
F 0 "GPS_Rx1" H 6100 5050 50  0000 C BNN
F 1 "TEST" H 6100 5000 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 6100 4750 50  0001 C CNN
F 3 "" H 6100 4750 50  0000 C CNN
	1    6100 4750
	1    0    0    -1  
$EndComp
$Comp
L TEST SCL1
U 1 1 5895C70E
P 5650 4400
F 0 "SCL1" H 5650 4250 50  0000 C BNN
F 1 "TEST" H 5650 4200 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 5650 4400 50  0001 C CNN
F 3 "" H 5650 4400 50  0000 C CNN
	1    5650 4400
	1    0    0    -1  
$EndComp
$Comp
L TEST SDA1
U 1 1 5895C896
P 5450 4300
F 0 "SDA1" H 5450 4150 50  0000 C BNN
F 1 "TEST" H 5450 4100 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 5450 4300 50  0001 C CNN
F 3 "" H 5450 4300 50  0000 C CNN
	1    5450 4300
	1    0    0    -1  
$EndComp
Text Notes 7600 950  0    60   ~ 0
Drive STX3_en high to \nenable voltage reference
$Comp
L TEST CLK0
U 1 1 5897FAC7
P 4800 3000
F 0 "CLK0" H 4800 3300 50  0000 C BNN
F 1 "TEST" H 4800 3250 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 4800 3000 50  0001 C CNN
F 3 "" H 4800 3000 50  0000 C CNN
	1    4800 3000
	1    0    0    -1  
$EndComp
$Comp
L TEST SANT1
U 1 1 5897FC3E
P 8900 3600
F 0 "SANT1" H 8900 3900 50  0000 C BNN
F 1 "TEST" H 8900 3850 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 8900 3600 50  0001 C CNN
F 3 "" H 8900 3600 50  0000 C CNN
	1    8900 3600
	1    0    0    -1  
$EndComp
$Comp
L TEST SMOD1
U 1 1 5897FDBC
P 9100 3700
F 0 "SMOD1" H 9100 4000 50  0000 C BNN
F 1 "TEST" H 9100 3950 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 9100 3700 50  0001 C CNN
F 3 "" H 9100 3700 50  0000 C CNN
	1    9100 3700
	1    0    0    -1  
$EndComp
$Sheet
S 9500 5150 800  750 
U 5894EC0E
F0 "sensors" 60
F1 "file5894EC0D.sch" 60
F2 "Probe1" I L 9500 5200 60 
F3 "Probe2" I L 9500 5300 60 
F4 "VDivide" I L 9500 5400 60 
F5 "Batt_health" I L 9500 5500 60 
F6 "Buzz" I L 9500 5600 60 
F7 "noise_out" I L 9500 5700 60 
F8 "noise_gen" I L 9500 5800 60 
$EndSheet
Text GLabel 9350 5200 0    60   Input ~ 0
Probe1
Text GLabel 9350 5300 0    60   Input ~ 0
Probe2
$Comp
L R_network02 J1
U 1 1 589580E1
P 3350 7450
F 0 "J1" H 3600 7550 60  0000 C CNN
F 1 "R_network02" H 3395 7315 60  0000 C CNN
F 2 "lib:3pad_0603" H 3370 7390 60  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3370 7390 60  0001 C CNN
F 4 "Select off board activation pwr source" H 3350 7450 60  0001 C CNN "Function"
F 5 "DNP, exclusive jumper" H 3350 7450 60  0001 C CNN "Notes"
F 6 "Yageo" H 3350 7450 60  0001 C CNN "MFG"
F 7 "RC0603JR-070RL" H 3350 7450 60  0001 C CNN "part_num"
F 8 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" H 3350 7450 60  0001 C CNN "Buy"
	1    3350 7450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 5895D72C
P 7150 4850
F 0 "#PWR017" H 7150 4600 50  0001 C CNN
F 1 "GND" H 7150 4700 50  0000 C CNN
F 2 "" H 7150 4850 50  0000 C CNN
F 3 "" H 7150 4850 50  0000 C CNN
	1    7150 4850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR018
U 1 1 5895E32C
P 7150 4200
F 0 "#PWR018" H 7150 4050 50  0001 C CNN
F 1 "VCC" H 7150 4350 50  0000 C CNN
F 2 "" H 7150 4200 50  0000 C CNN
F 3 "" H 7150 4200 50  0000 C CNN
	1    7150 4200
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 58962D0D
P 650 3400
F 0 "C1" V 600 3250 50  0000 L CNN
F 1 "1uF" V 700 3200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 688 3250 50  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" H 650 3400 50  0001 C CNN
F 4 "MCU decouple" V 650 3400 60  0001 C CNN "Field4"
F 5 "Yageo" V 650 3400 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB105" V 650 3400 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" V 650 3400 60  0001 C CNN "Buy"
	1    650  3400
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR019
U 1 1 5896327B
P 750 3650
F 0 "#PWR019" H 750 3400 50  0001 C CNN
F 1 "GND" H 750 3500 50  0000 C CNN
F 2 "" H 750 3650 50  0000 C CNN
F 3 "" H 750 3650 50  0000 C CNN
	1    750  3650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 589662ED
P 2500 3950
F 0 "#PWR020" H 2500 3700 50  0001 C CNN
F 1 "GND" H 2500 3800 50  0000 C CNN
F 2 "" H 2500 3950 50  0000 C CNN
F 3 "" H 2500 3950 50  0000 C CNN
	1    2500 3950
	1    0    0    -1  
$EndComp
Text Notes 7300 4800 0    60   ~ 0
RST lacks built \nin ESD protection
Text GLabel 2750 4300 0    60   Input ~ 0
General_EN
Text GLabel 8400 3850 0    60   Input ~ 0
STX3_transmit
Text GLabel 8050 1150 2    60   Input ~ 0
PWR_EN
$Comp
L VCC #PWR021
U 1 1 58992365
P 3550 650
F 0 "#PWR021" H 3550 500 50  0001 C CNN
F 1 "VCC" H 3550 800 50  0000 C CNN
F 2 "" H 3550 650 50  0000 C CNN
F 3 "" H 3550 650 50  0000 C CNN
	1    3550 650 
	1    0    0    -1  
$EndComp
Text GLabel 3600 1450 2    60   Input ~ 0
PWR_EN
Text GLabel 4450 1000 2    60   Input ~ 0
General_EN
$Comp
L C_Small C12
U 1 1 58996A7A
P 3300 1450
F 0 "C12" H 3310 1520 50  0000 L CNN
F 1 "100nF" H 3310 1370 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3300 1450 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 3300 1450 50  0001 C CNN
F 4 "Smoothing" H 3300 1450 60  0001 C CNN "Field4"
F 5 "Yageo" H 3300 1450 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 3300 1450 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 3300 1450 60  0001 C CNN "Buy"
	1    3300 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 589970C2
P 3300 1650
F 0 "#PWR022" H 3300 1400 50  0001 C CNN
F 1 "GND" H 3300 1500 50  0000 C CNN
F 2 "" H 3300 1650 50  0000 C CNN
F 3 "" H 3300 1650 50  0000 C CNN
	1    3300 1650
	1    0    0    -1  
$EndComp
Text GLabel 9350 5400 0    60   Input ~ 0
VDivide
Text GLabel 4750 4150 2    60   Input ~ 0
VDivide
Text Label 1800 1400 0    60   ~ 0
batt1
Text Label 1800 1800 0    60   ~ 0
batt2
Text Label 1800 2200 0    60   ~ 0
batt3
Text Label 1800 2550 0    60   ~ 0
batt4
Text Label 3700 7400 0    60   ~ 0
activation_pwr
Text GLabel 9350 5500 0    60   Input ~ 0
Batt_health
$Comp
L Led_Small_drifter D5
U 1 1 589BBBA5
P 2150 4500
F 0 "D5" H 2200 4450 50  0000 L CNN
F 1 "Led_Small_drifter" H 1900 4600 50  0001 L CNN
F 2 "lib:3.2x2.7mm_LED" V 2150 4500 50  0001 C CNN
F 3 "http://www.cree.com/~/media/Files/Cree/LED%20Components%20and%20Modules/HB/Data%20Sheets/CLM1BRKWAKW1084.pdf" V 2150 4500 50  0001 C CNN
F 4 "LED indicator" H 2150 4500 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329314&uq=636227661034218865" H 2150 4500 60  0001 C CNN "Buy"
F 6 "DNP, Amber" H 2150 4500 60  0001 C CNN "Notes"
F 7 "Cree Inc." H 2150 4500 60  0001 C CNN "MFG"
F 8 "CLM1B-AKW-CUAVB253" H 2150 4500 60  0001 C CNN "part_num"
	1    2150 4500
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 589BBBAD
P 1550 4500
F 0 "R1" V 1650 4500 50  0000 C CNN
F 1 "120" V 1550 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 1480 4500 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 1550 4500 50  0001 C CNN
F 4 "LED resistor" V 1550 4500 60  0001 C CNN "Function"
F 5 "Yageo" V 1550 4500 60  0001 C CNN "MFG"
F 6 "RC0603JR-07120RL" V 1550 4500 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-07120RL/311-120GRCT-ND/729653" V 1550 4500 60  0001 C CNN "Buy"
	1    1550 4500
	0    1    1    0   
$EndComp
Text Label 2050 4500 2    60   ~ 0
led_cath1
$Comp
L GND #PWR023
U 1 1 589BC942
P 1300 4600
F 0 "#PWR023" H 1300 4350 50  0001 C CNN
F 1 "GND" H 1300 4450 50  0000 C CNN
F 2 "" H 1300 4600 50  0000 C CNN
F 3 "" H 1300 4600 50  0000 C CNN
	1    1300 4600
	1    0    0    -1  
$EndComp
Text Label 2300 4500 0    60   ~ 0
led_anode1
$Comp
L R J2
U 1 1 586F91E6
P 6300 6100
F 0 "J2" V 6200 6100 50  0000 C CNN
F 1 "J" V 6300 6100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6230 6100 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 6300 6100 50  0001 C CNN
F 4 "Bypass power supply enable" V 6300 6100 60  0001 C CNN "Function"
F 5 "Yageo" V 6300 6100 60  0001 C CNN "MFG"
F 6 "RC0603JR-070RL" V 6300 6100 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" V 6300 6100 60  0001 C CNN "Buy"
	1    6300 6100
	0    1    1    0   
$EndComp
Text GLabel 1650 5600 2    60   Input ~ 0
PWR_EN
$Comp
L D_Zener_Small_ALT D9
U 1 1 589A15F7
P 1250 2000
F 0 "D9" H 1250 2090 50  0000 C CNN
F 1 "18V" H 1250 1910 50  0000 C CNN
F 2 "Diodes_SMD:D_SMA_Standard" V 1250 2000 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/3SMAJ5913B~3SMAJ5943B(DO-214AC).pdf" V 1250 2000 50  0001 C CNN
F 4 "Overvoltage protection (Zener diode)" H 1250 2000 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=218388676&uq=636227658265199238" H 1250 2000 60  0001 C CNN "Buy"
F 6 "Micro Commercial Co" H 1250 2000 60  0001 C CNN "MFG"
F 7 "3SMAJ5931B-TP" H 1250 2000 60  0001 C CNN "part_num"
	1    1250 2000
	0    1    1    0   
$EndComp
$Comp
L L_Small L5
U 1 1 589A3D28
P 1350 3400
F 0 "L5" H 1380 3440 50  0000 L CNN
F 1 "10uH" H 1380 3360 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 1350 3400 50  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/taiyo-yuden/CB2012T100KR/587-2452-1-ND/2230318" H 1350 3400 50  0001 C CNN
F 4 "AVCC filter" H 1350 3400 60  0001 C CNN "Function"
F 5 "Taiyo Yuden" H 1350 3400 60  0001 C CNN "MFG"
F 6 "CB2012T100KR" H 1350 3400 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/taiyo-yuden/CB2012T100KR/587-2452-1-ND/2230318" H 1350 3400 60  0001 C CNN "Buy"
	1    1350 3400
	1    0    0    -1  
$EndComp
Text GLabel 9350 5600 0    60   Input ~ 0
Buzz_EN
NoConn ~ 4400 7200
$Comp
L PWR_FLAG #FLG024
U 1 1 589D028E
P 1700 3150
F 0 "#FLG024" H 1700 3245 50  0001 C CNN
F 1 "PWR_FLAG" H 1700 3330 50  0000 C CNN
F 2 "" H 1700 3150 50  0000 C CNN
F 3 "" H 1700 3150 50  0000 C CNN
	1    1700 3150
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG025
U 1 1 589D1047
P 1900 6000
F 0 "#FLG025" H 1900 6095 50  0001 C CNN
F 1 "PWR_FLAG" H 1900 6180 50  0000 C CNN
F 2 "" H 1900 6000 50  0000 C CNN
F 3 "" H 1900 6000 50  0000 C CNN
	1    1900 6000
	1    0    0    -1  
$EndComp
$Comp
L Led_Small_drifter D10
U 1 1 589D5510
P 5200 5100
F 0 "D10" H 5250 5050 50  0000 L CNN
F 1 "Led_Small_drifter" H 4950 5200 50  0001 L CNN
F 2 "lib:3.2x2.7mm_LED" V 5200 5100 50  0001 C CNN
F 3 "http://www.cree.com/~/media/Files/Cree/LED%20Components%20and%20Modules/HB/Data%20Sheets/CLM1BRKWAKW1084.pdf" V 5200 5100 50  0001 C CNN
F 4 "LED indicator" H 5200 5100 60  0001 C CNN "Function"
F 5 "Cree Inc." H 5200 5100 60  0001 C CNN "MFG"
F 6 "DNP, Red" H 5200 5100 60  0001 C CNN "Notes"
F 7 "CLM1B-RKW-CUAVBAA3" H 5200 5100 60  0001 C CNN "part_num"
F 8 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329317&uq=636227661034218865" H 5200 5100 60  0001 C CNN "Buy"
	1    5200 5100
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 589D551E
P 6100 5200
F 0 "#PWR026" H 6100 4950 50  0001 C CNN
F 1 "GND" H 6100 5050 50  0000 C CNN
F 2 "" H 6100 5200 50  0000 C CNN
F 3 "" H 6100 5200 50  0000 C CNN
	1    6100 5200
	-1   0    0    -1  
$EndComp
Text Label 5300 5100 0    60   ~ 0
led_cath2
Text Label 4700 5100 0    60   ~ 0
led_anode2
Text Label 3550 1900 0    60   ~ 0
led_cath3
$Comp
L GND #PWR027
U 1 1 589D907C
P 3550 2350
F 0 "#PWR027" H 3550 2100 50  0001 C CNN
F 1 "GND" H 3550 2200 50  0000 C CNN
F 2 "" H 3550 2350 50  0000 C CNN
F 3 "" H 3550 2350 50  0000 C CNN
	1    3550 2350
	1    0    0    -1  
$EndComp
Text Label 3550 1600 0    60   ~ 0
led_anode3
$Comp
L A3211 U1
U 1 1 589DAC91
P 5300 6950
F 0 "U1" H 5200 7050 60  0000 C CNN
F 1 "A3212" H 5300 6950 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch1.27mm" H 5300 6950 60  0001 C CNN
F 3 "http://www.allegromicro.com/~/media/Files/Datasheets/A3211-12-Datasheet.ashx?la=en" H 5300 6950 60  0001 C CNN
F 4 "Wake device" H 5300 6950 60  0001 C CNN "Function"
F 5 "Allegro MicroSystems, LLC" H 5300 6950 60  0001 C CNN "MFG"
F 6 "A3212EUA-T" H 5300 6950 60  0001 C CNN "part_num"
	1    5300 6950
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR028
U 1 1 589DBCF7
P 5000 7400
F 0 "#PWR028" H 5000 7250 50  0001 C CNN
F 1 "VCC" H 5000 7550 50  0000 C CNN
F 2 "" H 5000 7400 50  0000 C CNN
F 3 "" H 5000 7400 50  0000 C CNN
	1    5000 7400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 589DC055
P 5300 7600
F 0 "#PWR029" H 5300 7350 50  0001 C CNN
F 1 "GND" H 5300 7450 50  0000 C CNN
F 2 "" H 5300 7600 50  0000 C CNN
F 3 "" H 5300 7600 50  0000 C CNN
	1    5300 7600
	1    0    0    -1  
$EndComp
Text GLabel 5500 7500 2    60   Input ~ 0
HALL_SW
Text Label 1650 3600 2    60   ~ 0
AVCC
Text Label 1600 6150 0    60   ~ 0
accel_pwr
Text GLabel 6850 700  2    60   Input ~ 0
PWR_EN
Text GLabel 6500 6050 1    60   Input ~ 0
3.3_smps_EN
Text GLabel 6550 3850 2    60   Input ~ 0
PWR_EN
Text Label 3800 7100 0    60   ~ 0
activation_sig
$Comp
L Crystal_GND24_Small_jte Y1
U 1 1 58A3451E
P 5650 3850
F 0 "Y1" V 5550 3600 50  0000 L CNN
F 1 "7.3728MHz" V 5750 3850 50  0000 L CNN
F 2 "lib:ABMM2_4pad_crystal" H 5650 3850 50  0001 C CNN
F 3 "http://www.abracon.com/Resonators/ABMM2.pdf" H 5650 3850 50  0001 C CNN
F 4 "Crystal" V 5650 3850 60  0001 C CNN "Function"
F 5 "ABMM2-7.3728MHZ-E2-T" V 5650 3850 60  0001 C CNN "part_num"
F 6 "https://www.digikey.com/product-detail/en/abracon-llc/ABMM2-7.3728MHZ-E2-T/535-10157-2-ND/2184100" V 5650 3850 60  0001 C CNN "Buy"
F 7 "Abracon" V 5650 3850 60  0001 C CNN "MFG"
	1    5650 3850
	0    1    1    0   
$EndComp
$Comp
L GND #PWR030
U 1 1 58A37336
P 5300 3900
F 0 "#PWR030" H 5300 3650 50  0001 C CNN
F 1 "GND" H 5300 3750 50  0000 C CNN
F 2 "" H 5300 3900 50  0000 C CNN
F 3 "" H 5300 3900 50  0000 C CNN
	1    5300 3900
	1    0    0    -1  
$EndComp
NoConn ~ 4700 5400
$Comp
L TEST PWR_EN1
U 1 1 58A3DA73
P 3100 1200
F 0 "PWR_EN1" H 3100 1500 50  0000 C BNN
F 1 "TEST" H 3100 1450 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 3100 1200 50  0001 C CNN
F 3 "" H 3100 1200 50  0000 C CNN
	1    3100 1200
	1    0    0    -1  
$EndComp
$Comp
L Q_PMOS_GSD Q1
U 1 1 58A4C1F3
P 3650 1000
F 0 "Q1" H 3550 1150 50  0000 L CNN
F 1 "Q_PMOS_GSD" H 3200 800 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3850 1100 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds31861.pdf" H 3650 1000 50  0001 C CNN
F 4 "Switch power to other parts" H 3650 1000 60  0001 C CNN "Function"
F 5 "Diodes Incorporated" H 3650 1000 60  0001 C CNN "MFG"
F 6 "DMG1013UW-7" H 3650 1000 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/diodes-incorporated/DMG1013UW-7/DMG1013UW-7DICT-ND/2183252" H 3650 1000 60  0001 C CNN "Buy"
	1    3650 1000
	-1   0    0    1   
$EndComp
$Comp
L D_Small D2
U 1 1 58A5E6F5
P 1600 1800
F 0 "D2" H 1450 1850 50  0000 L CNN
F 1 "D_Small" H 1250 1850 50  0001 L CNN
F 2 "lib:DB2J317" V 1600 1800 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J317_E.pdf" V 1600 1800 50  0001 C CNN
F 4 "Protection Diode" H 1600 1800 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/DB2J31700L/DB2J31700LCT-ND/2184712" H 1600 1800 60  0001 C CNN "Buy"
F 6 "Panasonic Electronic Components" H 1600 1800 60  0001 C CNN "MFG"
F 7 "DB2J31700L" H 1600 1800 60  0001 C CNN "part_num"
	1    1600 1800
	1    0    0    -1  
$EndComp
$Comp
L D_Small D3
U 1 1 58A5E861
P 1600 2200
F 0 "D3" H 1450 2250 50  0000 L CNN
F 1 "D_Small" H 1250 2250 50  0001 L CNN
F 2 "lib:DB2J317" V 1600 2200 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J317_E.pdf" V 1600 2200 50  0001 C CNN
F 4 "Protection Diode" H 1600 2200 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/DB2J31700L/DB2J31700LCT-ND/2184712" H 1600 2200 60  0001 C CNN "Buy"
F 6 "Panasonic Electronic Components" H 1600 2200 60  0001 C CNN "MFG"
F 7 "DB2J31700L" H 1600 2200 60  0001 C CNN "part_num"
	1    1600 2200
	1    0    0    -1  
$EndComp
$Comp
L D_Small D4
U 1 1 58A5E9CC
P 1600 2550
F 0 "D4" H 1450 2600 50  0000 L CNN
F 1 "D_Small" H 1250 2600 50  0001 L CNN
F 2 "lib:DB2J317" V 1600 2550 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J317_E.pdf" V 1600 2550 50  0001 C CNN
F 4 "Protection Diode" H 1600 2550 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/DB2J31700L/DB2J31700LCT-ND/2184712" H 1600 2550 60  0001 C CNN "Buy"
F 6 "Panasonic Electronic Components" H 1600 2550 60  0001 C CNN "MFG"
F 7 "DB2J31700L" H 1600 2550 60  0001 C CNN "part_num"
	1    1600 2550
	1    0    0    -1  
$EndComp
$Comp
L D_Small D6
U 1 1 58A5F44D
P 7300 4400
F 0 "D6" H 7150 4450 50  0000 L CNN
F 1 "D_Small" H 6950 4450 50  0001 L CNN
F 2 "lib:DB2J317" V 7300 4400 50  0001 C CNN
F 3 "http://www.semicon.panasonic.co.jp/ds4/DB2J317_E.pdf" V 7300 4400 50  0001 C CNN
F 4 "RST ESD Protection" H 7300 4400 60  0001 C CNN "Function"
F 5 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/DB2J31700L/DB2J31700LCT-ND/2184712" H 7300 4400 60  0001 C CNN "Buy"
F 6 "Panasonic Electronic Components" H 7300 4400 60  0001 C CNN "MFG"
F 7 "DB2J31700L" H 7300 4400 60  0001 C CNN "part_num"
	1    7300 4400
	0    1    1    0   
$EndComp
$Comp
L Led_Small_drifter D11
U 1 1 58A65A67
P 3550 1750
F 0 "D11" H 3600 1700 50  0000 L CNN
F 1 "Led_Small_drifter" H 3300 1850 50  0001 L CNN
F 2 "lib:3.2x2.7mm_LED" V 3550 1750 50  0001 C CNN
F 3 "http://www.cree.com/~/media/Files/Cree/LED%20Components%20and%20Modules/HB/Data%20Sheets/CLM1BRKWAKW1084.pdf" V 3550 1750 50  0001 C CNN
F 4 "PWR_EN LED indicator" H 3550 1750 60  0001 C CNN "Function"
F 5 "Cree Inc." H 3550 1750 60  0001 C CNN "MFG"
F 6 "DNP, Red" H 3550 1750 60  0001 C CNN "Notes"
F 7 "CLM1B-RKW-CUAVBAA3" H 3550 1750 60  0001 C CNN "part_num"
F 8 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329317&uq=636227661034218865" H 3550 1750 60  0001 C CNN "Buy"
	1    3550 1750
	0    1    -1   0   
$EndComp
$Comp
L C C8
U 1 1 58A5FC4A
P 2350 3700
F 0 "C8" V 2300 3550 50  0000 L CNN
F 1 "1uF" V 2400 3500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2388 3550 50  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" H 2350 3700 50  0001 C CNN
F 4 "VREF decouple" V 2350 3700 60  0001 C CNN "Field4"
F 5 "Yageo" V 2350 3700 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB105" V 2350 3700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB105/311-1446-1-ND/2833752" V 2350 3700 60  0001 C CNN "Buy"
	1    2350 3700
	-1   0    0    1   
$EndComp
$Comp
L C_Small C16
U 1 1 58A62C95
P 6800 1350
F 0 "C16" H 6810 1420 50  0000 L CNN
F 1 "100nF" H 6810 1270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6800 1350 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 6800 1350 50  0001 C CNN
F 4 "voltage reference decoupling" H 6800 1350 60  0001 C CNN "Function"
F 5 "Yageo" H 6800 1350 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 6800 1350 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 6800 1350 60  0001 C CNN "Buy"
	1    6800 1350
	1    0    0    -1  
$EndComp
Connection ~ 4200 1000
Connection ~ 3550 700 
Wire Wire Line
	3550 700  4200 700 
Wire Wire Line
	3850 1000 4450 1000
Wire Wire Line
	3100 1200 3100 1250
Wire Wire Line
	5300 3850 5500 3850
Wire Wire Line
	5300 3900 5300 3850
Connection ~ 5650 4100
Wire Wire Line
	5200 3800 5200 4100
Wire Wire Line
	4700 3800 5200 3800
Connection ~ 5650 3700
Connection ~ 6100 3850
Wire Wire Line
	5800 3850 6100 3850
Wire Wire Line
	4700 3700 5900 3700
Wire Wire Line
	5650 3750 5650 3700
Wire Wire Line
	5650 4100 5650 3950
Wire Wire Line
	5200 4100 5900 4100
Connection ~ 6100 4100
Wire Wire Line
	6050 4100 6100 4100
Wire Wire Line
	6100 3700 6050 3700
Wire Wire Line
	6100 3700 6100 4150
Wire Wire Line
	4800 3100 4800 3000
Wire Wire Line
	4700 3100 4800 3100
Wire Wire Line
	6550 3850 6500 3850
Wire Wire Line
	6850 700  6800 700 
Wire Wire Line
	5400 7500 5400 7400
Wire Wire Line
	5500 7500 5400 7500
Wire Wire Line
	5300 7400 5300 7600
Wire Wire Line
	5200 7500 5200 7400
Wire Wire Line
	5000 7500 5200 7500
Wire Wire Line
	5000 7400 5000 7500
Connection ~ 3550 1450
Wire Wire Line
	3550 2250 3550 2350
Wire Wire Line
	3550 1950 3550 1850
Wire Wire Line
	4700 5100 5100 5100
Wire Wire Line
	6100 5100 6050 5100
Wire Wire Line
	6100 5200 6100 5100
Wire Wire Line
	5750 5100 5300 5100
Connection ~ 1600 6150
Wire Wire Line
	1900 6150 1600 6150
Wire Wire Line
	1900 6000 1900 6150
Connection ~ 1600 6400
Connection ~ 1600 6500
Connection ~ 1700 3300
Wire Wire Line
	1600 5600 1600 6500
Wire Wire Line
	1650 5600 1600 5600
Wire Wire Line
	3750 7100 4400 7100
Wire Wire Line
	3200 7150 3250 7150
Wire Wire Line
	3200 7050 3250 7050
Wire Wire Line
	9300 5800 9500 5800
Wire Wire Line
	9350 5600 9500 5600
Wire Wire Line
	8400 4450 9500 4450
Wire Wire Line
	8400 4550 9500 4550
Wire Wire Line
	8400 4250 9500 4250
Wire Wire Line
	8400 4150 9500 4150
Wire Wire Line
	8400 4050 9500 4050
Wire Wire Line
	8400 3950 9500 3950
Wire Wire Line
	8400 3850 9500 3850
Connection ~ 1350 3600
Wire Wire Line
	1700 3300 2800 3300
Wire Wire Line
	1700 3150 1700 3600
Wire Wire Line
	1700 3600 1350 3600
Wire Wire Line
	1350 3500 1350 3700
Wire Wire Line
	1350 3100 1350 3300
Wire Wire Line
	1350 3900 1350 4000
Connection ~ 1500 2650
Wire Wire Line
	1250 2100 1250 2650
Connection ~ 1350 1400
Wire Wire Line
	1250 1900 1250 1400
Wire Wire Line
	6450 6100 7100 6100
Wire Wire Line
	6500 6100 6500 6050
Connection ~ 6500 6100
Wire Wire Line
	6150 6100 6100 6100
Wire Wire Line
	6100 6100 6100 6200
Connection ~ 6100 6200
Wire Wire Line
	2750 4300 2800 4300
Connection ~ 3300 1250
Wire Wire Line
	3100 1250 3550 1250
Wire Wire Line
	3300 1100 3300 1350
Connection ~ 3550 750 
Wire Wire Line
	3300 750  3550 750 
Wire Wire Line
	2800 4500 2250 4500
Wire Wire Line
	1300 4500 1400 4500
Wire Wire Line
	1300 4600 1300 4500
Wire Wire Line
	1700 4500 2050 4500
Connection ~ 1450 1400
Wire Wire Line
	1350 1400 1350 1350
Connection ~ 1450 1800
Wire Wire Line
	1500 1800 1450 1800
Connection ~ 1450 2200
Wire Wire Line
	1450 2200 1500 2200
Wire Wire Line
	1450 2550 1500 2550
Wire Wire Line
	1450 1400 1450 2550
Wire Wire Line
	1250 1400 1500 1400
Wire Wire Line
	2100 2550 1700 2550
Wire Wire Line
	2100 2200 1700 2200
Wire Wire Line
	2100 1400 1700 1400
Wire Wire Line
	2100 1800 1700 1800
Connection ~ 1750 1900
Wire Wire Line
	2100 1900 1750 1900
Connection ~ 1750 2300
Wire Wire Line
	1750 2300 2100 2300
Connection ~ 1750 2650
Wire Wire Line
	1750 1500 1750 2650
Wire Wire Line
	2100 1500 1750 1500
Wire Wire Line
	1500 2650 1500 2700
Wire Wire Line
	1250 2650 2100 2650
Wire Wire Line
	4750 4150 4700 4150
Wire Wire Line
	9150 5700 9100 5700
Wire Wire Line
	9450 5700 9500 5700
Wire Wire Line
	2550 900  2600 900 
Wire Wire Line
	2600 900  2600 950 
Wire Wire Line
	2600 700  2550 700 
Wire Wire Line
	2050 800  2000 800 
Wire Wire Line
	2050 900  2000 900 
Wire Wire Line
	2050 700  2000 700 
Wire Wire Line
	5250 3500 4700 3500
Wire Wire Line
	4700 3400 5250 3400
Wire Wire Line
	2250 3400 2800 3400
Wire Wire Line
	5550 3400 6550 3400
Wire Wire Line
	5550 3500 6550 3500
Wire Wire Line
	4700 4700 6400 4700
Wire Wire Line
	4700 4800 6400 4800
Wire Wire Line
	4700 3300 5650 3300
Wire Wire Line
	4700 4050 4750 4050
Wire Wire Line
	4700 3950 4750 3950
Wire Wire Line
	1500 7500 1650 7500
Wire Wire Line
	1500 7300 1650 7300
Wire Wire Line
	1500 7200 1650 7200
Wire Wire Line
	4700 4450 6600 4450
Wire Wire Line
	4700 4350 6600 4350
Wire Wire Line
	1850 7500 2150 7500
Wire Wire Line
	2150 6300 2150 7550
Wire Wire Line
	2150 7100 1500 7100
Wire Wire Line
	2150 6800 1500 6800
Connection ~ 2150 7100
Wire Wire Line
	2150 6700 1500 6700
Connection ~ 2150 6800
Connection ~ 2150 7500
Wire Wire Line
	4700 4900 5150 4900
Wire Wire Line
	2100 6500 2150 6500
Connection ~ 2150 6700
Wire Wire Line
	650  3200 2800 3200
Wire Wire Line
	1650 7000 1500 7000
Wire Wire Line
	1500 6500 1800 6500
Wire Wire Line
	2150 6600 1500 6600
Connection ~ 2150 6600
Wire Wire Line
	1600 6300 1850 6300
Wire Wire Line
	1600 6400 1500 6400
Connection ~ 1600 6300
Wire Wire Line
	2050 6300 2150 6300
Connection ~ 2150 6500
Wire Wire Line
	6350 4150 6350 3950
Wire Wire Line
	6350 3950 6500 3950
Wire Wire Line
	6500 3850 6500 4050
Connection ~ 6500 3950
Connection ~ 6500 4350
Connection ~ 6350 4450
Wire Wire Line
	2600 700  2600 650 
Wire Wire Line
	2750 4400 2800 4400
Wire Wire Line
	5150 5000 4700 5000
Wire Wire Line
	2900 7400 3150 7400
Wire Wire Line
	2600 800  2550 800 
Wire Wire Line
	2750 5200 2750 5400
Wire Wire Line
	2750 5200 2800 5200
Wire Wire Line
	2800 5300 2750 5300
Connection ~ 2750 5300
Wire Wire Line
	6750 6000 7100 6000
Wire Wire Line
	7000 6000 7000 5950
Wire Wire Line
	6750 6300 7100 6300
Wire Wire Line
	7000 6300 7000 6350
Wire Wire Line
	6750 6350 6750 6300
Connection ~ 7000 6300
Wire Wire Line
	6750 5950 6750 6000
Connection ~ 7000 6000
Wire Wire Line
	6050 6200 7100 6200
Wire Wire Line
	7700 1050 7850 1050
Connection ~ 6800 1050
Wire Wire Line
	7700 1150 8050 1150
Wire Wire Line
	6900 1050 6800 1050
Wire Wire Line
	6800 700  6800 1250
Wire Wire Line
	6650 1150 6900 1150
Wire Wire Line
	6800 1450 6800 1550
Wire Wire Line
	7850 1050 7850 1250
Wire Wire Line
	8400 3650 9500 3650
Wire Wire Line
	8400 3750 9500 3750
Wire Wire Line
	4700 5300 5150 5300
Wire Wire Line
	4700 5200 5150 5200
Wire Wire Line
	6400 3450 6400 3500
Connection ~ 6400 3500
Wire Wire Line
	6200 3300 6200 3400
Connection ~ 6200 3400
Wire Wire Line
	5500 3250 5500 3300
Connection ~ 5500 3300
Wire Wire Line
	6100 4750 6100 4800
Connection ~ 6100 4800
Wire Wire Line
	5650 4400 5650 4450
Connection ~ 5650 4450
Wire Wire Line
	5450 4300 5450 4350
Connection ~ 5450 4350
Wire Wire Line
	5950 4650 5950 4700
Connection ~ 5950 4700
Wire Wire Line
	4400 7300 3100 7300
Wire Wire Line
	9100 3700 9100 3750
Connection ~ 9100 3750
Wire Wire Line
	8900 3600 8900 3650
Connection ~ 8900 3650
Wire Wire Line
	9350 5200 9500 5200
Wire Wire Line
	9350 5300 9500 5300
Wire Wire Line
	3650 7450 3700 7450
Wire Wire Line
	3700 7450 3700 7400
Wire Wire Line
	3700 7400 4400 7400
Wire Wire Line
	3100 7500 3150 7500
Wire Wire Line
	7300 4550 7300 4500
Wire Wire Line
	4700 4550 7450 4550
Wire Wire Line
	7150 4550 7150 4600
Connection ~ 7150 4550
Wire Wire Line
	7150 4800 7150 4850
Wire Wire Line
	7000 4250 7300 4250
Wire Wire Line
	7300 4250 7300 4300
Wire Wire Line
	7150 4200 7150 4250
Connection ~ 7150 4250
Connection ~ 7000 4550
Connection ~ 7300 4550
Wire Wire Line
	900  3500 900  3600
Wire Wire Line
	900  3600 650  3600
Wire Wire Line
	650  3600 650  3550
Wire Wire Line
	750  3650 750  3600
Connection ~ 750  3600
Wire Wire Line
	900  3200 900  3300
Wire Wire Line
	650  3250 650  3200
Connection ~ 900  3200
Wire Wire Line
	2350 3850 2350 3900
Wire Wire Line
	2350 3900 2600 3900
Wire Wire Line
	2600 3900 2600 3800
Wire Wire Line
	2500 3950 2500 3900
Connection ~ 2500 3900
Wire Wire Line
	2600 3400 2600 3600
Wire Wire Line
	2350 3400 2350 3550
Connection ~ 2600 3400
Connection ~ 1350 3200
Wire Wire Line
	4700 3200 5650 3200
Wire Wire Line
	5300 3150 5300 3200
Connection ~ 5300 3200
Wire Wire Line
	4700 3600 5250 3600
Wire Wire Line
	5550 3600 6550 3600
Wire Wire Line
	2750 4200 2800 4200
Wire Wire Line
	3550 650  3550 800 
Wire Wire Line
	3550 1200 3550 1650
Wire Wire Line
	3550 1450 3600 1450
Wire Wire Line
	3300 1650 3300 1550
Connection ~ 3550 1250
Wire Wire Line
	4700 4250 4750 4250
Wire Wire Line
	9350 5400 9500 5400
Connection ~ 2350 3400
Wire Wire Line
	9350 5500 9500 5500
$Comp
L C_Small C17
U 1 1 58A63E96
P 7150 4700
F 0 "C17" H 7160 4770 50  0000 L CNN
F 1 "100nF" H 7150 4550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7150 4700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 7150 4700 50  0001 C CNN
F 4 "RST ESD Protection" H 7150 4700 60  0001 C CNN "Function"
F 5 "Yageo" H 7150 4700 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 7150 4700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 7150 4700 60  0001 C CNN "Buy"
	1    7150 4700
	1    0    0    -1  
$EndComp
$Comp
L C_Small C2
U 1 1 58A654D0
P 900 3400
F 0 "C2" H 910 3470 50  0000 L CNN
F 1 "100nF" H 910 3320 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 900 3400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 900 3400 50  0001 C CNN
F 4 "MCU decouple" H 900 3400 60  0001 C CNN "Function"
F 5 "Yageo" H 900 3400 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 900 3400 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 900 3400 60  0001 C CNN "Buy"
	1    900  3400
	1    0    0    -1  
$EndComp
$Comp
L C_Small C4
U 1 1 58A677FD
P 1350 3800
F 0 "C4" H 1360 3870 50  0000 L CNN
F 1 "100nF" H 1360 3720 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1350 3800 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 1350 3800 50  0001 C CNN
F 4 "AVCC decoupling" H 1350 3800 60  0001 C CNN "Function"
F 5 "Yageo" H 1350 3800 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 1350 3800 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 1350 3800 60  0001 C CNN "Buy"
	1    1350 3800
	1    0    0    -1  
$EndComp
$Comp
L C_Small C5
U 1 1 58A686C6
P 1750 7500
F 0 "C5" H 1760 7570 50  0000 L CNN
F 1 "100nF" H 1760 7420 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1750 7500 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 1750 7500 50  0001 C CNN
F 4 "acceleromter bypass" H 1750 7500 60  0001 C CNN "Function"
F 5 "Yageo" H 1750 7500 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 1750 7500 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 1750 7500 60  0001 C CNN "Buy"
	1    1750 7500
	0    1    1    0   
$EndComp
$Comp
L C_Small C6
U 1 1 58A6917F
P 1950 6300
F 0 "C6" V 2000 6350 50  0000 L CNN
F 1 "100nF" V 1900 6350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1950 6300 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 1950 6300 50  0001 C CNN
F 4 "accelerometer decouple" H 1950 6300 60  0001 C CNN "Function"
F 5 "Yageo" H 1950 6300 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 1950 6300 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 1950 6300 60  0001 C CNN "Buy"
	1    1950 6300
	0    1    1    0   
$EndComp
$Comp
L C_Small C9
U 1 1 58A6A243
P 2600 3700
F 0 "C9" H 2610 3770 50  0000 L CNN
F 1 "100nF" H 2610 3620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2600 3700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 2600 3700 50  0001 C CNN
F 4 "voltage reference decoupling" H 2600 3700 60  0001 C CNN "Function"
F 5 "Yageo" H 2600 3700 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 2600 3700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 2600 3700 60  0001 C CNN "Buy"
	1    2600 3700
	1    0    0    -1  
$EndComp
$Comp
L cap C14
U 1 1 58A6BA52
P 5950 4100
F 0 "C14" H 5700 4050 60  0000 C CNN
F 1 "15pf" H 6050 4150 60  0000 C CNN
F 2 "Capacitors_SMD:C_0603" H 5950 4100 60  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/MLCC/UPY-GP_NP0_16V-to-50V_14.pdf" H 5950 4100 60  0001 C CNN
F 4 "clock cap" H 5950 4100 60  0001 C CNN "Function"
F 5 "Yageo" H 5950 4100 60  0001 C CNN "MFG"
F 6 "CC0603JRNPO9BN150" H 5950 4100 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603JRNPO9BN150/311-1060-1-ND/302970" H 5950 4100 60  0001 C CNN "Buy"
	1    5950 4100
	-1   0    0    1   
$EndComp
$Comp
L C C7
U 1 1 58A6D93A
P 1950 6500
F 0 "C7" V 2000 6350 50  0000 L CNN
F 1 "4.7uF" V 1900 6200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1988 6350 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X5R_4V-to-50V_23.pdf" H 1950 6500 50  0001 C CNN
F 4 "accelerometer decoupling" V 1950 6500 60  0001 C CNN "Function"
F 5 "Yageo" V 1950 6500 60  0001 C CNN "MFG"
F 6 "CC0603KRX5R5BB475" V 1950 6500 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX5R5BB475/311-1521-1-ND/2833827" V 1950 6500 60  0001 C CNN "Buy"
	1    1950 6500
	0    1    1    0   
$EndComp
$Comp
L R_network02 J11
U 1 1 58A7091D
P 3450 7100
F 0 "J11" H 3700 7200 60  0000 C CNN
F 1 "R_network02" H 3495 6965 60  0000 C CNN
F 2 "lib:3pad_0603" H 3470 7040 60  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3470 7040 60  0001 C CNN
F 4 "Select activation type" H 3450 7100 60  0001 C CNN "Function"
F 5 "DNP, exclusive jumper" H 3450 7100 60  0001 C CNN "Notes"
F 6 "Yageo" H 3450 7100 60  0001 C CNN "MFG"
F 7 "RC0603JR-070RL" H 3450 7100 60  0001 C CNN "part_num"
F 8 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" H 3450 7100 60  0001 C CNN "Buy"
	1    3450 7100
	1    0    0    -1  
$EndComp
$Comp
L R J3
U 1 1 58A725E0
P 9300 5700
F 0 "J3" V 9250 5850 50  0000 C CNN
F 1 "J" V 9300 5700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 9230 5700 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 9300 5700 50  0001 C CNN
F 4 "connect noise circuit" V 9300 5700 60  0001 C CNN "Function"
F 5 "Yageo" V 9300 5700 60  0001 C CNN "MFG"
F 6 "RC0603JR-070RL" V 9300 5700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" V 9300 5700 60  0001 C CNN "Buy"
	1    9300 5700
	0    1    1    0   
$EndComp
$Comp
L R J6
U 1 1 58A73A05
P 3300 950
F 0 "J6" V 3250 800 50  0000 C CNN
F 1 "J" V 3300 950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3230 950 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3300 950 50  0001 C CNN
F 4 "bypass general enable" V 3300 950 60  0001 C CNN "Function"
F 5 "Yageo" V 3300 950 60  0001 C CNN "MFG"
F 6 "RC0603JR-070RL" V 3300 950 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" V 3300 950 60  0001 C CNN "Buy"
	1    3300 950 
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 800  3300 750 
$Comp
L CONN_01X02 P2
U 1 1 58A77713
P 2300 1850
F 0 "P2" H 2300 2000 50  0000 C CNN
F 1 "CONN_01X02" H 2300 1700 50  0000 C CNN
F 2 "lib:9175-700_two-pin" H 2300 1850 50  0001 C CNN
F 3 "http://datasheets.avx.com/CappedIDC_9175-700.pdf" H 2300 1850 50  0001 C CNN
F 4 "Battery connector" H 2300 1850 60  0001 C CNN "Function"
F 5 "AVX Corp." H 2300 1850 60  0001 C CNN "MFG"
F 6 "009175002701106" H 2300 1850 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329313&uq=636227693972232779" H 2300 1850 60  0001 C CNN "Buy"
	1    2300 1850
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 58A77B2E
P 2300 2250
F 0 "P3" H 2300 2400 50  0000 C CNN
F 1 "CONN_01X02" H 2300 2100 50  0000 C CNN
F 2 "lib:9175-700_two-pin" H 2300 2250 50  0001 C CNN
F 3 "http://datasheets.avx.com/CappedIDC_9175-700.pdf" H 2300 2250 50  0001 C CNN
F 4 "Battery connector" H 2300 2250 60  0001 C CNN "Function"
F 5 "AVX Corp." H 2300 2250 60  0001 C CNN "MFG"
F 6 "009175002701106" H 2300 2250 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329313&uq=636227693972232779" H 2300 2250 60  0001 C CNN "Buy"
	1    2300 2250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P4
U 1 1 58A77F43
P 2300 2600
F 0 "P4" H 2400 2700 50  0000 C CNN
F 1 "CONN_01X02" H 2300 2450 50  0000 C CNN
F 2 "lib:9175-700_two-pin" H 2300 2600 50  0001 C CNN
F 3 "http://datasheets.avx.com/CappedIDC_9175-700.pdf" H 2300 2600 50  0001 C CNN
F 4 "Battery connector" H 2300 2600 60  0001 C CNN "Function"
F 5 "AVX Corp." H 2300 2600 60  0001 C CNN "MFG"
F 6 "009175002701106" H 2300 2600 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=217329313&uq=636227693972232779" H 2300 2600 60  0001 C CNN "Buy"
	1    2300 2600
	1    0    0    -1  
$EndComp
$Comp
L R R15
U 1 1 58A7C616
P 5900 5100
F 0 "R15" V 6000 5100 50  0000 C CNN
F 1 "120" V 5900 5100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5830 5100 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 5900 5100 50  0001 C CNN
F 4 "LED resistor" V 5900 5100 60  0001 C CNN "Function"
F 5 "Yageo" V 5900 5100 60  0001 C CNN "MFG"
F 6 "RC0603JR-07120RL" V 5900 5100 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-07120RL/311-120GRCT-ND/729653" V 5900 5100 60  0001 C CNN "Buy"
	1    5900 5100
	0    1    1    0   
$EndComp
$Comp
L R R16
U 1 1 58A7CF80
P 3550 2100
F 0 "R16" V 3650 2100 50  0000 C CNN
F 1 "120" V 3550 2100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3480 2100 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3550 2100 50  0001 C CNN
F 4 "LED resistor" V 3550 2100 60  0001 C CNN "Function"
F 5 "Yageo" V 3550 2100 60  0001 C CNN "MFG"
F 6 "RC0603JR-07120RL" V 3550 2100 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-07120RL/311-120GRCT-ND/729653" V 3550 2100 60  0001 C CNN "Buy"
	1    3550 2100
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 58A8178C
P 5400 3500
F 0 "R3" V 5350 3650 50  0000 C CNN
F 1 "4.7k" V 5400 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5330 3500 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 5400 3500 50  0001 C CNN
F 4 "programming resistor" V 5400 3500 60  0001 C CNN "Function"
F 5 "Yageo" V 5400 3500 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 5400 3500 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 5400 3500 60  0001 C CNN "Buy"
	1    5400 3500
	0    1    1    0   
$EndComp
$Comp
L R R4
U 1 1 58A81B81
P 5400 3600
F 0 "R4" V 5350 3750 50  0000 C CNN
F 1 "4.7k" V 5400 3600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5330 3600 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 5400 3600 50  0001 C CNN
F 4 "programming resistor" V 5400 3600 60  0001 C CNN "Function"
F 5 "Yageo" V 5400 3600 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 5400 3600 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 5400 3600 60  0001 C CNN "Buy"
	1    5400 3600
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 58A823BD
P 6350 4300
F 0 "R5" V 6400 4150 50  0000 C CNN
F 1 "4.7k" V 6350 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6280 4300 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 6350 4300 50  0001 C CNN
F 4 "SCL pullup" V 6350 4300 60  0001 C CNN "Function"
F 5 "Yageo" V 6350 4300 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 6350 4300 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 6350 4300 60  0001 C CNN "Buy"
	1    6350 4300
	-1   0    0    1   
$EndComp
$Comp
L R R6
U 1 1 58A829A2
P 6500 4200
F 0 "R6" V 6550 4050 50  0000 C CNN
F 1 "4.7k" V 6500 4200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6430 4200 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 6500 4200 50  0001 C CNN
F 4 "SDA pullup" V 6500 4200 60  0001 C CNN "Function"
F 5 "Yageo" V 6500 4200 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 6500 4200 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 6500 4200 60  0001 C CNN "Buy"
	1    6500 4200
	-1   0    0    1   
$EndComp
$Comp
L R R7
U 1 1 58A83489
P 7000 4400
F 0 "R7" V 7050 4250 50  0000 C CNN
F 1 "4.7k" V 7000 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6930 4400 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 7000 4400 50  0001 C CNN
F 4 "RST ESD protection" V 7000 4400 60  0001 C CNN "Function"
F 5 "Yageo" V 7000 4400 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 7000 4400 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 7000 4400 60  0001 C CNN "Buy"
	1    7000 4400
	-1   0    0    1   
$EndComp
$Comp
L R R21
U 1 1 58A844D1
P 4200 850
F 0 "R21" V 4100 800 50  0000 C CNN
F 1 "4.7k" V 4200 850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4130 850 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 4200 850 50  0001 C CNN
F 4 "PMOS pullup" V 4200 850 60  0001 C CNN "Function"
F 5 "Yageo" V 4200 850 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 4200 850 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 4200 850 60  0001 C CNN "Buy"
	1    4200 850 
	-1   0    0    1   
$EndComp
$EndSCHEMATC
