EESchema Schematic File Version 2
LIBS:STINGR_drifter-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:STINGR_drifter-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 7
Title "Stokes Drifter"
Date "2017-02-04"
Rev "v2.0"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RXM-GPS-RM MOD1
U 1 1 58925837
P 2550 2150
F 0 "MOD1" H 2350 2050 60  0000 C CNN
F 1 "RXM-GPS-RM" H 2550 2150 60  0000 C CNN
F 2 "lib:RXM-GPS-RM" H 2550 2150 60  0001 C CNN
F 3 "" H 2550 2150 60  0000 C CNN
F 4 "GPS module" H 2550 2150 60  0001 C CNN "Field4"
	1    2550 2150
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG3
U 1 1 589264D4
P 3650 2300
F 0 "#FLG3" H 3650 2395 50  0001 C CNN
F 1 "PWR_FLAG" H 3650 2480 50  0000 C CNN
F 2 "" H 3650 2300 50  0000 C CNN
F 3 "" H 3650 2300 50  0000 C CNN
	1    3650 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 2050 4450 2050
NoConn ~ 3500 2150
NoConn ~ 1900 1550
NoConn ~ 1900 1850
Text HLabel 1750 1650 0    60   Input ~ 0
GPS_tx
Text HLabel 1750 1750 0    60   Input ~ 0
GPS_rx
Text HLabel 1750 1950 0    60   Input ~ 0
GPS_ant
Wire Wire Line
	1750 1950 1900 1950
Wire Wire Line
	1750 1750 1900 1750
Wire Wire Line
	1750 1650 1900 1650
Wire Wire Line
	3500 1950 3700 1950
$Comp
L GND #PWR28
U 1 1 589285D1
P 3450 1300
F 0 "#PWR28" H 3450 1050 50  0001 C CNN
F 1 "GND" H 3450 1150 50  0000 C CNN
F 2 "" H 3450 1300 50  0000 C CNN
F 3 "" H 3450 1300 50  0000 C CNN
	1    3450 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 1550 3550 1550
Wire Wire Line
	3550 1250 3550 1850
Connection ~ 3550 1850
Wire Wire Line
	3500 1750 3550 1750
Connection ~ 3550 1750
Wire Wire Line
	3500 1650 3550 1650
Connection ~ 3550 1650
Wire Wire Line
	3650 2300 3650 2050
Connection ~ 3650 2050
Text Label 3800 2050 0    60   ~ 0
GPS_pwr
$Comp
L C C26
U 1 1 5893B704
P 4200 2350
F 0 "C26" V 4250 2200 50  0000 L CNN
F 1 "4.7uF" V 4150 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4238 2200 50  0001 C CNN
F 3 "" H 4200 2350 50  0000 C CNN
F 4 "GPS decoupling" V 4200 2350 60  0001 C CNN "Field4"
	1    4200 2350
	1    0    0    -1  
$EndComp
$Comp
L C C27
U 1 1 5893C499
P 4450 2350
F 0 "C27" V 4500 2200 50  0000 L CNN
F 1 "100nF" V 4400 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4488 2200 50  0001 C CNN
F 3 "" H 4450 2350 50  0000 C CNN
F 4 "GPS decoupling" V 4450 2350 60  0001 C CNN "Field4"
	1    4450 2350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR30
U 1 1 5896E9DB
P 4300 2600
F 0 "#PWR30" H 4300 2350 50  0001 C CNN
F 1 "GND" H 4300 2450 50  0000 C CNN
F 2 "" H 4300 2600 50  0000 C CNN
F 3 "" H 4300 2600 50  0000 C CNN
	1    4300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2500 4200 2550
Wire Wire Line
	4200 2550 4450 2550
Wire Wire Line
	4450 2550 4450 2500
Wire Wire Line
	4300 2600 4300 2550
Connection ~ 4300 2550
Wire Wire Line
	4200 2050 4200 2200
Wire Wire Line
	4450 1600 4450 2200
Connection ~ 4200 2050
Wire Wire Line
	3550 1850 3500 1850
Wire Wire Line
	3550 1250 3450 1250
Wire Wire Line
	3450 1250 3450 1300
Connection ~ 3550 1550
$Comp
L VCC #PWR29
U 1 1 5898CFA7
P 3700 1500
F 0 "#PWR29" H 3700 1350 50  0001 C CNN
F 1 "VCC" H 3700 1650 50  0000 C CNN
F 2 "" H 3700 1500 50  0000 C CNN
F 3 "" H 3700 1500 50  0000 C CNN
	1    3700 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1950 3700 1500
Text GLabel 4400 1600 0    60   Input ~ 0
EN_PWR
Wire Wire Line
	4400 1600 4450 1600
Connection ~ 4450 2050
$EndSCHEMATC
