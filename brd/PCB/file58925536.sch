EESchema Schematic File Version 2
LIBS:STINGR_drifter-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:STINGR_drifter-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 6 7
Title "Stokes Drifter"
Date "2017-02-04"
Rev "v2.0"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L STX3 MOD2
U 1 1 5892563B
P 5100 2000
F 0 "MOD2" H 5000 1350 60  0000 C CNN
F 1 "STX3" H 5500 1350 60  0000 C CNN
F 2 "lib:STX3" H 6200 2100 60  0001 C CNN
F 3 "" H 6200 2100 60  0000 C CNN
F 4 "Satellite modem" H 5100 2000 60  0001 C CNN "Field4"
	1    5100 2000
	1    0    0    -1  
$EndComp
$Comp
L R_Small J5
U 1 1 58929C8F
P 4150 2250
F 0 "J5" V 4100 2300 50  0000 L CNN
F 1 "J" V 4100 2150 50  0001 L CNN
F 2 "Resistors_SMD:R_0603" H 4150 2250 50  0001 C CNN
F 3 "" H 4150 2250 50  0000 C CNN
F 4 "STX3_test1" V 4150 2250 60  0001 C CNN "Field4"
	1    4150 2250
	0    1    1    0   
$EndComp
$Comp
L R_Small J4
U 1 1 58929F94
P 4150 2150
F 0 "J4" V 4100 2200 50  0000 L CNN
F 1 "J" V 4100 2050 50  0001 L CNN
F 2 "Resistors_SMD:R_0603" H 4150 2150 50  0001 C CNN
F 3 "" H 4150 2150 50  0000 C CNN
F 4 "STX3_test2" V 4150 2150 60  0001 C CNN "Field4"
	1    4150 2150
	0    1    1    0   
$EndComp
$Comp
L GND #PWR32
U 1 1 5892A042
P 3950 2300
F 0 "#PWR32" H 3950 2050 50  0001 C CNN
F 1 "GND" H 3950 2150 50  0000 C CNN
F 2 "" H 3950 2300 50  0000 C CNN
F 3 "" H 3950 2300 50  0000 C CNN
	1    3950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 2150 4650 2150
Wire Wire Line
	4650 2250 4250 2250
Wire Wire Line
	3950 2150 3950 2300
Wire Wire Line
	3950 2150 4050 2150
Wire Wire Line
	4050 2250 3950 2250
Connection ~ 3950 2250
Wire Wire Line
	5900 1450 6000 1450
Wire Wire Line
	6000 1450 6000 2650
Wire Wire Line
	5900 2550 6000 2550
Connection ~ 6000 2550
Wire Wire Line
	5900 2450 6000 2450
Connection ~ 6000 2450
Wire Wire Line
	5900 2350 6000 2350
Connection ~ 6000 2350
Wire Wire Line
	5900 2250 6000 2250
Connection ~ 6000 2250
Wire Wire Line
	5900 2150 6000 2150
Connection ~ 6000 2150
Wire Wire Line
	5900 2050 6000 2050
Connection ~ 6000 2050
Wire Wire Line
	5900 1950 6000 1950
Connection ~ 6000 1950
Wire Wire Line
	5900 1850 6000 1850
Connection ~ 6000 1850
Wire Wire Line
	5900 1750 6000 1750
Connection ~ 6000 1750
Wire Wire Line
	5900 1650 6000 1650
Connection ~ 6000 1650
Wire Wire Line
	5900 1550 6000 1550
Connection ~ 6000 1550
$Comp
L GND #PWR33
U 1 1 5892A65C
P 6000 2650
F 0 "#PWR33" H 6000 2400 50  0001 C CNN
F 1 "GND" H 6000 2500 50  0000 C CNN
F 2 "" H 6000 2650 50  0000 C CNN
F 3 "" H 6000 2650 50  0000 C CNN
	1    6000 2650
	1    0    0    -1  
$EndComp
NoConn ~ 4650 2450
Text HLabel 4500 1850 0    60   Input ~ 0
STX3_transmit
Text HLabel 4500 1950 0    60   Input ~ 0
STX3_tx
Text HLabel 4500 2050 0    60   Input ~ 0
STX3_rx
Wire Wire Line
	4500 2050 4650 2050
Wire Wire Line
	4500 1950 4650 1950
Wire Wire Line
	4500 1850 4650 1850
Wire Wire Line
	3800 2550 4650 2550
Text HLabel 4500 1450 0    60   Input ~ 0
STX3_CTS
Text HLabel 4500 1550 0    60   Input ~ 0
STX3_RTS
Wire Wire Line
	4500 1550 4650 1550
Wire Wire Line
	4500 1450 4650 1450
Text HLabel 4500 1750 0    60   Input ~ 0
STX3_ant
Wire Wire Line
	4500 1750 4650 1750
Wire Wire Line
	3800 1650 3800 2550
Connection ~ 3800 1700
Wire Wire Line
	3800 1650 4650 1650
Connection ~ 3400 1700
Text Label 3000 1700 0    60   ~ 0
STX3_pwr
$Comp
L C C29
U 1 1 5893BD21
P 3550 1950
F 0 "C29" V 3600 1800 50  0000 L CNN
F 1 "4.7uF" V 3400 1850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3588 1800 50  0001 C CNN
F 3 "" H 3550 1950 50  0000 C CNN
F 4 "STX3 decoupling" V 3550 1950 60  0001 C CNN "Field4"
	1    3550 1950
	-1   0    0    -1  
$EndComp
$Comp
L C C28
U 1 1 5893C7EF
P 3250 1950
F 0 "C28" V 3300 1800 50  0000 L CNN
F 1 "100nF" V 3400 1850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3288 1800 50  0001 C CNN
F 3 "" H 3250 1950 50  0000 C CNN
F 4 "STX3 decoupling" V 3250 1950 60  0001 C CNN "Field4"
	1    3250 1950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 1700 3250 1800
Connection ~ 3250 1700
Wire Wire Line
	3550 1700 3550 1800
Connection ~ 3550 1700
$Comp
L GND #PWR31
U 1 1 5896D95C
P 3400 2200
F 0 "#PWR31" H 3400 1950 50  0001 C CNN
F 1 "GND" H 3400 2050 50  0000 C CNN
F 2 "" H 3400 2200 50  0000 C CNN
F 3 "" H 3400 2200 50  0000 C CNN
	1    3400 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2100 3250 2150
Wire Wire Line
	3250 2150 3550 2150
Wire Wire Line
	3550 2150 3550 2100
Wire Wire Line
	3400 2200 3400 2150
Connection ~ 3400 2150
Wire Wire Line
	2800 1700 3800 1700
Text GLabel 2750 1350 0    60   Input ~ 0
EN_PWR
Wire Wire Line
	2750 1350 2800 1350
Text Label 4300 2150 0    60   ~ 0
TEST2
Text Label 4300 2250 0    60   ~ 0
TEST1
Wire Wire Line
	2800 1350 2800 1700
$EndSCHEMATC
