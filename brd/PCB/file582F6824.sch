EESchema Schematic File Version 2
LIBS:STINGR_drifter-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:STINGR_drifter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "Stokes Drifter"
Date ""
Rev ""
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LMT70YFQR U5
U 1 1 582F6BDE
P 1900 2150
F 0 "U5" H 2200 2300 60  0000 C CNN
F 1 "LMT70YFQR" H 1850 2250 60  0000 C CNN
F 2 "lib:BGA_4-pitch_0.4mm" H 1900 2150 60  0001 C CNN
F 3 "" H 1900 2150 60  0000 C CNN
	1    1900 2150
	1    0    0    -1  
$EndComp
$Comp
L LMT70YFQR U6
U 1 1 582F6C08
P 1900 3000
F 0 "U6" H 2200 3150 60  0000 C CNN
F 1 "LMT70YFQR" H 1850 3100 60  0000 C CNN
F 2 "lib:BGA_4-pitch_0.4mm" H 1900 3000 60  0001 C CNN
F 3 "" H 1900 3000 60  0000 C CNN
	1    1900 3000
	1    0    0    -1  
$EndComp
Text Notes 2500 2050 0    60   ~ 0
Off-board probes
Text HLabel 1150 2250 0    60   Input ~ 0
Probe1
Text HLabel 1150 3100 0    60   Input ~ 0
Probe2
Wire Wire Line
	2500 3100 2300 3100
Wire Wire Line
	2300 3000 2650 3000
Connection ~ 2500 3000
Connection ~ 2500 2250
Wire Wire Line
	2300 2150 2650 2150
Connection ~ 2500 2150
Wire Wire Line
	1400 3100 1150 3100
Wire Wire Line
	1400 2250 1150 2250
Wire Wire Line
	2500 2250 2300 2250
Wire Notes Line
	1200 3550 4400 3550
Text HLabel 2650 2150 2    60   Input ~ 0
T_VCC1
Wire Wire Line
	2500 2150 2500 2250
Wire Wire Line
	2500 3000 2500 3100
Text HLabel 2650 3000 2    60   Input ~ 0
T_VCC2
Text HLabel 1150 2150 0    60   Input ~ 0
T_GND1
Text HLabel 1150 3000 0    60   Input ~ 0
T_GND2
Wire Wire Line
	1400 3000 1150 3000
Wire Wire Line
	1400 2150 1150 2150
$EndSCHEMATC
