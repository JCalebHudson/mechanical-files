EESchema Schematic File Version 2
LIBS:STINGR_drifter-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:STINGR_drifter-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 8
Title "Stokes Drifter"
Date "2017-02-04"
Rev "v2.0"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AP3012 IC5
U 1 1 5859748E
P 2300 1350
F 0 "IC5" H 2500 1550 60  0000 C CNN
F 1 "AP3012" H 2100 1550 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 2500 1750 60  0001 C CNN
F 3 "" H 2500 1750 60  0001 C CNN
F 4 "26V adjustable SMPS" H 2300 1350 60  0001 C CNN "Field4"
	1    2300 1350
	-1   0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 58597495
P 2400 1000
F 0 "L1" V 2350 1000 50  0000 C CNN
F 1 "2.2uH" V 2500 1000 50  0000 C CNN
F 2 "Inductors_NEOSID:Neosid_Inductor_SM-NE29_SMD1008" V 2600 700 50  0001 C CNN
F 3 "" H 2400 1000 50  0000 C CNN
F 4 "buzzer inductor" V 2400 1000 60  0001 C CNN "Field4"
	1    2400 1000
	0    -1   -1   0   
$EndComp
$Comp
L C C20
U 1 1 5859749D
P 3700 1400
F 0 "C20" H 3550 1500 50  0000 L CNN
F 1 "1uF" H 3550 1300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3738 1250 50  0001 C CNN
F 3 "" H 3700 1400 50  0000 C CNN
F 4 "26V SMPS smoothing" V 3700 1400 60  0001 C CNN "Field4"
	1    3700 1400
	-1   0    0    1   
$EndComp
$Comp
L D_Small D8
U 1 1 585974AA
P 3050 1000
F 0 "D8" H 3050 900 50  0000 L CNN
F 1 "D_Small" H 2700 1050 50  0001 L CNN
F 2 "lib:DB2J317" V 3050 1000 50  0001 C CNN
F 3 "" V 3050 1000 50  0000 C CNN
F 4 "26V SMPS diode" H 3050 1000 60  0001 C CNN "Field4"
	1    3050 1000
	-1   0    0    1   
$EndComp
$Comp
L POT-RESCUE-STINGR_drifter RV2
U 1 1 585974B2
P 3400 1400
F 0 "RV2" H 3400 1320 50  0000 C CNN
F 1 "POT" H 3400 1400 50  0000 C CNN
F 2 "lib:Bourns-TC33X" H 3400 1400 50  0001 C CNN
F 3 "" H 3400 1400 50  0000 C CNN
F 4 "50k" H 3400 1400 60  0001 C CNN "Max_value"
F 5 "adjust buzzer voltage" H 3400 1400 60  0001 C CNN "Field4"
	1    3400 1400
	0    -1   -1   0   
$EndComp
$Comp
L R R13
U 1 1 585974B9
P 3200 1200
F 0 "R13" V 3300 1200 50  0000 C CNN
F 1 "20k" V 3200 1200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3130 1200 50  0001 C CNN
F 3 "" H 3200 1200 50  0000 C CNN
F 4 "fix buzzer voltage" V 3200 1200 60  0001 C CNN "Field4"
	1    3200 1200
	-1   0    0    1   
$EndComp
$Comp
L R R14
U 1 1 585974C0
P 3200 1600
F 0 "R14" V 3300 1600 50  0000 C CNN
F 1 "1k" V 3200 1600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3130 1600 50  0001 C CNN
F 3 "" H 3200 1600 50  0000 C CNN
F 4 "Fix buzzer voltage" V 3200 1600 60  0001 C CNN "Field4"
	1    3200 1600
	-1   0    0    1   
$EndComp
Text Notes 2900 2100 0    60   ~ 0
Populate resistors OR pot to set voltage\nVout = 1.25*(1+R100/R200) < 29V\nR100 = R200*(Vout/1.25-1)
$Comp
L C C19
U 1 1 585974CE
P 1650 1600
F 0 "C19" H 1750 1650 50  0000 L CNN
F 1 "1uF" H 1750 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1688 1450 50  0001 C CNN
F 3 "" H 1650 1600 50  0000 C CNN
F 4 "26V SMPS input decoupling" V 1650 1600 60  0001 C CNN "Field4"
	1    1650 1600
	-1   0    0    1   
$EndComp
$Comp
L Led_Small_drifter D7
U 1 1 585974D5
P 1950 1650
F 0 "D7" H 2000 1600 50  0000 L CNN
F 1 "Led_Small_drifter" H 1700 1750 50  0001 L CNN
F 2 "lib:3.2x2.7mm_LED" V 1950 1650 50  0001 C CNN
F 3 "" V 1950 1650 50  0000 C CNN
F 4 "Buzzer led indicator" H 1950 1650 60  0001 C CNN "Field4"
	1    1950 1650
	-1   0    0    1   
$EndComp
$Comp
L R R12
U 1 1 585974DC
P 2650 1650
F 0 "R12" V 2750 1650 50  0000 C CNN
F 1 "120" V 2650 1650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2580 1650 50  0001 C CNN
F 3 "" H 2650 1650 50  0000 C CNN
F 4 "LED resistor" V 2650 1650 60  0001 C CNN "Field4"
	1    2650 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 1400 3250 1400
Wire Wire Line
	2800 1650 2850 1650
Wire Wire Line
	2050 1650 2500 1650
Wire Wire Line
	1750 1650 1850 1650
Wire Wire Line
	3150 1000 3900 1000
Wire Wire Line
	3400 1000 3400 1250
Wire Wire Line
	3200 1050 3200 1000
Connection ~ 3200 1000
Wire Wire Line
	3200 1350 3200 1450
Connection ~ 3200 1400
Wire Wire Line
	3400 1800 3400 1550
Wire Wire Line
	3200 1800 3200 1750
Wire Wire Line
	3700 1000 3700 1250
Connection ~ 3400 1000
Connection ~ 3700 1800
Connection ~ 3400 1800
Connection ~ 3700 1000
Wire Wire Line
	1650 1800 3900 1800
Wire Wire Line
	1650 1250 1800 1250
Connection ~ 1650 1000
Wire Wire Line
	2950 1400 2950 1450
Wire Wire Line
	2950 1450 2850 1450
Connection ~ 3200 1800
Wire Wire Line
	3700 1800 3700 1550
Connection ~ 2850 1800
Connection ~ 1650 1250
Wire Wire Line
	2850 1250 2900 1250
Wire Wire Line
	2900 1250 2900 1000
Connection ~ 2900 1000
Wire Wire Line
	2900 1350 2850 1350
Wire Wire Line
	2900 1800 2900 1350
Connection ~ 2900 1800
Wire Wire Line
	3900 1000 3900 1250
Wire Wire Line
	3900 1250 4000 1250
Wire Wire Line
	3900 1450 4000 1450
Wire Wire Line
	3900 1800 3900 1450
Text HLabel 1600 1350 0    60   Input ~ 0
Enable
$Comp
L GND #PWR28
U 1 1 5896C166
P 1650 1900
F 0 "#PWR28" H 1650 1650 50  0001 C CNN
F 1 "GND" H 1650 1750 50  0000 C CNN
F 2 "" H 1650 1900 50  0000 C CNN
F 3 "" H 1650 1900 50  0000 C CNN
	1    1650 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1000 1650 1000
Wire Wire Line
	2700 1000 2950 1000
Wire Wire Line
	1750 1350 1750 1650
Connection ~ 1750 1450
Wire Wire Line
	1750 1450 1800 1450
Wire Wire Line
	1750 1350 1600 1350
Wire Wire Line
	1650 1750 1650 1900
Wire Wire Line
	1650 800  1650 1450
Connection ~ 1650 1800
Wire Wire Line
	2850 1650 2850 1800
Text Label 3250 1000 0    60   ~ 0
buzz_voltage
Text Label 2100 1650 0    60   ~ 0
buzz_LED
$Comp
L Buzzer BZ1
U 1 1 589B41CC
P 4100 1350
F 0 "BZ1" H 4250 1400 50  0000 L CNN
F 1 "Buzzer" H 4250 1300 50  0000 L CNN
F 2 "lib:9175-700_two-pin" V 4075 1450 50  0001 C CNN
F 3 "" V 4075 1450 50  0000 C CNN
	1    4100 1350
	1    0    0    -1  
$EndComp
Text Label 2950 1400 0    60   ~ 0
buzz_FB
Text Label 2850 1000 1    60   ~ 0
buzz_coil
Text GLabel 1700 800  2    60   Input ~ 0
PWR_EN
Wire Wire Line
	1700 800  1650 800 
$EndSCHEMATC
