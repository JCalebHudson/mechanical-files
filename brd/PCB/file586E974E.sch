EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:drifter-rescue
LIBS:drifter-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 4
Title "Stokes Drifter"
Date "2017-02-15"
Rev "v2.2"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L_Small L2
U 1 1 586E9D8C
P 3450 1550
F 0 "L2" V 3500 1500 50  0000 L CNN
F 1 "2.2uH" V 3400 1450 50  0000 L CNN
F 2 "Inductors_NEOSID:Neosid_Inductor_SM-NE29_SMD1008" H 3450 1550 50  0001 C CNN
F 3 "http://media.digikey.com/pdf/Data%20Sheets/TDK%20PDFs/vls252012.pdf" H 3450 1550 50  0001 C CNN
F 4 "3.3V supply inductor" V 3450 1550 60  0001 C CNN "Function"
F 5 "TDK Corporation" V 3450 1550 60  0001 C CNN "MFG"
F 6 "VLS252012T-2R2M1R3" H 3450 1550 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/tdk-corporation/VLS252012T-2R2M1R3/445-3670-1-ND/1856638" V 3450 1550 60  0001 C CNN "Buy"
F 8 "Value" H 3450 1550 60  0001 C CNN "Notes"
	1    3450 1550
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C22
U 1 1 586E9F45
P 3950 1700
F 0 "C22" H 3960 1770 50  0000 L CNN
F 1 "10uF" H 3960 1620 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3950 1700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 3950 1700 50  0001 C CNN
F 4 "Power supply smoothing" H 3950 1700 60  0001 C CNN "Function"
F 5 "Yageo" H 3950 1700 60  0001 C CNN "MFG"
F 6 "CC1206KKX7R8BB106" H 3950 1700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC1206KKX7R8BB106/311-1959-1-ND/5195861" H 3950 1700 60  0001 C CNN "Buy"
F 8 "Value" H 3950 1700 60  0001 C CNN "Notes"
	1    3950 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 1850 2350 1850
Wire Wire Line
	2100 1850 2100 1800
Wire Wire Line
	2100 1600 2100 1550
Wire Wire Line
	1950 1550 2350 1550
Wire Wire Line
	2350 1650 2300 1650
Wire Wire Line
	2350 1750 2300 1750
Wire Wire Line
	2300 1750 2300 1850
Connection ~ 2300 1850
Wire Wire Line
	3300 1550 3350 1550
Wire Wire Line
	3300 1850 4550 1850
Wire Wire Line
	3950 1850 3950 1800
Wire Wire Line
	3550 1550 4650 1550
Wire Wire Line
	3950 1550 3950 1600
Wire Wire Line
	3300 1650 3600 1650
Wire Wire Line
	3600 1650 3600 1550
Connection ~ 3600 1550
Connection ~ 3950 1550
Connection ~ 3950 1850
Wire Wire Line
	4150 1850 4150 1800
Connection ~ 4150 1850
Wire Wire Line
	4150 1600 4150 1550
Connection ~ 4150 1550
Text HLabel 1950 1550 0    60   Input ~ 0
BATT
Text HLabel 1950 1850 0    60   Input ~ 0
GND
Connection ~ 2100 1850
Connection ~ 2100 1550
Wire Wire Line
	4550 1850 4550 1800
Wire Wire Line
	4550 1550 4550 1600
Text HLabel 4650 1550 2    60   Input ~ 0
Power_out
Connection ~ 4550 1550
Wire Wire Line
	3400 1850 3400 2050
Wire Wire Line
	3400 2050 2200 2050
Wire Wire Line
	2200 2050 2200 1850
Connection ~ 2200 1850
Connection ~ 3400 1850
$Comp
L CP1_Small C21
U 1 1 586ECF75
P 2100 1700
F 0 "C21" H 2110 1770 50  0000 L CNN
F 1 "10uF" H 2110 1620 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2100 1700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 2100 1700 50  0001 C CNN
F 4 "VBATT decoupling" H 2100 1700 60  0001 C CNN "Function"
F 5 "Yageo" H 2100 1700 60  0001 C CNN "MFG"
F 6 "CC1206KKX7R8BB106" H 2100 1700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC1206KKX7R8BB106/311-1959-1-ND/5195861" H 2100 1700 60  0001 C CNN "Buy"
F 8 "Must be rated at least 25V" H 2100 1700 60  0001 C CNN "Notes"
	1    2100 1700
	1    0    0    -1  
$EndComp
Text HLabel 1950 1400 0    60   Input ~ 0
Enable
Wire Wire Line
	2300 1650 2300 1400
Wire Wire Line
	2300 1400 1950 1400
Wire Wire Line
	3300 1750 3350 1750
Wire Wire Line
	3350 1750 3350 1850
Connection ~ 3350 1850
$Comp
L TEST VCC1
U 1 1 5896FBDD
P 3700 1500
F 0 "VCC1" H 3700 1800 50  0000 C BNN
F 1 "TEST" H 3700 1750 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 3700 1500 50  0001 C CNN
F 3 "" H 3700 1500 50  0000 C CNN
	1    3700 1500
	1    0    0    -1  
$EndComp
$Comp
L TEST GND1
U 1 1 5896FDC8
P 3800 1250
F 0 "GND1" H 3800 1550 50  0000 C BNN
F 1 "TEST" H 3800 1500 50  0000 C CNN
F 2 "lib:SolderWirePad_single_0-8mmDrill" H 3800 1250 50  0001 C CNN
F 3 "" H 3800 1250 50  0000 C CNN
	1    3800 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1250 3800 1850
Connection ~ 3800 1850
Wire Wire Line
	3700 1500 3700 1550
Connection ~ 3700 1550
Text Label 3300 1550 1    60   ~ 0
3V3_inductor
$Comp
L TPS62162 IC6
U 1 1 586E9D13
P 2850 1650
F 0 "IC6" H 2850 1350 60  0000 C CNN
F 1 "TPS62162" H 2850 1850 60  0000 C CNN
F 2 "lib:VSSOP-8" H 2750 1650 60  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tps62161.pdf" H 2750 1650 60  0001 C CNN
F 4 "Value" H 2850 1650 60  0001 C CNN "Function"
F 5 "Texas Instruments" H 2850 1650 60  0001 C CNN "MFG"
F 6 "TPS62162DSGT" H 2850 1650 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/texas-instruments/TPS62162DSGT/296-29897-1-ND/2782627" H 2850 1650 60  0001 C CNN "Buy"
F 8 "Value" H 2850 1650 60  0001 C CNN "Notes"
F 9 "3.3V fixed SMPS" H 2850 1650 60  0001 C CNN "Field4"
	1    2850 1650
	1    0    0    -1  
$EndComp
$Comp
L C_Small C25
U 1 1 58A66B3C
P 4550 1700
F 0 "C25" H 4560 1770 50  0000 L CNN
F 1 "100nF" H 4560 1620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4550 1700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 4550 1700 50  0001 C CNN
F 4 "Power supply smoothing" H 4550 1700 60  0001 C CNN "Function"
F 5 "Yageo" H 4550 1700 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 4550 1700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 4550 1700 60  0001 C CNN "Buy"
F 8 "Value" H 4550 1700 60  0001 C CNN "Notes"
	1    4550 1700
	1    0    0    -1  
$EndComp
$Comp
L C_Small C23
U 1 1 58A6E1D7
P 4150 1700
F 0 "C23" H 4160 1770 50  0000 L CNN
F 1 "10uF" H 4160 1620 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4150 1700 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 4150 1700 50  0001 C CNN
F 4 "Power supply smoothing" H 4150 1700 60  0001 C CNN "Function"
F 5 "Yageo" H 4150 1700 60  0001 C CNN "MFG"
F 6 "CC1206KKX7R8BB106" H 4150 1700 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC1206KKX7R8BB106/311-1959-1-ND/5195861" H 4150 1700 60  0001 C CNN "Buy"
F 8 "Value" H 4150 1700 60  0001 C CNN "Notes"
	1    4150 1700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
