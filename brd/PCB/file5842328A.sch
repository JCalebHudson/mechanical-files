EESchema Schematic File Version 2
LIBS:STINGR_drifter-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:STINGR_drifter-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 7
Title "Stokes Drifter"
Date "2017-02-04"
Rev "v2.0"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R8
U 1 1 58423384
P 1650 1500
F 0 "R8" V 1730 1500 50  0000 C CNN
F 1 "120" V 1650 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 1580 1500 50  0001 C CNN
F 3 "" H 1650 1500 50  0000 C CNN
F 4 "noise circuit" V 1650 1500 60  0001 C CNN "Field4"
	1    1650 1500
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 58423403
P 1650 1900
F 0 "R9" V 1730 1900 50  0000 C CNN
F 1 "120" V 1650 1900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 1580 1900 50  0001 C CNN
F 3 "" H 1650 1900 50  0000 C CNN
F 4 "noise circuit" V 1650 1900 60  0001 C CNN "Field4"
	1    1650 1900
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 58423427
P 1900 2150
F 0 "R10" V 1980 2150 50  0000 C CNN
F 1 "1k" V 1900 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 1830 2150 50  0001 C CNN
F 3 "" H 1900 2150 50  0000 C CNN
F 4 "noise circuit" V 1900 2150 60  0001 C CNN "Field4"
	1    1900 2150
	0    -1   -1   0   
$EndComp
$Comp
L C C18
U 1 1 58423476
P 1650 2400
F 0 "C18" H 1675 2500 50  0000 L CNN
F 1 "100n" H 1450 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1688 2250 50  0001 C CNN
F 3 "" H 1650 2400 50  0000 C CNN
F 4 "noise circuit" H 1650 2400 60  0001 C CNN "Field4"
	1    1650 2400
	1    0    0    -1  
$EndComp
Text HLabel 2150 1700 2    60   Input ~ 0
N_VREF
Text HLabel 2550 2150 2    60   Input ~ 0
TIMER_IN
$Comp
L POT-RESCUE-STINGR_drifter RV1
U 1 1 5842713D
P 2300 2150
F 0 "RV1" H 2300 2070 50  0000 C CNN
F 1 "POT" H 2300 2150 50  0000 C CNN
F 2 "lib:Bourns-TC33X" H 2350 2350 50  0000 C CNN
F 3 "" H 2300 2150 50  0000 C CNN
F 4 "50k" H 2300 2150 60  0001 C CNN "Max_resistance"
F 5 "adjust noise circuit" H 2300 2150 60  0001 C CNN "Field4"
	1    2300 2150
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 58429B9B
P 2300 2400
F 0 "R11" V 2380 2400 50  0000 C CNN
F 1 "50k" V 2300 2400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2230 2400 50  0001 C CNN
F 3 "" H 2300 2400 50  0000 C CNN
F 4 "fix noise circuit" V 2300 2400 60  0001 C CNN "Field4"
	1    2300 2400
	0    -1   -1   0   
$EndComp
Text Notes 1800 1600 0    60   ~ 0
This is a filter that adds a small amount\nof noise to the reference voltage for \nthe purposes of oversampling and \ndecimation
Text Notes 2500 2550 0    60   ~ 0
populate the potentiometer\nto set the noise level, OR \npopulate the resistor in parallel\nto fix it.
Wire Wire Line
	1650 1650 1650 1750
Connection ~ 1650 1700
Wire Wire Line
	2300 2000 2450 2000
Wire Wire Line
	2450 2000 2450 2400
Wire Wire Line
	2450 2150 2550 2150
Wire Wire Line
	2150 2150 2050 2150
Connection ~ 2450 2150
Wire Wire Line
	2150 2400 2150 2150
Connection ~ 2150 2150
Wire Wire Line
	1650 1700 2150 1700
Wire Wire Line
	1650 2050 1650 2250
Wire Wire Line
	1750 2150 1650 2150
Connection ~ 1650 2150
$Comp
L GND #PWR27
U 1 1 5899AB51
P 1650 2700
F 0 "#PWR27" H 1650 2450 50  0001 C CNN
F 1 "GND" H 1650 2550 50  0000 C CNN
F 2 "" H 1650 2700 50  0000 C CNN
F 3 "" H 1650 2700 50  0000 C CNN
	1    1650 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2550 1650 2700
Text GLabel 1550 1150 0    60   Input ~ 0
PWR_EN
Wire Wire Line
	1550 1150 1650 1150
Wire Wire Line
	1650 1150 1650 1350
$EndSCHEMATC
