EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:drifter
LIBS:dips-s
LIBS:drifter-rescue
LIBS:drifter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Stokes Drifter"
Date "2017-02-15"
Rev "v2.2"
Comp "Florida State University"
Comment1 "John Easton"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PA45-1615-1575SA ANT1
U 1 1 589331AF
P 10050 3800
F 0 "ANT1" H 9800 3950 60  0000 C CNN
F 1 "PA45-1615-1575SA" H 10450 3650 60  0000 C CNN
F 2 "drifter:PA45-1615-1575SA" H 9550 3850 60  0001 C CNN
F 3 "http://www.mouser.com/ds/2/382/SLD_PA4516151575SA-204377.pdf" H 10050 3800 60  0001 C CNN
F 4 "Globalstar/GPS antenna" H 10050 3800 60  0001 C CNN "Function"
F 5 "PA45-1615-1575" H 10050 3800 60  0001 C CNN "part_num"
F 6 "API Technologies" H 10050 3800 60  0001 C CNN "MFG"
	1    10050 3800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 5893371C
P 7800 3700
F 0 "#PWR031" H 7800 3450 50  0001 C CNN
F 1 "GND" H 7800 3550 50  0000 C CNN
F 2 "" H 7800 3700 50  0000 C CNN
F 3 "" H 7800 3700 50  0000 C CNN
	1    7800 3700
	-1   0    0    -1  
$EndComp
Text HLabel 6400 3950 2    60   Input ~ 0
select_module
Text HLabel 8750 3950 0    60   Input ~ 0
select_antenna
Text Label 8750 3450 0    60   ~ 0
bottom_ant
Text Label 7300 3550 0    60   ~ 0
RF_com
$Comp
L STX3 MOD2
U 1 1 589B1910
P 5800 4950
F 0 "MOD2" H 5700 4300 60  0000 C CNN
F 1 "STX3" H 6200 4300 60  0000 C CNN
F 2 "lib:STX3" H 6900 5050 60  0001 C CNN
F 3 "" H 6900 5050 60  0000 C CNN
F 4 "Satellite modem" H 5800 4950 60  0001 C CNN "Function"
F 5 "Globalstar" H 5800 4950 60  0001 C CNN "MFG"
F 6 "STX3-Globalstar" H 5800 4950 60  0001 C CNN "part_num"
F 7 "Value" H 5800 4950 60  0001 C CNN "Buy"
F 8 "Value" H 5800 4950 60  0001 C CNN "Notes"
	1    5800 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR032
U 1 1 589B1927
P 4650 5250
F 0 "#PWR032" H 4650 5000 50  0001 C CNN
F 1 "GND" H 4650 5100 50  0000 C CNN
F 2 "" H 4650 5250 50  0000 C CNN
F 3 "" H 4650 5250 50  0000 C CNN
	1    4650 5250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR033
U 1 1 589B194B
P 6700 5600
F 0 "#PWR033" H 6700 5350 50  0001 C CNN
F 1 "GND" H 6700 5450 50  0000 C CNN
F 2 "" H 6700 5600 50  0000 C CNN
F 3 "" H 6700 5600 50  0000 C CNN
	1    6700 5600
	1    0    0    -1  
$EndComp
NoConn ~ 5350 5400
Text HLabel 2900 4900 0    60   Input ~ 0
STX3_transmit
Text HLabel 5200 4900 0    60   Input ~ 0
STX3_tx
Text HLabel 5200 5000 0    60   Input ~ 0
STX3_rx
Text HLabel 5200 4400 0    60   Input ~ 0
STX3_CTS
Text HLabel 5200 4500 0    60   Input ~ 0
STX3_RTS
Text Label 5000 5100 0    60   ~ 0
TEST2
Text Label 5000 5200 0    60   ~ 0
TEST1
$Comp
L RXM-GPS-RM MOD3
U 1 1 589B2565
P 5900 2150
F 0 "MOD3" H 5700 2050 60  0000 C CNN
F 1 "RXM-GPS-RM" H 5900 2150 60  0000 C CNN
F 2 "lib:RXM-GPS-RM" H 5900 2150 60  0001 C CNN
F 3 "https://www.linxtechnologies.com/wp/wp-content/uploads/rxm-gps-rm.pdf" H 5900 2150 60  0001 C CNN
F 4 "GPS module" H 5900 2150 60  0001 C CNN "Function"
F 5 "Linx" H 5900 2150 60  0001 C CNN "MFG"
F 6 "RXM-GPS-RM-T" H 5900 2150 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/linx-technologies-inc/RXM-GPS-RM-T/RXM-GPS-RM-TCT-ND/4475293" H 5900 2150 60  0001 C CNN "Buy"
F 8 "Value" H 5900 2150 60  0001 C CNN "Notes"
	1    5900 2150
	1    0    0    -1  
$EndComp
NoConn ~ 6850 2150
NoConn ~ 5250 1550
NoConn ~ 5250 1850
Text HLabel 5100 1650 0    60   Input ~ 0
GPS_tx
Text HLabel 5100 1750 0    60   Input ~ 0
GPS_rx
$Comp
L GND #PWR034
U 1 1 589B257D
P 6800 1300
F 0 "#PWR034" H 6800 1050 50  0001 C CNN
F 1 "GND" H 6800 1150 50  0000 C CNN
F 2 "" H 6800 1300 50  0000 C CNN
F 3 "" H 6800 1300 50  0000 C CNN
	1    6800 1300
	1    0    0    -1  
$EndComp
Text Label 7150 2050 0    60   ~ 0
GPS_pwr
$Comp
L C C18
U 1 1 589B258E
P 7550 2350
F 0 "C18" V 7600 2200 50  0000 L CNN
F 1 "4.7uF" V 7500 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7588 2200 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X5R_4V-to-50V_23.pdf" H 7550 2350 50  0001 C CNN
F 4 "GPS decoupling" V 7550 2350 60  0001 C CNN "Function"
F 5 "Yageo" V 7550 2350 60  0001 C CNN "MFG"
F 6 "CC0603KRX5R5BB475" V 7550 2350 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX5R5BB475/311-1521-1-ND/2833827" V 7550 2350 60  0001 C CNN "Buy"
	1    7550 2350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR035
U 1 1 589B259D
P 7650 2600
F 0 "#PWR035" H 7650 2350 50  0001 C CNN
F 1 "GND" H 7650 2450 50  0000 C CNN
F 2 "" H 7650 2600 50  0000 C CNN
F 3 "" H 7650 2600 50  0000 C CNN
	1    7650 2600
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR036
U 1 1 589B25AF
P 7050 1500
F 0 "#PWR036" H 7050 1350 50  0001 C CNN
F 1 "VCC" H 7050 1650 50  0000 C CNN
F 2 "" H 7050 1500 50  0000 C CNN
F 3 "" H 7050 1500 50  0000 C CNN
	1    7050 1500
	1    0    0    -1  
$EndComp
Text GLabel 7850 1800 2    60   Input ~ 0
PWR_EN
Text Label 5250 3150 0    60   ~ 0
GPS_ant
Text Label 5350 3450 0    60   ~ 0
STX3_ant
$Comp
L L_Small L3
U 1 1 58A21000
P 5200 3650
F 0 "L3" H 5230 3690 50  0000 L CNN
F 1 "39nH" H 5230 3610 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 5200 3650 50  0001 C CNN
F 3 "http://search.murata.co.jp/Ceramy/image/img/PDF/ENG/L0110S0101BLM18H.pdf" H 5200 3650 50  0001 C CNN
F 4 "ESD shunt" H 5200 3650 60  0001 C CNN "Function"
F 5 "Murata" H 5200 3650 60  0001 C CNN "MFG"
F 6 "BLM18HG471SN1D" H 5200 3650 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/murata-electronics-north-america/BLM18HG471SN1D/490-1032-1-ND/584480" H 5200 3650 60  0001 C CNN "Buy"
	1    5200 3650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR037
U 1 1 58A21006
P 5200 3850
F 0 "#PWR037" H 5200 3600 50  0001 C CNN
F 1 "GND" H 5200 3700 50  0000 C CNN
F 2 "" H 5200 3850 50  0000 C CNN
F 3 "" H 5200 3850 50  0000 C CNN
	1    5200 3850
	1    0    0    -1  
$EndComp
$Comp
L PE4259 SW1
U 1 1 58A2274A
P 6650 3550
F 0 "SW1" H 6700 3750 60  0000 C CNN
F 1 "PE4259" H 6850 3350 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6" H 6850 3250 60  0001 C CNN
F 3 "http://www.psemi.com/pdf/datasheets/pe4259ds.pdf" H 6850 3850 60  0001 C CNN
F 4 "RF switch" H 6650 3550 60  0001 C CNN "Function"
F 5 "Peregrine Semiconductor" H 6650 3550 60  0001 C CNN "MFG"
F 6 "\\4259-63" H 6650 3550 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/peregrine-semiconductor/4259-63/1046-1011-1-ND/2614515" H 6650 3550 60  0001 C CNN "Buy"
F 8 "Recommend 1.6k resistors on ctrl/vdd lines to limit current" H 6900 3950 60  0001 C CNN "Notes"
	1    6650 3550
	1    0    0    -1  
$EndComp
Text Label 8750 3550 0    60   ~ 0
top_ant
Text GLabel 7450 3100 2    60   Input ~ 0
PWR_EN
$Comp
L C_Small C27
U 1 1 58A2552D
P 4950 3450
F 0 "C27" V 5100 3450 50  0000 L CNN
F 1 "39pF" V 4800 3300 50  0000 L CNN
F 2 "Resistors_SMD:R_0402" H 4950 3450 50  0001 C CNN
F 3 "http://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM155R71A104KA01-01.pdf" H 4950 3450 50  0001 C CNN
F 4 "STX3 ESD protection" V 4950 3450 60  0001 C CNN "Function"
F 5 "Murata" V 4950 3450 60  0001 C CNN "MFG"
F 6 "GRM155R71A104KA01D" V 4950 3450 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM155R71A104KA01D/490-6321-1-ND/3845518" V 4950 3450 60  0001 C CNN "Buy"
	1    4950 3450
	0    1    1    0   
$EndComp
$Comp
L GND #PWR038
U 1 1 589336FE
P 7300 3700
F 0 "#PWR038" H 7300 3450 50  0001 C CNN
F 1 "GND" H 7300 3550 50  0000 C CNN
F 2 "" H 7300 3700 50  0000 C CNN
F 3 "" H 7300 3700 50  0000 C CNN
	1    7300 3700
	-1   0    0    -1  
$EndComp
Text Label 7400 3450 0    60   ~ 0
sw_pwr
Text Label 8750 3650 0    60   ~ 0
ctl_sw2
Text Label 6350 3650 2    60   ~ 0
ctl_sw1
Text Label 4650 4150 1    60   ~ 0
stx3_rfout
$Comp
L Q_PMOS_GSD Q3
U 1 1 58A4A5F3
P 3650 4550
F 0 "Q3" V 3850 4700 50  0000 L CNN
F 1 "Q_PMOS_GSD" V 3550 4750 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3850 4650 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds31861.pdf" H 3650 4550 50  0001 C CNN
F 4 "Diodes Inc." V 3650 4550 60  0001 C CNN "MFG"
F 5 "DMG1013UW" V 3650 4550 60  0001 C CNN "part_num"
F 6 "http://www.digikey.com/product-detail/en/diodes-incorporated/DMG1013UW-7/DMG1013UW-7DICT-ND/2183252" V 3650 4550 60  0001 C CNN "Buy"
F 7 "Power STX3 RF part" V 3650 4550 60  0001 C CNN "Function"
	1    3650 4550
	0    1    -1   0   
$EndComp
Wire Wire Line
	7800 3700 7800 3650
Wire Wire Line
	7800 3650 7850 3650
Wire Wire Line
	4950 5100 5350 5100
Wire Wire Line
	5350 5200 4950 5200
Wire Wire Line
	4650 5100 4650 5250
Connection ~ 4650 5200
Wire Wire Line
	6600 4400 6700 4400
Wire Wire Line
	6700 4400 6700 5600
Wire Wire Line
	6600 5500 6700 5500
Connection ~ 6700 5500
Wire Wire Line
	6600 5400 6700 5400
Connection ~ 6700 5400
Wire Wire Line
	6600 5300 6700 5300
Connection ~ 6700 5300
Wire Wire Line
	6600 5200 6700 5200
Connection ~ 6700 5200
Wire Wire Line
	6600 5100 6700 5100
Connection ~ 6700 5100
Wire Wire Line
	6600 5000 6700 5000
Connection ~ 6700 5000
Wire Wire Line
	6600 4900 6700 4900
Connection ~ 6700 4900
Wire Wire Line
	6600 4800 6700 4800
Connection ~ 6700 4800
Wire Wire Line
	6600 4700 6700 4700
Connection ~ 6700 4700
Wire Wire Line
	6600 4600 6700 4600
Connection ~ 6700 4600
Wire Wire Line
	6600 4500 6700 4500
Connection ~ 6700 4500
Wire Wire Line
	5200 5000 5350 5000
Wire Wire Line
	5200 4900 5350 4900
Wire Wire Line
	2750 5500 5350 5500
Wire Wire Line
	5200 4500 5350 4500
Wire Wire Line
	5200 4400 5350 4400
Wire Wire Line
	4650 4700 5350 4700
Wire Wire Line
	4500 4600 5350 4600
Wire Wire Line
	6850 2050 7800 2050
Wire Wire Line
	5100 1750 5250 1750
Wire Wire Line
	5100 1650 5250 1650
Wire Wire Line
	6850 1950 7050 1950
Wire Wire Line
	6850 1550 6900 1550
Wire Wire Line
	6900 1250 6900 1850
Connection ~ 6900 1850
Wire Wire Line
	6850 1750 6900 1750
Connection ~ 6900 1750
Wire Wire Line
	6850 1650 6900 1650
Connection ~ 6900 1650
Wire Wire Line
	7550 2500 7550 2550
Wire Wire Line
	7550 2550 7800 2550
Wire Wire Line
	7800 2550 7800 2450
Wire Wire Line
	7650 2600 7650 2550
Connection ~ 7650 2550
Wire Wire Line
	7550 2050 7550 2200
Connection ~ 7550 2050
Wire Wire Line
	6900 1850 6850 1850
Wire Wire Line
	6900 1250 6800 1250
Wire Wire Line
	6800 1250 6800 1300
Connection ~ 6900 1550
Wire Wire Line
	7050 1950 7050 1500
Connection ~ 7800 2050
Wire Wire Line
	7850 1800 7800 1800
Wire Wire Line
	7800 1800 7800 2250
Wire Wire Line
	5250 1950 5150 1950
Wire Wire Line
	4650 3450 4650 4700
Wire Wire Line
	7850 3550 7250 3550
Wire Wire Line
	5200 3750 5200 3850
Wire Wire Line
	7300 3700 7300 3650
Wire Wire Line
	7300 3650 7250 3650
Wire Wire Line
	8700 3550 9500 3550
Wire Wire Line
	9500 3550 9500 3800
Wire Wire Line
	9500 3800 9700 3800
Wire Wire Line
	9700 3300 9500 3300
Wire Wire Line
	9500 3300 9500 3450
Wire Wire Line
	9500 3450 8700 3450
Wire Wire Line
	7250 3450 7850 3450
Wire Wire Line
	8750 3950 8950 3950
Wire Wire Line
	8700 3650 8950 3650
Wire Wire Line
	6200 3950 6400 3950
Wire Wire Line
	6200 3650 6400 3650
Wire Wire Line
	7450 3100 7300 3100
Wire Wire Line
	7300 3400 7300 3450
Connection ~ 7300 3450
Wire Wire Line
	4650 3450 4850 3450
Wire Wire Line
	5050 3450 6400 3450
Wire Wire Line
	5200 3550 5200 3450
Connection ~ 5200 3450
Wire Wire Line
	6400 3550 6100 3550
Wire Wire Line
	6100 3550 6100 3150
Wire Wire Line
	6100 3150 5150 3150
Wire Wire Line
	5150 3150 5150 1950
Wire Wire Line
	3500 5500 3500 5600
Wire Wire Line
	2750 5300 2750 5500
Wire Wire Line
	2800 5300 2750 5300
Text GLabel 2800 5300 2    60   Input ~ 0
PWR_EN
Connection ~ 3350 5950
Wire Wire Line
	3350 6000 3350 5950
Wire Wire Line
	3500 5950 3500 5900
Wire Wire Line
	3200 5950 3500 5950
Wire Wire Line
	3200 5850 3200 5950
$Comp
L GND #PWR039
U 1 1 589B1978
P 3350 6000
F 0 "#PWR039" H 3350 5750 50  0001 C CNN
F 1 "GND" H 3350 5850 50  0000 C CNN
F 2 "" H 3350 6000 50  0000 C CNN
F 3 "" H 3350 6000 50  0000 C CNN
	1    3350 6000
	1    0    0    -1  
$EndComp
Connection ~ 3500 5500
Connection ~ 3200 5500
Wire Wire Line
	3200 5500 3200 5650
Text Label 2950 5500 0    60   ~ 0
STX3_pwr
Text GLabel 2900 4450 0    60   Input ~ 0
PWR_EN
Wire Wire Line
	2900 4450 3450 4450
Wire Wire Line
	3850 4450 4500 4450
Wire Wire Line
	4500 4450 4500 4600
Wire Wire Line
	3500 4150 3350 4150
Wire Wire Line
	3350 4150 3350 4450
Connection ~ 3350 4450
Wire Wire Line
	3800 4150 4000 4150
Wire Wire Line
	4000 4150 4000 4450
Connection ~ 4000 4450
Wire Wire Line
	5350 4800 3650 4800
Wire Wire Line
	3650 4750 3650 4900
Wire Wire Line
	3650 4900 2900 4900
Connection ~ 3650 4800
Wire Wire Line
	3200 4800 3200 4900
Connection ~ 3200 4900
Wire Wire Line
	3200 4500 3200 4450
Connection ~ 3200 4450
$Comp
L PE4259 SW2
U 1 1 58A5AB10
P 8450 3550
F 0 "SW2" H 8500 3750 60  0000 C CNN
F 1 "PE4259" H 8650 3350 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6" H 8650 3250 60  0001 C CNN
F 3 "http://www.psemi.com/pdf/datasheets/pe4259ds.pdf" H 8650 3850 60  0001 C CNN
F 4 "RF switch" H 8450 3550 60  0001 C CNN "Function"
F 5 "Peregrine Semiconductor" H 8450 3550 60  0001 C CNN "MFG"
F 6 "4259-63" H 8450 3550 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/peregrine-semiconductor/4259-63/1046-1011-1-ND/2614515" H 8450 3550 60  0001 C CNN "Buy"
F 8 "Recommend 1.6k resistors on ctrl/vdd lines to limit current" H 8700 3950 60  0001 C CNN "Notes"
	1    8450 3550
	-1   0    0    -1  
$EndComp
$Comp
L PA45-1615-1575SA ANT2
U 1 1 58A5B161
P 10050 3300
F 0 "ANT2" H 9800 3450 60  0000 C CNN
F 1 "PA45-1615-1575SA" H 10450 3150 60  0000 C CNN
F 2 "drifter:PA45-1615-1575SA" H 9550 3350 60  0001 C CNN
F 3 "http://www.mouser.com/ds/2/382/SLD_PA4516151575SA-204377.pdf" H 10050 3300 60  0001 C CNN
F 4 "Globalstar/GPS antenna" H 10050 3300 60  0001 C CNN "Function"
F 5 "PA45-1615-1575" H 10050 3300 60  0001 C CNN "part_num"
F 6 "API Technologies" H 10050 3300 60  0001 C CNN "MFG"
	1    10050 3300
	1    0    0    -1  
$EndComp
$Comp
L C_Small C19
U 1 1 58A64BC7
P 3200 5750
F 0 "C19" H 3210 5820 50  0000 L CNN
F 1 "100nF" H 3210 5670 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3200 5750 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 3200 5750 50  0001 C CNN
F 4 "STX3 decoupling" H 3200 5750 60  0001 C CNN "Function"
F 5 "Yageo" H 3200 5750 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 3200 5750 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 3200 5750 60  0001 C CNN "Buy"
	1    3200 5750
	1    0    0    -1  
$EndComp
$Comp
L C_Small C24
U 1 1 58A665EA
P 7800 2350
F 0 "C24" H 7810 2420 50  0000 L CNN
F 1 "100nF" H 7810 2270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7800 2350 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_16.pdf" H 7800 2350 50  0001 C CNN
F 4 "GPS decoupling" H 7800 2350 60  0001 C CNN "Function"
F 5 "Yageo" H 7800 2350 60  0001 C CNN "MFG"
F 6 "CC0603KRX7R7BB104" H 7800 2350 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX7R7BB104/311-1088-1-ND/302998" H 7800 2350 60  0001 C CNN "Buy"
	1    7800 2350
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 58A6C6F6
P 3500 5750
F 0 "C20" H 3550 5850 50  0000 L CNN
F 1 "4.7uF" H 3550 5650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3538 5600 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X5R_4V-to-50V_23.pdf" H 3500 5750 50  0001 C CNN
F 4 "STX3 decoupling" V 3500 5750 60  0001 C CNN "Function"
F 5 "Yageo" V 3500 5750 60  0001 C CNN "MFG"
F 6 "CC0603KRX5R5BB475" V 3500 5750 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/CC0603KRX5R5BB475/311-1521-1-ND/2833827" V 3500 5750 60  0001 C CNN "Buy"
	1    3500 5750
	1    0    0    -1  
$EndComp
$Comp
L R J4
U 1 1 58A73144
P 4800 5100
F 0 "J4" V 4750 4950 50  0000 C CNN
F 1 "J" V 4800 5100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4730 5100 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 4800 5100 50  0001 C CNN
F 4 "STX3_test2" V 4800 5100 60  0001 C CNN "Function"
F 5 "Yageo" V 4800 5100 60  0001 C CNN "MFG"
F 6 "RC0603JR-070RL" V 4800 5100 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" V 4800 5100 60  0001 C CNN "Buy"
	1    4800 5100
	0    1    1    0   
$EndComp
$Comp
L R J5
U 1 1 58A733FC
P 4800 5200
F 0 "J5" V 4850 5350 50  0000 C CNN
F 1 "J" V 4800 5200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4730 5200 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 4800 5200 50  0001 C CNN
F 4 "STX3_test1" V 4800 5200 60  0001 C CNN "Function"
F 5 "Yageo" V 4800 5200 60  0001 C CNN "MFG"
F 6 "RC0603JR-070RL" V 4800 5200 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" V 4800 5200 60  0001 C CNN "Buy"
	1    4800 5200
	0    1    1    0   
$EndComp
$Comp
L R J7
U 1 1 58A748E0
P 3650 4150
F 0 "J7" V 3600 4000 50  0000 C CNN
F 1 "J" V 3650 4150 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3580 4150 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3650 4150 50  0001 C CNN
F 4 "Bypass STX3 VRF" V 3650 4150 60  0001 C CNN "Function"
F 5 "Yageo" V 3650 4150 60  0001 C CNN "MFG"
F 6 "RC0603JR-070RL" V 3650 4150 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRCT-ND/729622" V 3650 4150 60  0001 C CNN "Buy"
F 8 "DNP" V 3650 4150 60  0001 C CNN "Notes"
	1    3650 4150
	0    1    1    0   
$EndComp
$Comp
L R R27
U 1 1 58A7B3CA
P 3200 4650
F 0 "R27" H 3100 4500 50  0000 C CNN
F 1 "4.7k" V 3200 4650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3130 4650 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 3200 4650 50  0001 C CNN
F 4 "PMOS pullup" V 3200 4650 60  0001 C CNN "Function"
F 5 "Yageo" V 3200 4650 60  0001 C CNN "MFG"
F 6 "RC0603JR-074K7L" V 3200 4650 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603JR-074K7L/311-4.7KGRCT-ND/729732" V 3200 4650 60  0001 C CNN "Buy"
	1    3200 4650
	-1   0    0    1   
$EndComp
$Comp
L R R22
U 1 1 58A7E21B
P 6200 3800
F 0 "R22" V 6300 3800 50  0000 C CNN
F 1 "1k" V 6200 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6130 3800 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 6200 3800 50  0001 C CNN
F 4 "Limit RF switch current" V 6200 3800 60  0001 C CNN "Function"
F 5 "Yageo" V 6200 3800 60  0001 C CNN "MFG"
F 6 "RC0603FR-071KL" V 6200 3800 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-071KL/311-1.00KHRCT-ND/729790" V 6200 3800 60  0001 C CNN "Buy"
F 8 "Value" H 6200 3800 60  0001 C CNN "Notes"
	1    6200 3800
	-1   0    0    1   
$EndComp
$Comp
L R R24
U 1 1 58A7E53D
P 8950 3800
F 0 "R24" V 9050 3800 50  0000 C CNN
F 1 "1k" V 8950 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 8880 3800 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 8950 3800 50  0001 C CNN
F 4 "Limit RF switch current" V 8950 3800 60  0001 C CNN "Function"
F 5 "Yageo" V 8950 3800 60  0001 C CNN "MFG"
F 6 "RC0603FR-071KL" V 8950 3800 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-071KL/311-1.00KHRCT-ND/729790" V 8950 3800 60  0001 C CNN "Buy"
F 8 "Value" H 8950 3800 60  0001 C CNN "Notes"
	1    8950 3800
	-1   0    0    1   
$EndComp
$Comp
L R R23
U 1 1 58A7E73B
P 7300 3250
F 0 "R23" V 7400 3250 50  0000 C CNN
F 1 "1k" V 7300 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7230 3250 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/R-Chip/PYu-RC_Group_51_RoHS_L_4.pdf" H 7300 3250 50  0001 C CNN
F 4 "Limit RF switch current" V 7300 3250 60  0001 C CNN "Function"
F 5 "Yageo" V 7300 3250 60  0001 C CNN "MFG"
F 6 "RC0603FR-071KL" V 7300 3250 60  0001 C CNN "part_num"
F 7 "http://www.digikey.com/product-detail/en/yageo/RC0603FR-071KL/311-1.00KHRCT-ND/729790" V 7300 3250 60  0001 C CNN "Buy"
F 8 "Value" H 7300 3250 60  0001 C CNN "Notes"
	1    7300 3250
	-1   0    0    1   
$EndComp
$EndSCHEMATC
