#!/usr/bin/perl

use strict;
use warnings;

my $filename="drifter.xml";
open( my $fh, '<', $filename ) or die "Can't open $filename: $!";
while ( my $line = <$fh> ) {
		if ($line =~ s/<fields>//) {
		} elsif ($line =~ s/<\/fields>//){
		} elsif ($line =~ s/<field name="(.*)">(.*)<\/field>/<$1>$2<\/$1>/) {
			print $line;
		} else {
			print $line;
		}
}
close $fh;
