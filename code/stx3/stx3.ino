#define TRUE 1
#define FALSE 0

//Pin assignments
const uint8_t  RTS = 2;
const uint8_t  CTS = 3;
const uint8_t  PWR_EN = 14;

  //length of message
  #define ARR 5
  //const uint8_t a[ARR] = {0xAA, 0x05, 0xAA, 0xAA, 0xAA};  //Nonsense Tx
  //const uint8_t a[ARR] = {0xAA, 0x05, 0x01, 0x50, 0xD5};  //Query ESN
  //const uint8_t a[ARR] = {0xAA, 0x05, 0x03, 0x42, 0xF6};  //Abort transission
  //const uint8_t a[ARR] = {0xAA, 0x05, 0x04, 0xFD, 0x82};  //Query bursts remaining
  //const uint8_t a[ARR] = {0xAA, 0x05, 0x05, 0x74, 0x93};  //Query Firmware version
  //const uint8_t a[ARR] = {0xAA, 0x05, 0x07, 0x66, 0xB0};  //Query setup
  //const uint8_t a[ARR] = {0xAA, 0x05, 0x09, 0x18, 0x59};  //Query hardware version
  uint8_t send_data[16] = {0xAA, 16, 0x00, 'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', 0xFF, 0xFF};  //Example
  
  //STINGR commands
  const uint8_t a[5] = {0xAA, 0x05, 0x25, 0x76, 0xB2};  //Query location
  const uint8_t GPS_on[6] = {0xAA, 0x06, 0xFD, 0x21, 0x6F, 0xA8}; //Turn on GPS
  const uint8_t GPS_off[6] = {0xAA, 0x06, 0xFD, 0x22, 0xF4, 0x9A}; //Turn off GPS
  const uint8_t GPS_pass[6] = {0xAA, 0x06, 0xFD, 0x00, 0xE4, 0x98}; //Turn on GPS passthrough
  const uint8_t GPS_nopass[6] = {0xAA, 0x06, 0xFD, 0x20, 0xE6, 0xB9}; //Turn off GPS passthrough
  const uint8_t self_test[6] = {0xAA, 0x06, 0xFD, 0x0E, 0x9A, 0x71}; //STINGR self test
  const uint8_t query_location[5] = {0xAA 0x05 0x25 0x76 0xB2 };  //Query GPS location


  uint8_t ptrack[9] = {0xAA, 0x30, 0xFF, 0xFF, 0x01, 0x02, 0x03, 0xFF, 0xFF};  //proprietary track
  uint8_t update_track[8] = {0xAA, 0x08, 0x31, 0x01, 0x02, 0x03, 0xFF, 0xFF};  //update proprietary track

unsigned long prevmillis = 0;
unsigned long interval = 1000;

void setup(){
  pinMode(RTS, OUTPUT);
  pinMode(CTS, INPUT);
  pinMode(PWR_EN, INPUT_PULLUP);
  Serial.begin(9600);

  transmit("STINGR-pvc");
}

void loop() {
  uint8_t b;
  while(Serial.available()){
    b = Serial.read();
    Serial.print(b, HEX);
    Serial.print(" ");
  }
}




//transmit a message
int transmit(const char *str){
  const int max_length = 9*8*5;   //arbitrary maximum message length in bits
  uint8_t command[max_length] = {};
  int length = 0;
  for (int i = 0; i < max_length; i++){
    if (i > max_length)
      return -1;                  // error if message is too long
    if (str[i] == '\0')           //If end of string is reached, exit loop
      break;
    command[i+3] = str[i];
    length = i;  
  }
  command[0] = 0xAA;
  command[1] = length;
  command[2] = 0x00;
  write_data(command, length);
  return TRUE;
}


int track(uint16_t interval, uint8_t *data){
  ptrack[2] = (uint8_t)(interval >> 8); //grap top half of number
  ptrack[3] = interval; //grap lower half of number
  ptrack[4] = data[0];
  ptrack[5] = data[1];
  ptrack[6] = data[2];
  write_data(ptrack, 9);
  return TRUE;
}

int up_track(uint8_t *data){
  update_track[4] = data[0];
  update_track[5] = data[1];
  update_track[6] = data[2];
  write_data(update_track, 8);
  return TRUE;
}


int write_data(uint8_t *p_data, int length){
  crc16(p_data, length);
  if (wait2send() == FALSE)
    return FALSE;
  Serial.write(p_data, length);
  digitalWrite(RTS, HIGH);
  return TRUE;
}

int wait2send(){ //wait until the module is ready to receive a message
  //TODO: timeout if its take too long
  digitalWrite(RTS, LOW);
  volatile int z=0;
  while(digitalRead(CTS)==HIGH){ //loop until CTS goes low
    z++;  //iterate volatile int to prevent optimization
  }
  return 1;
}

// Calculate the two byte crc and insert it at the end of the array
int crc16(uint8_t *pData, int length) {
  length = length - 2;
  int i;
  uint16_t data, crc;
  crc = 0xFFFF;
  if (length == 0) 
    return 0; 
  do 
  { 
    data = (unsigned int)0x00FF & *pData++; 
    crc = crc ^ data; 
    for (i = 8; i > 0; i--) 
    { 
     if (crc & 0x0001) 
       crc = (crc >> 1) ^ 0x8408; 
      else 
       crc >>= 1; 
     } 
  }while (--length); 
  crc = ~crc; 
  //return (crc);

  //place CRC in array
  pData[length] = crc;
  pData[length+1] = (uint8_t)(crc >> 8);
  return 1;
}
