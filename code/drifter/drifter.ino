#include <TinyGPS++.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#define T1 0.0
#define T2 60.0
#define V1 2.633
#define V2 1.819
#define TEMPERATURE_POWER A3
#define TEMPERATURE_SENSOR A2
#define TEMPERATURE_GND A1
#define VREF 3.3

#define LAT_LENGTH 8
#define LON_LENGTH 8
#define TIME_LENGTH 6

static const int RXPin = 0, TXPin = 1;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

typedef struct {
	double latitude;
	char NS; // North/South indicator
	double longitude;
	char EW; // East/West indicator
	uint32_t time;
	uint32_t date;
	char orientation;
	float temperature;
	int satellites;
} rawdata_struct;

int sleep_counter = 0;
int sleep_threshold = 30;

int go_to_sleep();
int init_sleep();
float read_temperature();

ISR(TIMER2_OVF_vect){ //TIMER2 overflows
	//TIMER2 overflow interupt. 
	sleep_counter++;
	if(sleep_counter < sleep_threshold){
		go_to_sleep();
	} else {
		PORTB ^= _BV(PB5);
		sleep_counter = 0;
		go_to_sleep();
	}
}


void setup()
{
  init_sleep();
  pinMode(13, OUTPUT);
  pinMode(A0, INPUT);
  digitalWrite(13, LOW);
  attachInterrupt(digitalPinToInterrupt(2), dummy, CHANGE);
  attachInterrupt(digitalPinToInterrupt(3), dummy, CHANGE);
  interrupts();
  go_to_sleep();
  Serial.begin(9600);
  ss.begin(GPSBaud);
  Serial.println("Hello World!");
}

void loop()
{
  rawdata_struct rawdata;
  (void)rawdata; //disable compiler warning

  rawdata.temperature = read_temperature();
  Serial.println();
  Serial.print("Temperature: ");
  Serial.print(rawdata.temperature);
  Serial.print("C");
  Serial.println();


  if(gps.location.isValid()){
  	rawdata.latitude = gps.location.lat();
	rawdata.longitude = gps.location.lng();
  } else {
  	rawdata.latitude = 0;
	rawdata.longitude = 0;
  }
  if(gps.time.isValid())
    rawdata.time = gps.time.value();
	else
	rawdata.time = 0;
  if(gps.date.isValid())
	rawdata.date = gps.date.value();
	else
	rawdata.date = 0;
  if (gps.satellites.isValid())
    rawdata.satellites =  gps.satellites.value();
	else
    rawdata.satellites =  0;

  Serial.print("Date: ");
  Serial.println(rawdata.date);
  Serial.print("Time: ");
  Serial.println(rawdata.time);
  Serial.print("Satellites: ");
  Serial.println(rawdata.satellites);
  Serial.print("Latitude: ");
  Serial.println(rawdata.latitude);
  Serial.print("Longitude: ");
  Serial.println(rawdata.longitude);

  smartDelay(1000);

  if (millis() > 5000 && gps.charsProcessed() < 10)
    Serial.println(F("No GPS data received: check wiring"));

  //go_to_sleep();
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

static void printFloat(float val, bool valid, int len, int prec)
{
  if (!valid)
  {
    while (len-- > 1)
      Serial.print('*');
    Serial.print(' ');
  }
  else
  {
    Serial.print(val, prec);
    int vi = abs((int)val);
    int flen = prec + (val < 0.0 ? 2 : 1); // . and -
    flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
    for (int i=flen; i<len; ++i)
      Serial.print(' ');
  }
  smartDelay(0);
}

static void printInt(unsigned long val, bool valid, int len)
{
  char sz[32] = "*****************";
  if (valid)
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i=strlen(sz); i<len; ++i)
    sz[i] = ' ';
  if (len > 0) 
    sz[len-1] = ' ';
  Serial.print(sz);
  smartDelay(0);
}

static void printDateTime(TinyGPSDate &d, TinyGPSTime &t)
{
  if (!d.isValid())
  {
    Serial.print(F("********** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d ", d.month(), d.day(), d.year());
    Serial.print(sz);
  }
  
  if (!t.isValid())
  {
    Serial.print(F("******** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d:%02d:%02d ", t.hour(), t.minute(), t.second());
    Serial.print(sz);
  }

  printInt(d.age(), d.isValid(), 5);
  smartDelay(0);
}

static void printStr(const char *str, int len)
{
  int slen = strlen(str);
  for (int i=0; i<len; ++i)
    Serial.print(i<slen ? str[i] : ' ');
  smartDelay(0);
}

float read_temperature(){
	pinMode(TEMPERATURE_POWER, OUTPUT);
	pinMode(TEMPERATURE_SENSOR, INPUT);
	pinMode(TEMPERATURE_GND, OUTPUT);
	digitalWrite(TEMPERATURE_POWER, HIGH);
	digitalWrite(TEMPERATURE_GND, LOW);
    int analog_value = analogRead(TEMPERATURE_SENSOR);
    float voltage = analog_value * (VREF / 1024.0); //calculate voltage
    /* The output of the temperature sensor can be approximated
    * linearly over a range of temperatures T1 to T2:
    *
    * V-V1 = (V2-V1)/(T2-T1) * (T-T1)
    *
    * V = (V2-V1)/(T2-T1) * (T-T1) + V1
    *
    * T = (V-V1) * (T2-T1)/(V2-V1) + T1
    *
    * Beware integer arithmatic
    */
    float temperature = ((voltage-V1) * ((T2-T1)/(V2-V1))) + T1;
    return temperature;
}

/* See http://www.nongnu.org/avr-libc/user-manual/group__avr__sleep.html
 * and page 39 of datasheet */
/* Valid sleep modes, as defined in sleep.h:
 *   SLEEP_MODE_IDLE
 *  SLEEP_MODE_ADC
 *  SLEEP_MODE_PWR_DOWN
 *  SLEEP_MODE_PWR_SAVE
 *  SLEEP_MODE_STANDBY
 *  SLEEP_MODE_EXT_STANDBY
 */
int go_to_sleep(){
    cli();                  //disable interupts
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();         //set SE (sleep enable) bit
    sleep_bod_disable();    //disable brownout detection
    sei();                  //enable interupts
    sleep_cpu();            //enter sleep
    sleep_disable();        //clear SE bit
    sei();
    return 1;
}

int init_sleep(){
    cli();
	// Timer2 should be used to count time spent sleeping.
	// SLEEP_PWR_DOWN requires an external clock, for exmple a crystal 
	// oscillator. The watchdog timer (WDT )could also be used, but the
	// WDT is not at all accurate. Accuracy is important if the drifter is
	// to sleep 12 hours a day
    sei();
    return 1;
}

void dummy(){
	digitalWrite(13, HIGH);
};

