#include "mcu.h"

//Supply power to the external parts that are normally off
void activate_modules(){
	DDRE  |= _BV(PE2);
	PORTE |= _BV(PE2);
}

//Cut off power to external parts
void deactivate_modules(){
	DDRE  |= _BV(PE2);
	PORTE &= ~_BV(PE2);
}

//Initialize the hall effect switch generate an interrupt.
//When PE3 changes, ISR(PCINT3_vect) will execute
void Hall_switch_init(){
	PCMSK3 |= _BV(PCINT25); //Enable pinchange interrupt on PE3
	PCICR  |= _BV(PCIE3);   //Enable pinchange interrupt on PortE
}

//WDTON fuse must be programmed to use watchdog timer
void WDT_init(){
	//Configure for 8 second timeout with interrupt (no reset)
	cli();
	MCUSR = 0;
	WDTCSR=0xB9;
	WDTCSR=0xE1;
	sei();
}

//Use the watchdog timer to initiate a software reset
void soft_reset(){
	cli();
	MCUSR = 0;
	WDTCSR= 0x98;  //Enable WDT changes
	WDTCSR= 0x88;  //Enable WDT reset with 16ms timeout
	while(1);      //loop until reset
	sei();
}

//Sleep for approximately the specified number of seconds
volatile uint32_t sleep_counter;
void sleep_time(uint32_t seconds){
	//Sleep counter in incremented in ISR(WDT_vect)
	while(sleep_counter < seconds){
		//Sleep until seconds have passed
		sleep_enable();
		sleep_bod_disable();
		sei();
		sleep_cpu();
		sleep_disable();
	}
	sleep_counter = 0;
}

//Covert an ADC sample of the battery voltage to an actual voltage
//Returns millivolts
int16_t batt2voltage(int16_t sample, char bits){
	int16_t voltage = (sample/pow(2,bits)) * 5.420 * (1100L);
	return voltage;
}

int16_t adc2temperature(int16_t sample){
	 //~314mv at 25C, 1mV per 1C
	int16_t temperature = (sample*1e3)/1024 * 1.1 - 289;
	return temperature;
}

//Perform a self test
char self_test(){

	activate_modules();
	_delay_ms(250); //Wait a bit for voltage to stablize

	//Test battery level
	int16_t voltage = sample_probe(BATT);
	voltage = batt2voltage(voltage, 12);
	//Enter fail state if battery is too low
	if (voltage < BATT_THRESHOLD_HIGH)
		fail();

	 //Test for comms with accelerometer
	twi0_init();
	while(!accel_ready()); 

	//Test for comms with GPS
	uart_init0();
	select_module(GPS);
	char GPS_message[128];
	do{
		while(!get_GPS_message(RMC, GPS_message));
	} while(!validate_message(GPS_message));

	//Test for comms with STX3 and send test message
	uart_init1();
	select_module(STX3);
	char STX3_response[5];
	do{
		transmit_data("Self Test", 9);
		get_response(STX3_response);
	} while (!valid_response(STX3_response));

	return 1;
}

//Enter the fail state
void fail(){
	while(1){
		buzz_fail();
		sleep_time(8);
	}
}

//Set pins and regisiters to minimize power use
void reduce_power(){
	PRR0  = 0xFF;  //Stop peripherals 
	DIDR0 = 0xFF;  //Disable ADC[7:0] to reduce power consumption
	DIDR1 |= 0x03; //Disable AC[1:0] to reduce power consumption
	ACSR |= _BV(ACD);

	//Set inputs
	DDRB &= ~_BV(PB5);
	DDRE &= ~_BV(PE1);
	//Set pullups
	PORTE |= _BV(PE1);
	PORTB |= _BV(PB5);

	//Set outputs
	DDRB |= _BV(PB0) | _BV(PB1) | _BV(PB2) | _BV(PB3) | _BV(PB4) | _BV(PB5);
	DDRC |= _BV(PC0) | _BV(PC1) | _BV(PC2) | _BV(PC3) | _BV(PC4) | _BV(PC5) | _BV(PC6);
	DDRD |= _BV(PD0) | _BV(PD1) | _BV(PD2) | _BV(PD3) | _BV(PD4) | _BV(PD5) | _BV(PD6) | _BV(PD7);
	DDRE |= _BV(PE0) | _BV(PE2) | _BV(PE3);

	//Set output low
	PORTB &= ~(_BV(PB1) | _BV(PB2) | _BV(PB3) | _BV(PB4) | _BV(PB5));//STX3 lines
	PORTC &= ~(_BV(PC0) | _BV(PC1) | _BV(PC2) | _BV(PC3)); //Analog sensor lines
	PORTC &= ~(_BV(PC4) | _BV(PC5)); //SDA and SCL
	PORTD &= ~(_BV(PD0) | _BV(PD1)); //GPS comm lines
	PORTD &= ~(_BV(PD2) | _BV(PD3) | _BV(PD4)); //accel_int, an LED, and noise_gen
	PORTD &= ~(_BV(PD5) | _BV(PD6)); //The RF switches
	PORTE &= ~(_BV(PE0) | _BV(PE3)); //Turn off LED and buzzer
	//Set output high 
	PORTB |= _BV(PB0);               //Buzzer switch
	PORTE |= _BV(PE2);               //PE2 is the general enable. Set this last
}

void wake_power(){
	PRR0 = 0x00;  //Enable MCU modules
	//Turn everything on with the general enable
        DDRE  |= _BV(PE2);  //Set this first
	PORTE &= ~_BV(PE2); //Turns everything on 

	//Set outputs
	DDRB  |= _BV(PB0) | _BV(PB1) | _BV(PB3); //Buzz switch, STX3_rx STX3_rts
	PORTB |= _BV(PB0) | _BV(PB1) | _BV(PB3); //Set lines high
	DDRD  |= _BV(PD1);              //GPS_rx
	DDRD  |= _BV(PD5) | _BV(PD6);   //RF switches
	PORTD |= _BV(PD1) | _BV(PD5) | _BV(PD6); //Set GPS_rx and sw lines high
	DDRE  |= _BV(PE3);              //Buzz enable
	PORTE &= ~(_BV(PE3));           //Disable the buzzer

	//Set inputs
	DDRB  &= ~(_BV(PB2) | _BV(PB4) | _BV(PB5));  //STX3 lines
	PORTB |= _BV(PB2) | _BV(PB4) | _BV(PB5);     //Enable internal pullups
	DDRC  &= ~(_BV(PC4) | _BV(PC5));  //SDA and SCL
	DDRC &= ~_BV(PC3);                //Battery monitor
	PORTC |= _BV(PC4) | _BV(PC5);     //Enable internal pullups
	DDRD  &= ~(_BV(PD0) | _BV(PD2));  //GPS_rx and accel_int
	PORTD |= _BV(PD0) | _BV(PD2);     //Enable internal pullups
	DDRE  &= ~(_BV(PE1));             //Hall effect switch
	PORTE |= _BV(PE1);                //Enable internal pullups
}

//FIXME: Use _delay_ms()
void short_buzz(char num){
	for(int i = 0; i<num; i++){
		int ms = 60;
		PORTE |= _BV(PE3);
		PORTB &= ~_BV(PB0);
		_delay_ms(ms);
		PORTE &= ~_BV(PE3);
		PORTB |= _BV(PB0);
		_delay_ms(ms);
	}
}

void long_buzz(char num, uint8_t length, uint8_t space){
	for(int i=0; i<num; i++){
		//FIXME
		//short_buzz(length, 0xFF, 0);
		_delay_loop_2(space);
	}
}

//A single long buzz to indicate that the device is entering a long sleep period
void buzz_sleeping(){
	long_buzz(1, 0xFF, 0);
}

//Two short buzzes to indicate that the device is beginning operation
void buzz_activation(){
	//FIXME
	//short_buzz(2, 0xFF, 0xFF);
}

//Three short buzzes to indicate that the device is inside the fail state
void buzz_fail(){
	//FIXME
	//short_buzz(3, 0xFF, 0xFF);
}

//Generate at least 1 LSB of noise for oversampling and decimation
void noise_gen_on(){
	// Set PD3 as an output
	DDRB |= _BV(PD3); 
	PRR0 &= ~_BV(PRTIM0); //Enable timer0
	// 50% duty cycle
	OCR0B = 0x7F;
	// Set fast PWM mode
	TCCR0A |= _BV(WGM01) |  _BV(WGM00);//See Pgs 139 of ATmega328PB datasheet
	TCCR0A |= _BV(COM0B1) | _BV(COM0B0); //inverting output
	TCCR0B |= _BV(CS00); //No prescaling. Start PWM
}

void noise_gen_off(){
	TCCR0B &= ~(_BV(CS00) | _BV(CS01) | _BV(CS02));
	PRR0 |= _BV(PRTIM0); //Stop timer0
}

//select internal 1.1V, external 1.8V precison, or AVCC as voltage reference
//AVCC and internal referance cannot be used if there is an external voltage on VREF pin.
void select_vref(enum vref_source vref){
	if (vref == VREF_EXTERNAL){
		DDRE  |= _BV(PE2);
		PORTE &= ~_BV(PE2);      //Turn on external refernce
		ADMUX &= ~(_BV(REFS1) | _BV(REFS0)); //REFS[1:0] = 00
	} else if (vref == VREF_INTERNAL){
		//XXX: Uncomment these if an external refence is used. Otherwise it will short
		//DDRE  |= _BV(PE2);
		//PORTE |= _BV(PE2);     //Turn off external refernce
		ADMUX |= _BV(REFS1) | _BV(REFS0);    //REFS[1:0] = 11
	} else { //Set Vref to AVCC if neither external nor internal referance is selected
		DDRE  |= _BV(PE2);
		PORTE |= _BV(PE2);
		ADMUX &= ~_BV(REFS1);   //Turn off external reference
		ADMUX |= _BV(REFS0);    //REFS[1:0] = 01
	}
}

void ADC_select_channel(enum ADC_channel channel){
	if (channel >= 0x07)
	DIDR0 &= ~_BV(channel);
	ADMUX &= ~0x0F;   //clear last four bits of ADMUX
	ADMUX |= channel; //select channel with last four bits of ADMUX
	PRR0 &= ~_BV(PRADC); //enable ADC in power reducing register
	ADCSRA |= _BV(ADPS2) | _BV(ADPS1);  //Set ADC prescaler (~114kHz)
}

uint16_t ADC_single_conversion(enum ADC_channel channel){
	ADC_select_channel(channel);
	ADCSRA &= ~_BV(ADATE); //Disable autotrigger
	ADCSRA |= _BV(ADSC) | _BV(ADEN); //Set ADSC and ADEN bits. ADSC is cleared when conversion is complete. ADEN enables ADC
	while(!(ADCSRA & _BV(ADSC))); //loop until conversion is complete
	return ADC;
}

//Start ADC in free running mode
//Discard first measurement if 1.1V internal reference is used
uint8_t ADC_free_start(enum ADC_channel channel){
	ADC_select_channel(channel);
	//Set ADSC,ADEN, and ADTS bits.
	//ADSC is cleared when conversion is complete. ADEN enables ADC. ADTS enables autotrigger
	ADCSRA |= _BV(ADSC) | _BV(ADEN) | _BV(ADATE);
	//ADC will now run until stopped. Read register ADC to get values.
	//ADSC bit goes high when conversion is complete
	//Discard first result
	while(!(ADCSRA & _BV(ADSC))); //loop until conversion is complete	
	return ADC;
}

//Turn off the ADC
void ADC_off(){
	ADCSRA &= ~(_BV(ADEN) | _BV(ADATE));
	PRR0 |= _BV(PRADC);
}

//See application note AVR121
//ADC must be in free running mode
//At least 1 LSB of noise is required for this to work
//Oversampled results can be averaged to reduce random error
int32_t ADC_oversample(uint8_t bits){
	const uint8_t adc_bits = 10; //number of bits the ADC output is
	const uint64_t num = 1<<((bits-adc_bits)*2);  //number of samples we need to take: 4^(bits-adc_bits)
	uint64_t sample = 0;
	for(int i = 0; i< num; i++){
		while(!(ADCSRA & _BV(ADSC))); //loop until conversion is complete
		int temp = ADC;
		sample += temp;                //get sample and add to oversample accumulator
	}
	sample = sample>>(bits-adc_bits);     //right shift by number of additional bits
	return sample;
}

//Set up and oversample a sensor. ADC will continue to run until told to stop.
uint16_t oversample_probe(enum ADC_channel probe_channel, uint8_t bits){
	if((probe_channel == PROBE1) || (probe_channel == PROBE2)){
		//Configure voltage divider
		DDRC  |= _BV(PC2);      //Set PC2 as output
		//PORTE &= ~_BV(PC2);   //Drive PC2 low
		PORTE |= _BV(PC2);      //Drive PC2 high
		select_vref(VREF_EXTERNAL);
	} else if (probe_channel == BATT){
		select_vref(VREF_EXTERNAL);
	} else if (probe_channel == TEMPERATURE){
		select_vref(VREF_INTERNAL);
	}
	ADC_select_channel(probe_channel);
	ADC_free_start(probe_channel);
	//TODO: Average reading to reduce randomness
	uint16_t probe_value = ADC_oversample(bits);
	return probe_value;
}

uint16_t sample_probe(enum ADC_channel probe_channel){
	if((probe_channel == PROBE1) || (probe_channel == PROBE2)){
		//Configure voltage divider
		DDRC  |= _BV(PC2);      //Set PC2 as output
		//PORTE &= ~_BV(PC2);   //Drive PC2 low
		PORTE |= _BV(PC2);      //Drive PC2 high
		select_vref(VREF_EXTERNAL);
	} else if (probe_channel == BATT){
		select_vref(VREF_INTERNAL);
	} else if (probe_channel == TEMPERATURE){
		select_vref(VREF_INTERNAL);
	}
	ADC_select_channel(probe_channel);
	volatile uint16_t probe_value = ADC_single_conversion(probe_channel);
	_delay_ms(1);
	probe_value = ADC_single_conversion(probe_channel);
	return probe_value;
}

void select_module(enum module mod){
	DDRD |= _BV(PD6);
	if(mod == STX3)
		PORTD |= _BV(PD6);
	else
		PORTD &= ~_BV(PD6);
}

//Orientation of zero or greater is considered down. Negative is up
void select_antenna(int8_t orientation){
	DDRD |= _BV(PD5);
	if ((uint8_t)orientation > 0x80 ){ //If component side is down
		PORTD |= _BV(PD5);
	}else{                 //If component side is up
		PORTD &= ~_BV(PD5);
	}
}

uint16_t TC1_timeout_period = 0;
uint16_t TC1_accum = 0;
uint16_t time_out = 0;
void TC1_timeout(const uint16_t seconds){
	//Passing UIN16_MAX disables timer
	TC1_accum = 0;
	time_out = 0;
	if (seconds != stop_timer){
		TC1_timeout_period = seconds;
		PRR0 &= ~_BV(PRTIM1);
		TCCR1B |= _BV(CS12) | _BV(CS10); //Divide clk by 1024 and start timer
		//Enable TC1 interrupt
		TIMSK1 |= _BV(OCIE1A);
		//Set timer frequency to 1Hz
		TCCR1B |= _BV(WGM12);   //Set TC1 to CTC mode (clear timer on match)
		OCR1A = 0x1BF3;         //Generate interrupt when timer reaches this value
		TIMSK1 |= _BV(OCIE1A);
		//print_str0("timer started\r\n");
	} else {
		//Stop TC1
		TCCR1B &= ~(_BV(2) | _BV(1) | _BV(0));
		TCNT1 = 0;     //Clear timer
		TC1_accum = 0; //Clear seconds counter
	}
}

void assemble_message(struct data sampled_data, char transmit_data[]){
	//Take sampled_data, and assemble it into binary array of char transmit_data[]
	char array[4];

	make_array(sampled_data.longitude,   array);
	memcpy(&transmit_data[0], &array, 4);

	make_array(sampled_data.latitude,    array);
	memcpy(&transmit_data[4], &array, 4);

	make_array(sampled_data.battery,   array);
	memcpy(&transmit_data[8], &array[2], 2);

	make_array(sampled_data.temperature,   array);
	memcpy(&transmit_data[10], &array[2], 2);

	transmit_data[12] = sampled_data.lock_time;

	transmit_data[13] = sampled_data.num_flips;
	
	make_array(sampled_data.timestamp,   array);
	memcpy(&transmit_data[14], array, 4);

	//Stuff data into the unused bits
	//transmit_data[10] |= (sampled_data.HDOP & 0xFC);
	//transmit_data[4]  |= (sampled_data.orientation & 0xF0);
}

static float poly_lat(uint8_t i, const float poly[][2]){
	return pgm_read_float(&(poly[i][0]));
}

static float poly_lng(uint8_t i, const float poly[][2]){
	return pgm_read_float(&(poly[i][1]));
}

//Return the winding number given a point and a polygon. Return 0 means point is outside
//XXX: Latitudes and longitudes are treated as cartesian coordinates. X and Y should be taken as a projection of latitude and longitude
char point_in_polygon(float lat, float lng, const float poly[][2], char vertices){
	vertices--; //The last point is the same as the first
	char wnd=0;
	for(int i=0; i<vertices; i++){
		if (poly_lng(i, poly) > lng){
			if((poly_lat(i, poly) < lat) && (poly_lat(i+1, poly) > lat)){      //Upward crossing
				wnd++;
			} else if((poly_lat(i, poly) > lat) && (poly_lat(i+1, poly) < lat)){ //Downward crossing
				wnd--;
			}
		}
	}
	return wnd;
}


static float deg2rad(float deg){
	return deg*M_PI/180;
}

//Calculate the distance between two points in the Earth
static float geographic_distance(float lat, float lng, float site_lat, float site_lng){
	/*
	//Totally crap calculation
	float a = square(lat - site_lat);
	float mean_lat = (lat-site_lat)/2;
	float b = square(cos(mean_lat)*(lng - site_lng));
	float distance = EARTH_RADIUS*sqrt(a+b);
	distance = distance*0.9;  //Fudge factor
	print_str0("\r\nDistance: ");
	print_32int0(distance);
	return distance;
	*/
	//Calculate great circle distance between device and radio site (Haversine formula)
	float a = square(sin(lat-site_lat)/2);
	float b = cos(site_lat) * cos(lat);
	float c = square(sin(lng-site_lng)/2);
	//Arcsine is not accurate on this device and inflates distance
	float distance = 2*EARTH_RADIUS*asin(sqrt(a+b*c));
	//Fudge factor. It'll be a couple of km off (and appear closer to the site), but it was already a couple off in the other direction
	distance = distance*0.95; 
	return distance;
}

char radio_astro(float lat, float lng){
	lat = deg2rad(lat);
	lng = deg2rad(lng);
	for(int i = 0; i< num_radio_50km; i++){
		float site_lat = deg2rad(poly_lat(i,radio_astro_50km));
		float site_lng = deg2rad(poly_lng(i,radio_astro_50km));

		float distance = geographic_distance(lat, lng, site_lat, site_lng);

		if (distance <= 50)
			return 1;
	}
	for(int i = 0; i< num_radio_160km; i++){
		float site_lat = deg2rad(poly_lat(i,radio_astro_160km));
		float site_lng = deg2rad(poly_lng(i,radio_astro_160km));

		float distance = geographic_distance(lat, lng, site_lat, site_lng);

		if (distance <= 160)
			return 1;
	}

	return 0;
}

//Convert integer latitude to floating point
float float_lat(int32_t lat){
	float flat = 90LL/(1L<<23);
	flat = lat*flat;
	if (flat >= 90)
		flat = flat - 180;
	return flat;
}

//Convert interger longitude to floating point
float float_lng(int32_t lng){
	float flng = 180/(1L<<23);
	flng = lng*flng;
	if (flng >= 180)
		lng = flng - 360;
	return flng;
}

//Select an appropiate (or none) channel to transmit on
enum channel choose_channel(struct data my_data){
	float lat = float_lat(my_data.latitude);
	float lng = float_lng(my_data.latitude);
	if(point_in_polygon(lat, lng, apolygon, apolygon_size)){
		if(radio_astro(lat, lng))
			return channel_C;
		return channel_A;
	} else if(point_in_polygon(lat, lng, usuda, usuda_size))
		return channel_none;
	else if(point_in_polygon(lat, lng, kashima, kashima_size))
		return channel_none;
	return channel_C;
}

//Check if the STX3 is transmiting
char is_transmiting(){
	_delay_ms(500);
	return !(PINB & _BV(PB5));  //Transmission is finished when pin goes low (return inverse)
}

void make_array(int32_t number, char array[]){
	array[0] = (0xFF000000 & number)>>24;
	array[1] = (0x00FF0000 & number)>>16;
	array[2] = (0x0000FF00 & number)>>8;
	array[3] = (0x000000FF & number);
}

void devices(uint8_t state){
	if(state){  //Turn on devices
		DDRE  |= _BV(PE2);
		PORTE &= ~_BV(PE2);
	} else {    //Turn off devices
		DDRE  |= _BV(PE2);
		PORTE |= _BV(PE2);
	}
}
