
#if !defined UART_H_
#define UART_H_


#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#if !defined USART_BAUDRATE
	#define USART_BAUDRATE 9600
#endif
#define BAUD_PRESCALE (((F_CPU/(USART_BAUDRATE*16UL)))-1)

//initialize UART0
void uart_init0();

//initialize UART1
void uart_init1();

//Test if UART0 is ready to be read
uint8_t UART0_ready();

//Test if UART1 is ready to be read
uint8_t UART1_ready();

//Read a byte from UART0
char read_UART0();

//Read a byte from UART0
char read_UART1();

//Write a single byte on UART0
char uart_putbyte0(char byte);

//Write a sinlge byte on UART1
char uart_putbyte1(char byte);

//Write and array of bytes on UART0
void print_str0(const char string[]);

//Write and array of bytes on UART1
void print_str1(const char string[]);

void print_array0(const char string[], int l);

void print_array1(const char string[], int l);

void print_64int0(int64_t my_int);

//Print a 32 bit number as an ascii hex string
void print_32int0(int32_t my_int);

//Print an 8 bit number to the uart in hex
void print_8int0(int8_t my_int);

uint8_t wait_UART0();

uint8_t wait_UART1();

#endif
