#include <time.h>
#include "config.h"
#include "GPS.h"
#include "mcu.h"
#include "uart.h"

uint16_t TC1_accum = 0;
uint16_t TC1_timeout_period = 0;
char time_out = 0;
struct{
	char message0[9];
	char message1[9];
	char message2[7];
	char message3[9+7];
} buf;
int main(){
	uart_init0();

	struct data test_data = {0};

	//Write some data
	test_data.orientation = 0xFF;
	test_data.time        = 0xFEDCBA98;
	test_data.latitude    = 0x123456;
	test_data.longitude   = 0x789ABC;
	test_data.HDOP        = 16;
	test_data.probe1      = 0x00FFFF/2;
	test_data.probe2      = 0x00FFFF/3;
	test_data.probe1      = 0;
	test_data.probe2      = 0;
	test_data.batt        = 0x0;
	test_data.temperature = 0x00004AAA;

	//Print time and location data as binary
	assemble_message(test_data, buf.message0, 0);
	for(uint8_t i =0; i< 9; i++)
		print_8int0(buf.message0[i]);
	print_str0("\r\n");
	print_str0("\r\n");
	return 1;
}
