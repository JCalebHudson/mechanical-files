//This file contains constants that we may like to have (e.g radio astonomy sites, A-polygon)
//Store constants in program space

#include <avr/pgmspace.h>
#include "constants.h"
const float apolygon[apolygon_size][2] PROGMEM = {
	//Latitude, Longitude
	{90,   -180},
	{8.73, -180},
	{8.73, -108.61},
	{8,    -103.61},
	{7.93, -99},
	{8.13, -92.31},
	{8.82, -87.67},
	{11.07, -82.78},
	{11.67, -82.37},
	{19.07, -80.28},
	{24.02, -78.57},
	{23.74, -75.76},
	{25.11, -70.09},
	{25.77, -59.73},
	{26.19, -57.08},
	{27.89, -51.15},
	{29.89, -47.09},
	{31.52, -44.45},
	{33.59, -41.98},
	{36.18, -39.39},
	{38.83, -36.79},
	{55.98, -37.17},
	{62.51, -37.32},
	{90,    -37.32},
	{90,   -180}
};

const float radio_astro_160km[num_radio_160km][2] PROGMEM = {
	{38.433056, -79.840000},
	{34.078611, -107.617778},
	{37.231667, -118.276111}
};


const float radio_astro_50km[num_radio_50km][2] PROGMEM = {
	{34.301111, -108.118611},
	{35.775000, -106.245000},
	{31.956111, -111.611667},
	{30.635000, -103.944167},
	{41.771389, -91.573889},
	{48.131389, -119.681944},
	//{17.758611, -64.584167},  //Puerto Rico is outside the A-polygon
	{19.804444, -155.458056},   //Hawaii
	{42.933611, -71.986667},
	{49.32, -119.62}
};

//Japanese exclusion zone Usuda-Nobeyama
//Four sided polygon
const float usuda[usuda_size][2] PROGMEM = {
	{36.51907, 138.4996},
	{36.23521, 137.9007},
	{35.56391, 138.3445},
	{35.85227, 138.9521},
	{36.51907, 138.4996}
};

//Japanese exclusion zone Kashima
//Five sided polygon
const float kashima[kashima_size][2] PROGMEM = {
	{35.48377, 140.4270},
	{35.49796, 141.1611},
	{36.33011, 141.2421},
	{36.43511, 140.5931},
	{36.00148, 140.2853},
	{35.48377, 140.4270}
};

const float EARTH_RADIUS_MEM PROGMEM = 6371.0; //Mean radius of the Earth in kilometers
