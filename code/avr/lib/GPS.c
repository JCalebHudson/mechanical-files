#include "GPS.h"

//TODO: store in program space
const char GGA_str[] = "GGA";
const char RMC_str[] = "RMC";
const char GGL_str[] = "GGL";
const char GSA_str[] = "GSA";
const char GSV_str[] = "GSV";
const char VTG_str[] = "VTG";

const char *message_strings[] = {
	GGA_str,
	RMC_str,
	GGL_str,
	GSA_str,
	GSV_str,
	VTG_str
};

char crc[]={0,0};

//Get the id of NMEA message sent by the GPS
void slurp_id(char GPS_message[]){
	//Loop until we have the start of the GPS message
	do{
		wait_UART0(); //wait until UART0 is ready
		GPS_message[0] = read_UART0();
	}while (GPS_message[0] != '$');

	//Get the GPS_message ID
	for (int i=1; i<6 ; i++){ 
		wait_UART0(); //wait until UART0 is ready
		GPS_message[i] = read_UART0();
	}
}

//Request a partcular NMEA message and store it in an array
char get_GPS_message(message_id id, char GPS_message[]){
	int i;
	do{
		slurp_id(GPS_message);
	}while(memcmp(message_strings[id], &GPS_message[3], 3));   //Loop until we have the requested message
	//Copy the message into an array
	for (i=7; (GPS_message[i] != '\n') && (GPS_message[i-1] != '\r'); i++){
		wait_UART0(); //wait until UART0 is ready
		GPS_message[i] = read_UART0();
		if(i > message_length)
			break;
	}

	//Return if the message is valid or not
	if (validate_message(GPS_message))
		return 1;
	else
		return 0;
}

//Process a NMEA string into struct GPS GPS_data
void process_GPS_message(char GPS_message[], struct GPS *GPS_data){
	//strcpy(GPS_message,"$GPRMC,053740.000,A,2503.6319,N,12136.0099,E,2.69,79.65,100106,,,A*53\r\n");
	//strcpy(GPS_message, "$GPGGA,053740.000,2503.6319,N,12136.0099,E,1,08,1.1,63,.8,M,15.2,M,,0000*64\r\n");
	//Identify message type and load appropriate list of fields
	const GPS_fields *ptr = 0;
	const GPS_fields GGA_array[]={id, time_field, lat, ns, lng, ew, fix, sat_num, HDOP, altitude, alt_units, geod_s, geo_units, diff_age, diff_ref_station, checksum, end};
	const GPS_fields RMC_array[]={id, time_field, status, lat, ns, lng, ew, speed, heading, date, mag_var, var_sens, mode, checksum, end};
	if(!memcmp(&GPS_message[3], message_strings[RMC], 3)){
		ptr = RMC_array;
	}
	else if(!memcmp(&GPS_message[3], message_strings[GGA], 3)){
		ptr = GGA_array;
	}

	//FIXME: Some fields could be variable length. Should find end of field before copying data
	int state_index = 0;
	for (int j = 0; j<message_length; j++){
		if(GPS_message[j] == ','){
			state_index++;  //Move to next field
			if(GPS_message[j+1] != ',')
				j++;            //Skip over field seperator (comma)

			if(ptr[state_index] == time_field)
				memcpy(&GPS_data->time_field, &GPS_message[j], 6);
			else if(ptr[state_index] == lat)
				 memcpy(&GPS_data->latitude,  &GPS_message[j], 8);
			else if(ptr[state_index] == ns)
				memcpy(&GPS_data->ns,         &GPS_message[j], 1);
			else if(ptr[state_index] == lng)
				memcpy(&GPS_data->longitude,  &GPS_message[j], 9);
			else if(ptr[state_index] == ew)
				memcpy(&GPS_data->ew,         &GPS_message[j], 1);
			else if(ptr[state_index] == date)
				memcpy(&GPS_data->date,       &GPS_message[j], 6);
			else if(ptr[state_index] == HDOP)
				memcpy(&GPS_data->HDOP,       &GPS_message[j], 4);
			 //Check if GGA or RMC has valid fix
			else if(ptr[state_index] == fix){  
				if (GPS_message[j]=='1') 
					GPS_data->fix = 1;
			}
			else if(ptr[state_index] == status){
				if(GPS_message[j]=='A')
					GPS_data->fix = 1;
			}
		}
	}

	


	//Clear the buffer
	for (int i = 0; i<message_length; i++){
		GPS_message[i] = 0;
	}
}

//Standard interger division truncates. This function returns the nearest integer
int64_t round_closest(int64_t dividend, int64_t divisor) {
    return (dividend + divisor / 2) / divisor;
}

int32_t get_lat(struct GPS GPS_data){
	//memcpy(GPS_data.latitude,"8959.9999",9);
	//GPS_data.ns='S';
	int64_t lat = 0; //We need a big variable so it doesn't overflow while we work on it. The final result is smaller
	//degrees
	lat =       (GPS_data.latitude[0] - 48)*1e8;
	lat = lat + (GPS_data.latitude[1] - 48)*1e7;
	//minutes
	lat = lat + round_closest(((GPS_data.latitude[2] - 48)*1e7),6);
	lat = lat + round_closest(((GPS_data.latitude[3] - 48)*1e6),6);
	//fractional lat (element four is a period)
	lat = lat + round_closest(((GPS_data.latitude[5] - 48)*1e5),6);
	lat = lat + round_closest(((GPS_data.latitude[6] - 48)*1e4),6);
	lat = lat + round_closest(((GPS_data.latitude[7] - 48)*1e3),6);
	lat = lat + round_closest(((GPS_data.latitude[8] - 48)*1e2),6);
	lat = lat * (1L<<28);
	lat = round_closest(lat, 90e7);

	//convert latitude to integer
	if (GPS_data.ns=='S')
		lat=-lat;
	return lat;
}

int32_t get_lng(struct GPS GPS_data){
	memcpy(GPS_data.longitude,"08415.2000",10);
	GPS_data.ew='E';
	int64_t lng = 0; //We need a big variable so it doesn't overflow while we work on it. The final result is smaller
	//degrees
	lng =       (GPS_data.longitude[0] - 48)*1e9;
	lng = lng + (GPS_data.longitude[1] - 48)*1e8;
	lng = lng + ((GPS_data.longitude[2] - 48)*1e7);
	//minutes
	lng = lng + round_closest(((GPS_data.longitude[3] - 48)*1e7),6);
	lng = lng + round_closest(((GPS_data.longitude[4] - 48)*1e6),6);
	//fractional minutes (element five is a period)
	lng = lng + round_closest(((GPS_data.longitude[6] - 48)*1e5),6);
	lng = lng + round_closest(((GPS_data.longitude[7] - 48)*1e4),6);
	lng = lng + round_closest(((GPS_data.longitude[8] - 48)*1e3),6);
	lng = lng + round_closest(((GPS_data.longitude[9] - 48)*1e2),6);

	//convert longitude to integer
	lng = lng*(1L<<29);
	lng = round_closest(lng,180e7);
	if (GPS_data.ew=='W')
		lng=-lng;
	lng &= 0x1FFFFFFF;
	return lng;
}

time_t get_timestamp(struct GPS GPS_data){
	//memcpy(&GPS_data.date, "200417",6);
	//memcpy(&GPS_data.time_field, "123456.123",9);
	struct tm start_date = {0};
	const char start_date_str[10] = START_DATE;
	const int16_t start_year  = (start_date_str[0]-48)*1000 + (start_date_str[1]-48)*100 + (start_date_str[2]-48)*10 + start_date_str[3]-48;
	const int8_t  start_month = (start_date_str[5]-48)*10   + (start_date_str[6]-48);
	const int8_t  start_mday  = (start_date_str[8]-48)*10   + (start_date_str[9]-48);
	start_date.tm_mday = start_mday;
	start_date.tm_mon  = start_month - 1;   //January is month 0
	start_date.tm_year = start_year - 1900; //Years since 1900
	const time_t start_ts = mktime(&start_date);

	struct tm GPS_time = {0};
	int16_t year    = (GPS_data.date[4] - 48)*10;              //ten years
	year            = year + (GPS_data.date[5] - 48);          //years
	int8_t month    = (GPS_data.date[2] - 48)*10;              //ten months
	month           = month + (GPS_data.date[3] - 48);         //months
	int8_t day      = (GPS_data.date[0] - 48)*10;              //ten days
	day             = day + (GPS_data.date[1] - 48);           //days

	uint8_t hours   = (GPS_data.time_field[0] - 48)*10;        //ten hours
	hours           = hours + (GPS_data.time_field[1] - 48);   //hours
	uint8_t minutes = (GPS_data.time_field[2] - 48)*10;        //ten minutes
	minutes         = minutes + (GPS_data.time_field[3] - 48); //minutes
	uint8_t seconds = (GPS_data.time_field[4] - 48)*10;        //ten seconds
	seconds         = seconds + (GPS_data.time_field[5] - 48); //seconds
	GPS_time.tm_sec  = seconds;
	GPS_time.tm_min  = minutes;
	GPS_time.tm_hour = hours;
	GPS_time.tm_mday = day;
	GPS_time.tm_mon  = month-1;            //January is month 0
	GPS_time.tm_year = (year+2000) - 1900; //years since 1900 (Assume we in year 20XX)

	time_t timestamp = mktime(&GPS_time) - start_ts; //seconds since START_DATE
	return timestamp;
}

uint8_t get_satnum(struct GPS GPS_data){
	uint8_t num=0;
	num = GPS_data.sat_num[1];
	num = num + GPS_data.sat_num[0]*10;
	return num;
}

uint8_t get_HDOP(struct GPS GPS_data){
	//XXX: Double check this
	uint8_t i;
	for(i=0; i<4; i++){
		if (GPS_data.HDOP[i] == '.')
			break;
	}
	uint8_t HDOP = 0;
	HDOP =  HDOP + (GPS_data.HDOP[i-1]-48) * 10;
	HDOP =  HDOP + (GPS_data.HDOP[i+1]-48);
	return HDOP;
}

void GPS_standby(){
	print_str0("$PMTK161,0*28\r\n");
}

void GPS_cold_restart(){
	print_str0("PMTK103*30\r\n");
}

void GPS_defaults(){
	print_str0("PMT104*37");
}

uint8_t calc_checksum(char string[]){
	uint8_t ck = string[1]; //Start at 1 because checksum does not include '$' character
	int i;
	for(i=2; i<message_length; i++){
		if (string[i] == '*') //checksum does not include '*' character
			break;
		ck ^= string[i];
	}
	return ck;
}

//Convert 1 byte into two ASCII hex digits
void crc2ascii(uint8_t cksum){
	crc[0] = ((cksum & 0xF0)>>4)+0x30;
	crc[1] = (cksum & 0x0F)+0x30;
}

uint8_t validate_message(char string[]){
	char cksum[2] = {0};
	for(int i=0; i<message_length; i++){
		if (string[i] == '*'){
			cksum[0]=string[i+1];
			cksum[1]=string[i+2];
			break;
		}
	}
	uint8_t calc = calc_checksum(string);  //calculate the checksum of the message
	uint8_t recv = (cksum[0] - 48)<<4;     //get the recieved checksum and convert ASCII to byte
	recv += cksum[1] - 48;
	if(recv == calc)                       //compare calculated and recieved checksums
		return 1;
	else{
		return 0;
	}
}

uint8_t fix_is_valid(struct GPS GPS_data){
	//Field is 0 length if no fix, and field seperator is copied to GPS_data
	if (GPS_data.latitude[0] != ',')
		return 1;
	else
		return 0;
}
