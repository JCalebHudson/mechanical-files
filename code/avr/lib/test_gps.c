#include "config.h"
#include "GPS.h"
#include "mcu.h"
#include <util/delay.h>

struct GPS GPS_data;
struct data my_data;
char GPS_message[message_length];


//Enter standby state
void test0(){
	GPS_standby();
}

//Blink light1 when a message is received
void test1(){
	wait_UART0();
	uart_putbyte1(read_UART0());
	PIND |= _BV(PD4);
}
//Blink light2 when fix is valid
void test2(){
	DDRD |= _BV(PD4);
	PORTD |= _BV(PD4);
	GPS_defaults();
	GPS_cold_restart();
	while(GPS_data.fix != '1'){
		//Loop until we have valid GGA message (GGA has fix indicator)
		while(!get_GPS_message(GGA, GPS_message)); 
		process_GPS_message(GPS_message, &GPS_data);
	}
	PORTD &= ~_BV(PD4);
}

struct GPS GPS_data;

int main(){
	wake_power();
	_delay_ms(1000);
	uart_init0();
	select_module(GPS);
	select_antenna(0xFF);
	test2();
	_delay_ms(5000);
}
