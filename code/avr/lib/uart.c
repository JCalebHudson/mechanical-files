#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include "uart.h"

//Initialize UART0
void uart_init0(){
	UCSR0B |= (1<<RXEN0)  | (1<<TXEN0);
	UCSR0C |= (1<<UCSZ00) | (1<<UCSZ01);
	UBRR0H  = (BAUD_PRESCALE >> 8);
	UBRR0L  = BAUD_PRESCALE;
}

#if defined (__AVR_ATmega328PB__)
//Initialize UART1
void uart_init1(){
	UCSR1B |= (1<<RXEN1)  | (1<<TXEN1);
	UCSR1C |= (1<<UCSZ10) | (1<<UCSZ11);
	UBRR1H  = (BAUD_PRESCALE >> 8);
	UBRR1L  = BAUD_PRESCALE;
}
#endif

//Test if UART0 is ready to be read
uint8_t UART0_ready(){
	uint8_t ready = (UCSR0A >> 7);
	return ready;
}

#if defined (__AVR_ATmega328PB__)
//Test if UART1 is ready to be read
uint8_t UART1_ready(){
	uint8_t ready = (UCSR1A >> 7);
	return ready;
}
#endif

//Read a byte from UART0
char read_UART0(){
	char byte = UDR0;
	return byte;
}

#if defined (__AVR_ATmega328PB__)
//Read a byte from UART0
char read_UART1(){
	char byte = UDR1;
	return byte;
}
#endif

//Write a single byte on UART0
char uart_putbyte0(char byte){
	while( ( UCSR0A & ( 1 << UDRE0 ) ) == 0 );
	UDR0 = byte;
	return UDR0;
}

#if defined (__AVR_ATmega328PB__)
//Write a sinlge byte on UART1
char uart_putbyte1(char byte){
	while( ( UCSR1A & ( 1 << UDRE1 ) ) == 0 );
	UDR1 = byte;
	return UDR1;
}
#endif

//Write and array of bytes on UART0
void print_str0(const char string[]){
	int i;
	for (i=0; string[i] != '\0'; i++){
		uart_putbyte0(string[i]);
	}
}

#if defined (__AVR_ATmega328PB__)
//Write and array of bytes on UART1
void print_str1(const char string[]){
	int i;
	for (i=0; string[i] != '\0'; i++){
		uart_putbyte1(string[i]);
	}
}
#endif

void print_array0(const char string[], int l){
	int i;
	for (i=0; i<l; i++){
		uart_putbyte0(string[i]);
	}
}

#if defined (__AVR_ATmega328PB__)
void print_array1(const char string[], int l){
	int i;
	for (i=0; i<l; i++){
		uart_putbyte1(string[i]);
	}
}
#endif

//Print a 64 bit number as an ascii hex string
void print_64int0(int64_t my_int){
	char string[16];
	string[0] = (my_int >> 60) & 0x0F;
	string[1] = (my_int >> 56) & 0x0F;
	string[2] = (my_int >> 52) & 0x0F;
	string[3] = (my_int >> 48) & 0x0F;
	string[4] = (my_int >> 44) & 0x0F;
	string[5] = (my_int >> 40) & 0x0F;
	string[6] = (my_int >> 36) & 0x0F;
	string[7] = (my_int >> 32) & 0xF;
	string[8] = (my_int >> 28) & 0x0F;
	string[9] = (my_int >> 24) & 0x0F;
	string[10] = (my_int >> 20) & 0x0F;
	string[11] = (my_int >> 16) & 0x0F;
	string[12] = (my_int >> 12) & 0x0F;
	string[13] = (my_int >> 8) & 0x0F;
	string[14] = (my_int >> 4) & 0x0F;
	string[15] = my_int & 0xF;
	for (int i=0; i<16; i++){
		if (i==8)
			print_str0(" ");
		char offset;
		if (string[i]>9)
			offset = 55;
		else
			offset = 48;
		uart_putbyte0(string[i]+offset);
	}
}

//Print a 32 bit number as an ascii hex string
void print_32int0(int32_t my_int){
	char string[8];
	string[0] = (my_int >> 28) & 0x0F;
	string[1] = (my_int >> 24) & 0x0F;
	string[2] = (my_int >> 20) & 0x0F;
	string[3] = (my_int >> 16) & 0x0F;
	string[4] = (my_int >> 12) & 0x0F;
	string[5] = (my_int >> 8) & 0x0F;
	string[6] = (my_int >> 4) & 0x0F;
	string[7] = my_int & 0xF;
	for (int i=0; i<8; i++){
		char offset;
		if (string[i]>9)
			offset = 55;
		else
			offset = 48;
		uart_putbyte0(string[i]+offset);
	}
}

//Print an 8 bit number to the uart in hex
void print_8int0(int8_t my_int){
	char string[2];
	string[0] = (my_int >> 4) & 0x0F;
	string[1] = my_int & 0xF;
	for (int i=0; i<2; i++){
		char offset;
		if (string[i]>9)
			offset = 55;
		else
			offset = 48;
		uart_putbyte0(string[i]+offset);
	}
}

uint8_t wait_UART0(){
	while(!UART0_ready());
	return 1;
}

#if defined (__AVR_ATmega328PB__)
uint8_t wait_UART1(){
	while(!UART1_ready());
	return 1;
}
#endif
