#include "config.h"
#include "uart.h"
#include "GPS.h"
#include "STX3.h"
#include "accel.h"
#include "mcu.h"
#include "config.h"
#include "constants.h"
#include <avr/sleep.h>
#include <avr/interrupt.h>

enum state_var state = init_state;

volatile uint32_t sleep_counter = 0; //count seconds asleep
//This happens when the watchdog timer times out
ISR(WDT_vect){
	sleep_disable();
	cli();              //disable interrupts
	MCUSR |= _BV(WDRF); //clear interrupt flag
	sleep_counter += 8; //WDT interrupts about evey 8 seconds
	wdt_reset();//reset the watchdog timer
}

volatile char active = 0;
volatile char self_test_needed = 1;
//Pinchange interrupt on PortE (The Hall effect switch)
ISR(PCINT3_vect){
	sleep_disable();
	cli();
	sleep_counter = UINT32_MAX-32; //exit sleep_time()
	self_test_needed = 1;
	active = !active;
}

extern uint16_t TC1_accum;
extern uint16_t TC1_timeout_period;
extern char time_out;
ISR(TIMER1_COMPA_vect, ISR_NOBLOCK){
	TC1_accum++;
	if(TC1_accum > TC1_timeout_period){
		if (state == init_state){
			//This should probably just reset
		} else if (state == self_state){  //The self test has hanged (failure)
			fail();
		} else {
			time_out = 1;  //Indicate that a timeout has occured
			TC1_accum = 0; //Clear the seconds counter
		}
		soft_reset();
	}
}

int main(){
	char did_reset = MCUSR & _BV(WDRF);  //Did the watchdog kill it?
	WDT_init();
	/*
	 *  INITIALIZATION STATE
	 */
	state = init_state;
	Hall_switch_init();
	sei();
	/*
	 * Sleep modes:
	 * 	SLEEP_MODE_IDLE
	 * 	SLEEP_MODE_ADC
	 * 	SLEEP_MODE_PWR_SAVE
	 * 	SLEEP_MODE_STANDBY
	 * 	SLEEP_MODE_PWR_DOWN
	 * See section 14.1 of ATmega328PB datasheet
	 */
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_bod_disable(); //BOD should should already be disabled with a fuse

	if(did_reset){
		self_test_needed = 0;
		active = 1;
		reduce_power();
		sleep_time(TRANSMISSION_INTERVAL);
	} else {
		//Wait a random interval before starting
		const char power_delay = start_delay(TRANSMISSION_INTERVAL);
		sleep_time(power_delay);
		state = self_state;
		TC1_timeout(init_timer);
		self_test();
		self_test_needed = 0; //self_test() would not exit if it failed
		buzz_sleeping();      //Buzz to indicate device is entering long sleep
	}

	while(1){
		if(active){
			if(self_test_needed){
				if (!self_test()){
					/*
					 *  SELF-TEST STATE
					 */
					state = self_state;
					TC1_timeout(init_timer);
					self_test();
				}
				//self_test() would not exit if test had failed
				self_test_needed = 0;
				buzz_activation();
			}
			TC1_timeout(init_timer);
			activate_modules();

			struct data my_data;  //Create data structure to hold everything

			/*
			 *  ACCELEROMETER STATE
			 */
			state = accel_state;
			//Start TWI0
			twi0_init();
			//Read orientation
			accel_init();
			while(!accel_ready()); //loop until the accelerometer will give a value.
			my_data.orientation = accel_Z_read();

			//START UART0
			uart_init0();
			//Get the GPS started
			select_module(GPS);
			//Select antenna
			select_antenna(my_data.orientation);

			/*
			 * SENSOR STATE
			 */
			state = sensor_state;
			//Read sensors
			my_data.temperature = sample_probe(TEMPERATURE); //~314mV at 25C. ~1mV per deg C
			my_data.battery     = sample_probe(BATT);
			ADC_off();

			/*
			 *  GPS STATE
			 */
			state = GPS_state;
			TC1_timeout(GPS_timer);
			struct GPS GPS_data;
			char GPS_message[message_length];
			while(GPS_data.fix != '1'){
				//Loop until we have valid GGA message (GGA has fix indicator)
				while(!get_GPS_message(GGA, GPS_message)); 
			}
			process_GPS_message(GPS_message, &GPS_data);
			//Loop until we have valid RMC message (RMC has date data)
			while(!get_GPS_message(RMC, GPS_message));
			process_GPS_message(GPS_message, &GPS_data);
			my_data.latitude  = get_lat(GPS_data);
			my_data.longitude = get_lng(GPS_data);
			my_data.timestamp = get_timestamp(GPS_data);
			my_data.HDOP      = get_HDOP(GPS_data);

			//Select the channel the STX3 will transmit on
			enum channel transmit_channel;
			transmit_channel = choose_channel(my_data);

			/*
			 *  STX3 STATE
			 */
			state = STX3_state;
			TC1_timeout(STX3_timer);
			if(transmit_channel != channel_none){
				//START UART1
				uart_init1();
				setup_STX3(transmit_channel);
				//Assemble message
				char transmit1[14]; //Create an array to store the binary message to be transmitted
				assemble_message(my_data, transmit1);
				//Transmit message
				select_module(STX3);
				crc16(transmit1,14);
				char stx3_response[16] = {0};
				//Send data and validate response from STX3. Send until valid response or at most 5 times
				for(char i=0; i>5; i++){
					transmit_data(transmit1, 9);
					get_response(stx3_response);
					if(valid_response(stx3_response))
						break;
				}
				//Wait for transmission to complete
				while (is_transmiting());
			}
		}
		/*
		 *  SLEEP STATE
		 */
		state = sleep_state;
		TC1_timeout(stop_timer);
		reduce_power();
		sleep_time(TRANSMISSION_INTERVAL); //sleep until next transmission interval
	}
	return 0;
}
