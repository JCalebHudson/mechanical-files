#include "config.h"
#include <util/delay.h>
#include "STX3.h"
#include "mcu.h"

ISR(BADISR_vect){
	DDRE  |= _BV(PE3);
	PORTE |= _BV(PE3);
	print_str0("BADISR\r\n");

}

void test0(){
	send_message(query_esn);
	char esn[9];
	get_response(esn);
	if (valid_response(esn)){
		DDRD  |= _BV(PD4);
		PORTD |= _BV(PD4);
	} else {
		DDRE  |= _BV(PE0);
		PORTE |= _BV(PE0);
	}
	_delay_ms(100);
	PORTE &= _BV(PE0);
	PORTD &= _BV(PD4);
}

void test1(){
	select_module(STX3);
	//Negative means component side up
	select_antenna(0xFF);
	setup_STX3(channel_A);
	char data[] = "Hello World! Number 3!\r\n";
	transmit_data(data, 25);
	char response[24];
	get_response(response);
	if(valid_response(response)){
		PORTD |= _BV(PD4);
	}else{
		short_buzz(1);
	}
	_delay_ms(250);
	DDRE  |= _BV(PE0);
	DDRD  |= _BV(PD4);
	PORTD &= ~_BV(PD4);
	PORTE &= ~_BV(PE0);
}

int main(){
	MCUSR = 0;
	_delay_ms(3000);
	wake_power();
	_delay_ms(1000);
	uart_init0();
	uart_init1();
	int i = 0;
	while(1){
		test1();
		PORTE |= _BV(PE0);
		while(is_transmiting());
		PORTE &= ~_BV(PE0);
		//Five minute delay
		_delay_ms(300000);
		i++;
	}
	return 1;
}
