#include "config.h"
#include "accel.h"
#include "mcu.h"
#include <avr/io.h>
#include <util/delay.h>

int main(){
	DDRD |= _BV(PD4);
	DDRE |= _BV(PE0) | _BV(PE3);
	PORTD &= ~_BV(PD3);
	PORTE &= ~_BV(PE0);
	wake_power();
	_delay_ms(500);
	twi0_init();
	accel_init();
	while(1){
		while(!accel_ready());
		PINE |= _BV(PE3);
		int8_t orientation = accel_Z_read();
		uart_init0();
		print_str0("Orientation: ");
		print_8int0(orientation);
		print_str0("\r\n");
		if ( orientation> 0){
			PORTD |= _BV(PD4);
			PORTE &= ~_BV(PE0);
		} else {
			PORTD &= ~_BV(PD4);
			PORTE |= _BV(PE0);
		}
	}
	return 1;
}
