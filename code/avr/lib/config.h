#if !defined CONFIG_H_
#define CONFIG_H_

/* 
 * Unit will return number of seconds (minus any leap seconds 
 * since since this date.
 * Format YYYY-MM-DD
 * Default value is "2000-01-01".
 */
#define START_DATE "2017-01-01"

/* 
 * Approximate number of seconds between transmissions.
 * Default is 300 seconds. Minimum is 300 seconds
 */
#define TRANSMISSION_INTERVAL 300

/*
 * Threshold (in millivolts) at which a low battery warning will trigger.
 * Recommend 80% of life remaining.
 * Default value is 5600 mV.
 */
#define BATT_THRESHOLD_LOW 5600

/*
 * Threshold at which the self test battery test will fail. It indicates that
 * the battery is too low to start the experiment
 */
#define BATT_THRESHOLD_HIGH 6000

/* 
 * Number of seconds to GPS will spend searching for a lock.
 * Default value is 180 seconds.
 */
#define SEARCH_TIME 20

#endif
