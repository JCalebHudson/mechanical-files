#include "uart.h"
#include "mcu.h"
#include "util/delay.h"

int main(){
	uart_init0();
	print_str0("\r\n");
	print_str0("\r\n");
	volatile int sample;
	wake_power();
	_delay_ms(30);
	sample = sample_probe(TEMPERATURE);
	_delay_ms(30);
	while(1){
		char bits = 10;
		sample = sample_probe(BATT);
		int millivolts = batt2voltage(sample, bits);
		print_str0("battery: ");
		print_32int0(millivolts);
		print_str0("\r\n");

		sample = sample_probe(TEMPERATURE);
		sample = adc2temperature(sample);
		print_str0("tempera: ");
		print_32int0(sample);
		print_str0("\r\n");
		_delay_ms(1000);
	}
	return 1;
}
