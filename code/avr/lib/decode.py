import fileinput
import datetime
import time
import calendar
import math
import ctypes

def decode_time(line):
    start_date = time.strptime("1 JAN 2017", "%d %b %Y")
    start_ts = calendar.timegm(start_date)
    #start_date = time.strptime("1 JAN 2000", "%d %b %Y") #default start date
    #start_ts = calendar.timegm(start_date)
    posix_time = int(line) + start_ts
    decoded_time = datetime.datetime.utcfromtimestamp(posix_time).strftime('%Y-%m-%dT%H:%M:%SZ')
    return decoded_time

def decode_latitude(line):
    if (line & 0x8000000):
        line = ~(line^0xFFFFFFFF)
    latitude = int(line)*(90/(2**28))
    if latitude > 90:
        latitude = latitude - 90
    return latitude

def decode_longitude(line):
    if (line & 0x10000000):
        line = line - (1<<29)
    longitude = int(line)*(180/(2**29))
    if longitude > 180:
        longitude = longitude - 360
        pass
    return longitude

def decode_HDOP(line):
    line = line/10
    return line


for line in fileinput.input():
    line = int(line,16)
    #18 byte message:    0xFFC05E9DFFE311601EF34870091101000000
    reserved1   = line & 0xE00000000000000000000000000000000000
    lng         = line & 0x1FFFFFFF0000000000000000000000000000
    orientation = line & 0x00000000F000000000000000000000000000
    lat         = line & 0x000000000FFFFFFF00000000000000000000
    reserved3   = line & 0x0000000000000000FC000000000000000000
    battery     = line & 0x000000000000000003FF0000000000000000
    HDOP        = line & 0x00000000000000000000FC00000000000000
    temperature = line & 0x0000000000000000000003FF000000000000
    lock_time   = line & 0x000000000000000000000000FF0000000000
    num_flips   = line & 0x00000000000000000000000000FF00000000
    timest      = line & 0x0000000000000000000000000000FFFFFFFF
    lng = lng>>(28*4)
    orientation = orientation>>(27*4)
    lat = lat>>(20*4)
    battery = battery>>(4*16)
    HDOP = HDOP>>(14*4 + 2)
    temperature = temperature>>(12*4)
    lock_time = lock_time>>(10*4)
    num_flips = num_flips>>(8*4)

    x = format(timest, '08X')
    y = format(lat, '08X')
    z = format(lng, '08X')
    w = format(battery, '03X')
    u = format(temperature, '03X')



    print(z);
    print(y);
    print(x);
    print(w);
    print(u);

    lat  = decode_latitude(lat)
    lng  = decode_longitude(lng)
    timest = decode_time(timest)

    #time = format(time, '08X')
    #lat  = format(lat,  '08X')
    #lng  = format(lng,  '08X')
    #iteration  = format(iteration,   '02X')
    #orientation = format(orientation, '02X')
    #lock_time   = format(lock_time,   '02X')
    
    battery = battery/1024 * 1.1 * (22+5.1)/5.1
    temperature = temperature*1e3/1024 * 1.1 - 289

    print()
    print("Time: ", timest)
    print("Latitiude: ", lat)
    print("Longitude: ",lng)
    print("Orientation: ", orientation)
    print("Battery: ",battery)
    print("HDOP: ", HDOP)
    print("Temperature: ",temperature)
    print("lock_time: ",lock_time)
    print("num_flips: ",num_flips)
    print()
    print()

    #if (line & 0x80000000):
    #    line = ~(line^0xFFFFFFFF)
    #statement = decode_time(line)
    #print(statement)


#for idx,line in enumerate(fileinput.input()):
#    line = int(line, 16)
#    # Convert to signed number
#    if (line & 0x80000000):
#        line = ~(line^0xFFFFFFFF)
#    if idx == 0:
#       time = decode_time(line)
#    elif idx ==1:
#        latitude = decode_latitude(line)
#    elif idx == 2:
#        longitude = decode_longitude(line)
#    elif idx == 3:
#        HDOP = decode_HDOP(line)

#print();
#print();
#print(time)
#print(latitude)
#print(longitude)
#print(HDOP)

