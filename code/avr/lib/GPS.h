#if !defined GPS_H_
#define GPS_H_

#include "config.h"
#include <string.h>
#include <util/delay.h>
#include <time.h>
#include <avr/pgmspace.h>
#include "uart.h"

#if !defined START_DATE
	//default epoch
	#define START_DATE "2000-01-01"
#endif

struct GPS {
	char time_field[10];
	char date[6];
	char latitude[9];
	char ew;
	char longitude[10];
	char ns;
	char HDOP[3];
	char sat_num[2];
	char GGA_checksum[2];
	char RMC_checksum[2];
	char fix;
};

//A list of all the fields a NMEA message may contain
typedef enum {
	id,
	time_field,   //UTC time
	lat,    //latitude
	ns,     //North/South indicator
	lng,    //longitude
	ew,     //East/West indicator
	fix,    //position fix indicator
	sat_num,//number of satellites used
	HDOP,   //horizontal dilution of precision
	altitude,
	alt_units,//altitude units
	geod_s, //geoid seperation
	geo_units,//geoid seperation units
	diff_age,//age of diff. corr.
	diff_ref_station,
	status,
	speed,  //speed is knots
	heading,
	date,
	mode,
	mag_var,
	var_sens,
	checksum,
	end
} GPS_fields;

typedef enum {
	GGA = 0,
	RMC = 1,
	GLL = 2,
	GSA = 3,
	GSV = 4,
	VTG = 5,
} message_id;

extern const char GGA_str[];
extern const char RMC_str[];
extern const char GGL_str[];
extern const char GSA_str[];
extern const char GSV_str[];
extern const char VTG_str[];

extern const char *message_strings[];

#define message_length 128
extern char crc[];

//Get the id of NMEA message sent by the GPS
void slurp_id(char GPS_message[]);

//Request a partcular NMEA message and store it in an array
char get_GPS_message(message_id id, char GPS_message[]);

//Process a NMEA string into struct GPS GPS_data
void process_GPS_message(char GPS_message[], struct GPS *GPS_data);

//Standard interger division truncates. This function returns the nearest integer
int64_t round_closest(int64_t dividend, int64_t divisor);

int32_t get_lat(struct GPS GPS_data);

int32_t get_lng(struct GPS GPS_data);

time_t get_timestamp(struct GPS GPS_data);

uint8_t get_satnum(struct GPS GPS_data);

uint8_t get_HDOP(struct GPS GPS_data);

void GPS_standby();

void GPS_cold_restart();

void GPS_defaults();

uint8_t calc_checksum(char string[]);

//Convert 1 byte into two ASCII hex digits
void crc2ascii(uint8_t cksum);

uint8_t validate_message(char string[]);

uint8_t fix_is_valid(struct GPS GPS_data);

#endif
