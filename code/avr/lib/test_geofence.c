#include "constants.h"
#include "uart.h"
#include "mcu.h"
#include <util/delay.h>
#include <avr/pgmspace.h>

uint16_t TC1_timeout_period = 0xFFFF;
uint16_t TC1_accum = 0;
uint16_t time_out = 0xFF;

ISR(WDT_vect){

}

ISR(BADISR_vect){

}

const float test_points[][2] = {
	{32.17561, -70.75195},  //Inside A-polygon
	{22.1874,  -47.19726},  //Out
	{1.75753,  -126.21093}, //Out
	{37.16031, -159.60937}, //In
	{28.92163,  168.39843}, //Out
	{-47.5172,  125.50781}, //Out
	{-39.09596, -104.0625}, //Out
	{54.36775,  -16.875},   //Out
	{49.38237, -44.9121},   //In
	{36.13232, 138.36282},  //Inside Usuda polygon
	{35.52384, 140.74035},  //Inside Kashima polygon
	{35.73202, -106.19247}, //Inside RAS
	{19.88297, -154.98207}, //Inside RAS (Hawaii)
	{19.80288, -155.11047}, //Inside RAS (Hawaii)
	{19.78544, -155.41191}, //Inside RAS (Hawaii) 
	{37.26968, -120.09704}, //Inside RAS (160km California)
};

int main(){
	uart_init0();
	print_str0("Start");
	print_str0("\r\n");
	int i = 10;
	int length = sizeof(test_points)/sizeof(test_points[0]);
		print_str0("\r\n\r\nA-polygon:");
		for (i = 0; i<length; i++){
			float lat = test_points[i][0];
			float lng = test_points[i][1];
			char var = point_in_polygon(lat, lng, apolygon, apolygon_size);
			print_str0("\r\nInside: ");
			uart_putbyte0(var+48);
		}
		
		print_str0("\r\n\r\nUsuda:");
		for (i = 0; i<length; i++){
			float lat = test_points[i][0];
			float lng = test_points[i][1];
			char var = point_in_polygon(lat, lng, usuda, usuda_size);
			print_str0("\r\nInside: ");
			uart_putbyte0(var+48);
		}

		print_str0("\r\n\r\nKashima:");
		for (i = 0; i<length; i++){
			float lat = test_points[i][0];
			float lng = test_points[i][1];
			char var = point_in_polygon(lat, lng, kashima, kashima_size);
			print_str0("\r\nInside: ");
			uart_putbyte0(var+48);
		}

		print_str0("\r\n\r\nRAS sites:");
		for (i = 0; i<length; i++){
			float lat = test_points[i][0];
			float lng = test_points[i][1];
			char var = radio_astro(lat, lng);
			//print_str0("\r\ni:      ");
			//uart_putbyte0(i+48);
			print_str0("\r\nInside: ");
			uart_putbyte0(var+48);
		}
	print_str0("\r\n");
	print_str0("\r\n");
	return 0;
}
