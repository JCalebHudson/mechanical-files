#include "mcu.h"
#include "GPS.h"
#include <util/delay.h>

ISR(WDT_vect){
	wdt_reset();
	sleep_counter += 8;
	MCUSR |=_BV(WDRF);
	char var1 = DDRE;
	char var2 = PORTE;
	DDRE |= _BV(PE0);
	PINE |= _BV(PE0);
	_delay_ms(250);
	PORTE = var2;
	DDRE  = var1;
}

ISR(PCINT3_vect){
	sleep_disable();
	sleep_counter = UINT32_MAX-32;
	DDRE |= _BV(PE3);
	PORTE |= _BV(PE3);
	_delay_ms(250);
	PORTE &= ~_BV(PE3);
	reduce_power();
}

ISR(BADISR_vect){}

int main(){
	WDT_init();
	Hall_switch_init();
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	uart_init0();
	GPS_standby();
	while(1){
		reduce_power();
		sleep_time(10);

		DDRD |= _BV(PD4);
		PORTD |= _BV(PD4);

		DDRE |= _BV(PE0);
		PORTE |= _BV(PE0);
		_delay_ms(250);
		PORTE &= ~_BV(PE0);
		PORTD &= ~_BV(PD4);
	}
	return 0;
}
