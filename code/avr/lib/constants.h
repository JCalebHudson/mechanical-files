//This file contains constants that we may like to have (e.g radio astonomy sites, A-polygon)
//Store constants in program space

#if !defined CONSTANTS_H_
#define CONSTANTS_H_

#include <avr/pgmspace.h>

//Polygons have an extra point because the first and last points are the same
enum polygon_size{
	apolygon_size = 25,
	usuda_size = 5,
	kashima_size = 6
};

enum num_radiosites{
	num_radio_50km  = 9,
	num_radio_160km = 3
};

extern const float apolygon[][2];

extern const float radio_astro_160km[][2];

extern const float radio_astro_50km[][2];

extern const float usuda[][2];

extern const float kashima[][2];

//Mean radius of the Earth in kilometers
extern const float EARTH_RADIUS_MEM;
#define EARTH_RADIUS pgm_read_float(&EARTH_RADIUS_MEM)

#endif
