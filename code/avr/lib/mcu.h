#if !defined MCU_H_
#define MCU_H_

#include <stdio.h>
#include <stdlib.h>
#include <avr/sleep.h>
#include <util/delay_basic.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include "accel.h"
#include "config.h"
#include "STX3.h"
#include "GPS.h"

enum state_var {
	init_state,
	accel_state,
	GPS_state,
	sensor_state,
	STX3_state,
	sleep_state,
	self_state
};

enum module{
	STX3,
	GPS
};

enum vref_source{
	VREF_EXTERNAL,
	VREF_INTERNAL,
	VREF_AVCC
};

//See section 29.9.1 (pg 321) of ATmega328PB datasheet
enum ADC_channel{
	TEMPERATURE  = 0x08, //Internal temperature sensor. 1.1V referance must be used. Pretty inaccurate. See section 29.8 (pg 319) of ATmega328PB datasheet. 
	PROBE1       = 0x00,
	PROBE2       = 0x01,
	BATT         = 0x03,
	BANDGAP      = 0x0E, //1.1V internal voltage reference. Varies with temperature and manufacture
	GND          = 0x0F
};

enum data_lengths{
	longitude_length   = 3,
	latitude_length    = 3,
	battery_length     = 2,
	temperature_length = 2,
	HDOP_length        = 1,
	timestamp_length   = 4,
};

struct data{
	int8_t  orientation;
	uint8_t num_flips;
	uint8_t lock_time;
	uint32_t timestamp;
	uint32_t latitude;
	uint32_t longitude;
	uint8_t  HDOP;
	uint16_t battery;
	uint16_t temperature;
};

enum timer_length{
	stop_timer = UINT16_MAX,
	GPS_timer  = SEARCH_TIME,
	STX3_timer = 15,
	init_timer = 10,
};


#if !defined TRANSMISSION_INTERVAL
	#define TRANSMISSION_INTERVAL 300
#endif

#if !defined BATT_THRESHOLD
	#define BATT_THRESHOLD 5600
#endif

extern uint16_t TC1_timeout_period;
extern volatile char did_reset;
volatile uint16_t elapsed_time;

//Supply power to the external parts that are normally off
void activate_modules();

//Cut off power to external parts
void deactivate_modules();

extern volatile uint32_t sleep_counter; //count seconds asleep
void sleep_time(uint32_t seconds);

//initialize the hall effect switch generate an interrupt.
//Whene PE3 changes, ISR(PCINT3_vect) will execute
void Hall_switch_init();

//WDTON fuse must be programmed to use watchdog timer
void WDT_init();

//Software reset
void soft_reset();

int16_t batt2voltage(int16_t sample, char bits);

int16_t adc2temperature(int16_t sample);

//Test that everything appears to be operating correctly
char self_test();

//Enter this state if self test fails
void fail();

//Set pins and regisiters to minimize power use
void reduce_power();

//Set pins for when device is awake
void wake_power();

void short_buzz(char num);

void long_buzz(char num, uint8_t length, uint8_t space);

//A single long buzz to indicate that the device is entering a long sleep period
void buzz_sleeping();

//Two short buzzes to indicate that the device is beginning operation
void buzz_activation();

//Three short buzzes to indicate that the device is inside the fail state
void buzz_fail();

//Generate at least 1 LSB of noise for oversampling and decimation
void noise_gen_on();

void noise_gen_off();

//select internal 1.1V, external 1.8V precison, or AVCC as voltage reference
//AVCC and internal referance cannot be used if there is an external voltage on VREF pin.
void select_vref(enum vref_source vref);

void ADC_select_channel(enum ADC_channel channel);

//Perform single conversion of ADC
//Discard first measurement if 1.1V internal reference is used
uint16_t ADC_single_conversion(enum ADC_channel channel);

//Start ADC in free running mode
//Discard first measurement if 1.1V internal reference is used
uint8_t ADC_free_start(enum ADC_channel channel);

//Turn off the ADC
void ADC_off();

//See application note AVR121
//ADC must be in free running mode (because it's faster)
//At least 1 LSB of noise is required for this to work
//Discard first measurement if 1.1V internal refernce is used
//Oversampled results can be averaged to reduce random error
int32_t ADC_oversample(uint8_t bits);

//Set up and oversample a sensor. ADC will continue to run until told to stop.
uint16_t oversample_probe(enum ADC_channel probe_channel, uint8_t bits);

uint16_t sample_probe(enum ADC_channel);

void select_module(enum module mod);

void select_antenna(int8_t orientation);

void TC1_timeout(const uint16_t seconds);

//Turn a struct data into an array of binary data ready for transmission
void assemble_message(struct data my_data, char transmit_data[]);

//Return the winding number given a point and a polygon. Return 0 means point is outside
char point_in_polygon(float lat, float lng, const float poly[][2], char vertices);

//Determine if the device is near a radio astronomy site
char radio_astro(float lat, float lng);

//Select an appropiate (or none) channel to transmit on
enum channel choose_channel(struct data my_data);

//Check if the STX3 is transmiting
char is_transmiting();

//Turn a number into an array and reverse the byte order
void make_array(int32_t number, char array[]);

#endif

