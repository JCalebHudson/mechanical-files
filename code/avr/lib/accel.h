#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include "uart.h"

#if !defined ACCEL_H_
#define ACCEL_H_

enum twi_status{
	TW_STATUS_MASK = 0xF8,
	TW_START       = 0x08,
	TW_REP_START   = 0x10,
	MT_SLA_ACK     = 0x18,
	MT_DATA_ACK    = 0x28,
	TW_MR_SLA_ACK  = 0x40
};

enum accel_values{
	ADDR       = 0x38,   //Accelerometer write address. The read addresss is this plus 1
	CTRL_REG1  = 0x2A,
	CTRL_REG2  = 0x2B,
	STATUS_REG = 0x00,   //The status register contains ZDR
	OUT_Z_MSB  = 0x05,   //Z-axis most sigifigant byte
	ZDR_MASK   = 0x04    //The ZDR bit is set when new Z-axis data is available
};
#if defined (__AVR_ATmega328P__)
#define TWDR0 TWDR
#define TWCR0 TWCR
#define TWBR0 TWBR
#define TWSR0 TWSR
#endif

/*
 *  Basic functions
 */

/* 
 * Standard TWI frequncey is 100KHz. The acceleromemter supports fast mode, with speeds up to 400kHz
 *
 * See page 274 of ATmega328PB datasheet
 * SCL frequency depends on F_CPU, TWBR, and TWSRn.TWPS[1:0] (prescaler)
 *	 SCL_freq = F_CPU/(16+2(TWBR)(prescaler))
 */
void twi0_init();

char twi0_twint();

char twi0_start();

//repeated start. Pretty much the same thing as a start.
char twi0_rstart();

void twi0_stop();

char twi0_writedata(char data);

char twi0_writeaddr(char addr);

char twi0_readaddr(char addr);

char twi0_readbyte();

/*
 *  Advanced functions
 */

char set_accel_reg(char addr, char reg, char value);

char read_accel_byte(char addr, char reg);

/*
 *  Top level stuff
 */

void accel_init();

char accel_ready();

char accel_Z_read();

#endif
