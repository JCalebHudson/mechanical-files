#include "accel.h"

/*
 *  Basic functions
 */

/* 
 * Standard TWI frequncey is 100KHz. The acceleromemter supports fast mode, with speeds up to 400kHz
 *
 * See page 274 of ATmega328PB datasheet
 * SCL frequency depends on F_CPU, TWBR, and TWSRn.TWPS[1:0] (prescaler)
 *	 SCL_freq = F_CPU/(16+2(TWBR)(prescaler))
 */
void twi0_init(){
	//Enable pullup resitors
	DDRC  &= ~ (_BV(PC4) | _BV(PC5));
	PORTC |= _BV(PC4) | _BV(PC5);
	//F_CPU is 7.327800 MHz
	//Default prescaler value is 1
	//TWSR0 &= ~(_BV(TWPS1) | _BV(TWPS0));
	//TWBR0 = 29; //~99 kHz
	TWBR0 = 72;   //100kHz when F_CPU 16 MHz

}

char twi0_twint(){
	//return 1 if TWINT has been set. This indicates that an operation has been completed
	if(TWCR0 & _BV(TWINT))
		return 1;
	else
		return 0;
}

char twi0_start(){
	TWCR0 = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
	while(!twi0_twint());
	uart_putbyte0(TWSR0);
	if((TWSR0 & TW_STATUS_MASK) != TW_START)
		return 0;
	return 1;
}

//repeated start. Pretty much the same thing as a start.
char twi0_rstart(){
	TWCR0 = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
	while(!twi0_twint());
	if((TWSR0 & TW_STATUS_MASK) != TW_REP_START)
		return 0;
	return 1;
}

void twi0_stop(){
	TWCR0 = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
	_delay_ms(1);   //Need to wait a bit after sending the stop condition. TWSR will not change
}

char twi0_writedata(char data){
	TWDR0 = data;
	TWCR0 = _BV(TWINT) | _BV(TWEN);
	while(!twi0_twint());
	if((TWSR0 & TW_STATUS_MASK) != MT_DATA_ACK)
		return 0;
	return 1;
}


char twi0_writeaddr(char addr){
	TWDR0 = addr;
	TWCR0 = _BV(TWINT) | _BV(TWEN);
	while(!twi0_twint());
	if((TWSR0 & TW_STATUS_MASK) != MT_SLA_ACK)
		return 0;
	return 1;
}

char twi0_readaddr(char addr){
	TWDR0 = addr+1;
	TWCR0 = _BV(TWINT) | _BV(TWEN);
	while(!twi0_twint());
	if((TWSR0 & TW_STATUS_MASK) != TW_MR_SLA_ACK)
		return 0;
	return 1;
}

char twi0_readbyte(){
	TWCR0 = _BV(TWINT) | _BV(TWEN);
	while (!twi0_twint());
	return TWDR0;
}


/*
 *  Advanced functions
 */

char set_accel_reg(char addr, char reg, char value){
	char nerror = 0;
	nerror = twi0_start();             //send start
	if(!nerror)
		return 0;
	nerror = twi0_writeaddr(addr);     //send address
	if(!nerror)
		return 0;
	nerror = twi0_writedata(reg);      //send register
	if(!nerror)
		return 0;
	nerror = twi0_writedata(value);    //send data
	if(!nerror)
		return 0;
	twi0_stop();                       //send stop
	return 1;
}

char read_accel_byte(char addr, char reg){
	twi0_stop();

	char nerror = 0;
	nerror = twi0_start();             //send start
	if(!nerror)
		return 0;
	nerror = twi0_writeaddr(addr);     //send address
	if(!nerror)
		return 0;
	nerror = twi0_writedata(reg);      //send register
	if(!nerror)
		return 0;
	nerror = twi0_rstart();            //send repeated start
	if(!nerror)
		return 0;
	nerror = twi0_readaddr(addr);      //send read address
	if(!nerror)
		return 0;
	char data = twi0_readbyte();       //read byte
	twi0_stop();                       //send stop
	return data;
}

/*
 *  Top level stuff
 */

void accel_init(){
	set_accel_reg(ADDR, CTRL_REG1, 0b11111000);  //Set register
	set_accel_reg(ADDR, CTRL_REG1, 0b11111001);  //Activate accelerometer
}

char accel_ready(){
	char status = read_accel_byte(ADDR, STATUS_REG);
	char ready = status & ZDR_MASK;
	if(ready)
		return 1;
	else
		return 0;
}

char accel_Z_read(){
	char output = read_accel_byte(ADDR, OUT_Z_MSB);
	return output;
}
