#include "STX3.h"

char query_esn[] = {0xAA, 0x05, 0x01, 0x50, 0xD5};	//Query ESN
char abort_transmission[] = {0xAA, 0x05, 0x03, 0x42, 0xF6};	//Abort transission
char query_setup[] = {0xAA, 0x05, 0x07, 0x66, 0xB0};	//Query setup
char send_data[] = {0xAA, 14, 0x00, '1', '2', '3', '4', '5', '6', '7', '8', '9', 0xFF, 0xFF, '\0'};
//char get_status[] = {0xAA, 0x0C, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x95, 0x29}; //STINGR command
//char example_trans[] = {0xAA, 0x0E, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xBE, 0xE8}; //Transmit example data

const char NAK[] = {0xAA, 0x05, 0x0FF, 0xA1, 0x0CB, '\0'}; //STX3 did not like what it was sent

//char esn[4]; //array to store STX3 serial number

// Calculate the two byte crc and insert it at the end of the array
int crc16(char *pData, int length) {
	length = length - 2;
	int i;
	uint16_t data, crc;
	crc = 0xFFFF;
	if (length == 0)
		return 0;
	do
	{
		data = (unsigned int)0x00FF & *pData++;
		crc = crc ^ data;
		for (i = 8; i > 0; i--)
		{
		 if (crc & 0x0001)
			 crc = (crc >> 1) ^ 0x8408;
			else
			 crc >>= 1;
		 }
	}while (--length);
	crc = ~crc;

	//place CRC in array
	pData[length] = crc;
	pData[length+1] = (uint8_t)(crc >> 8);
	crc = (crc>>8) | ((crc<<8) & 0xFF00); //Reverse byte order
	return (crc);
}

//How did the STX3 repsond? Store the response and append NULL
//Return how long the response is
//XXX: Does the STX3 respond to total nonsense? Needs timeout
void get_response(char response[]){
	wait_UART1();
	response[0] = read_UART1(); //0xAA header byte
	wait_UART1();
	response[1] = read_UART1(); //Length of response
	int length = response[1];
	for (int i = 2; i<length; i++){
		wait_UART1();
		response[i] = read_UART1();
	}
}

//Did the STX3 give a valid response?
uint8_t valid_response(char response[]){
	uint8_t i = response[1]; //Get how long the response is
	uint8_t crc1_temp = response[i-2]; // Save crc response
	uint8_t crc2_temp = response[i-1];
	uint16_t crc_resp = (crc1_temp<<8) | crc2_temp;
	uint16_t crc_calc = crc16(response, i); //calculate response crc
	response[i-2] = crc1_temp; //restore response crc after crc16 clobbers it
	response[i-1] = crc2_temp;
	if (crc_resp != crc_calc)
		return 0; //invalid response (recieved CRC does not match calculated crc)
	else if (memcmp(NAK, response, 6))
		return 1; //valid NAK
	else
		return 2; //valid and not NAK
}

//Bring RTS low and wait until CTS goes low
void rts_start(){
	while(!(PINB & _BV(PB2))); //Wait for CTS to go high
	PORTB &= ~_BV(PB1); //Bring RTS low
	while((PINB & _BV(PB2))); //Wait for CTS to go low
}

void rts_end(){
	PORTB |= _BV(PB1); //Bring RTS high
}

//Send a message to the STX3
void send_message(char message[]){
	char length = message[1];
	crc16(message, length);
	rts_start();
	print_array1(message, length);
	rts_end();
}

//Configure the STX3 the way we want, (selectable channel, 1 burst, minimal burst interval)
void setup_STX3(char channel){
	char setup[] = {0xAA, 0x0E, 0x06, 0x00, 0x00, 0x00, 0x00, channel, 0x01, 0x01, 0x02, 0x00, 0xFF, 0xFF}; //1 burst, 5-10s burst interval.
	send_message(setup);
}

//Ask the STX3 for its ESN
int32_t get_esn(){
	send_message(query_esn);
	char esn_response[9];
	get_response(esn_response);
	uint32_t esn=0;
	//Bitwise 'and' takes care of signedness weirdness
	esn += (uint32_t)(esn_response[3] & 0xFF)<<24;
	esn += (uint32_t)(esn_response[4] & 0xFF)<<16;
	esn += (uint32_t)(esn_response[5] & 0xFF)<<8;
	esn += (uint32_t)(esn_response[6] & 0xFF);
	return esn;
}

//Transmit a message
char transmit_data(char data[], char length){
	if (length > 144)   //144 bytes is maximum message length
		return 0;
	//Maximum size possible
	char transmit_command[149];
	//Messages always start with 0xAA
	transmit_command[0] = 0xAA;
	//Calculate length of command and insert into array
	transmit_command[1] = length + 5;
	//The actual command to transmit
	transmit_command[2] = 0x00;
	for(uint8_t i=0; i<length; i++)
		transmit_command[i+3] = data[i]; //Insert our data into the transmit_command array
	crc16(transmit_command, transmit_command[1]); //Calculate crc
	send_message(transmit_command);
	return 1;
}

//Return a random delay (in seconds) to wait before starting, so all units do not transmit at once
uint16_t start_delay(uint16_t max_delay){
	//Generate a random number (from ESN)
	int32_t esn = get_esn();
	srand(esn);
	uint8_t delay;
	do{
		delay = rand();
	} while(delay > max_delay);
	return delay;
}

