#include "config.h"
#include "mcu.h"
#include "GPS.h"
#include "STX3.h"
#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

struct data my_data;
struct GPS GPS_data;

ISR(WDT_vect){
	sleep_disable();
	MCUSR |= _BV(WDRF);
	sleep_counter += 8;
	wdt_reset();

	PORTD |= _BV(PD4);
	_delay_ms(250);
	PORTD &= ~_BV(PD4);
}

volatile char activate = 0;
ISR(PCINT3_vect){
	if(!(PINE & _BV(PE1))){ //If pin is low
		sleep_disable();
		wake_power();
		_delay_ms(200);
		short_buzz(3);
		sleep_counter = UINT32_MAX-32;
		//GPS_cold_restart();
		if(activate){
			cli();
			reduce_power();
			soft_reset();
		}
		activate = 1;

	}
}

extern int TC1_accum;
ISR(TIMER1_COMPA_vect, ISR_NOBLOCK){
	TC1_accum++;
}

ISR(BADISR_vect){}

int main(){
	reduce_power();
	WDT_init();
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	Hall_switch_init();
	wake_power();
	_delay_ms(1000);
	GPS_cold_restart();
	char last_orientation = 0;
	while(1){
		while(!activate){
			reduce_power();
			sleep_time(7);
		}
		wake_power();
		uart_init0();
		_delay_ms(1000);
		twi0_init();
		accel_init();
		while(!accel_ready());
		my_data.orientation = accel_Z_read();
		if(my_data.orientation != last_orientation)
			my_data.num_flips++;
		last_orientation = my_data.orientation;
		short_buzz(1);

		uart_init0();
		select_module(GPS);
		select_antenna(my_data.orientation);
		
		my_data.battery     = sample_probe(BATT);
		my_data.temperature = sample_probe(TEMPERATURE);

		PORTE |= _BV(PE0);
		TC1_accum = 0;
		//while(GPS_data.fix != '1'){
			char GPS_message[128];
			get_GPS_message(RMC, GPS_message);
			process_GPS_message(GPS_message, &GPS_data);
			get_GPS_message(GGA, GPS_message);
			process_GPS_message(GPS_message, &GPS_data);
		//}
		GPS_data.fix = '0';
		my_data.lock_time = TC1_accum;

		short_buzz(2);
		_delay_ms(250);
		PORTE &= ~_BV(PE0);
		my_data.latitude  = get_lat(GPS_data);
		my_data.longitude = get_lng(GPS_data);
		my_data.timestamp = get_timestamp(GPS_data);
		my_data.HDOP      = get_HDOP(GPS_data);

		uart_init1();
		char message[144];
		assemble_message(my_data, message);
		transmit_data(message, 18);
		short_buzz(3);

		PORTD |= _BV(PD4);
		while(is_transmiting());
		PORTD &= ~_BV(PD4);
		short_buzz(4);
		reduce_power();
		sleep_time(100);
	}
	return 1;
}
