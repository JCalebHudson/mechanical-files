#if !defined STX3_H_
#define STX3_H_

#include <string.h>
#include <util/delay.h>
#include "mcu.h"
#include "uart.h"
#include "constants.h"
#include <math.h>


#if defined TRANSMISSION_INTERVAL
	//Set 300 seconds as the minimum interval
	#if TRANSMISSION_INTERVAL < 300
		#define TRANSMISSION_INTERVAL 300
	#endif
#endif
extern char query_esn[];
extern char abort_transmission[];
extern char query_setup[];
extern char send_data[];
extern char setup[];
//extern char get_status[] = {0xAA, 0x0C, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x95, 0x29}; //STINGR command
//extern char example_trans[] = {0xAA, 0x0E, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xBE, 0xE8}; //Transmit example data
extern const char NAK[];

enum channel{
	channel_A,
	channel_B,
	channel_C,
	channel_D,
	channel_none
};

//extern struct data;

// Calculate the two byte crc and insert it at the end of the array
int crc16(char *pData, int length);

//How did the STX3 repsond? Store the response and append NULL
//Return how long the response is
//XXX: Does the STX3 respond to total nonsense? Needs timeout
void get_response(char response[]);

//Did the STX3 give a valid response?
uint8_t valid_response(char response[]);

//Bring RTS low and wait until CTS goes low
void rts_start();

void rts_end();

//Send a message to the STX3
void send_message(char message[]);

//Configure the STX3 the way we want
void setup_STX3(char channel);

//Ask the STX3 for its ESN
int32_t get_esn();

//Transmit a message
char transmit_data(char data[], char length);

//Return a random delay (in seconds) to wait before starting, so all units do not transmit at once
uint16_t start_delay(uint16_t max_delay);

#endif
