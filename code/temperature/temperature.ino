
#define T1 0.0
#define T2 60.0
#define V1 2.633
#define V2 1.819
#define TEMPERATURE_POWER A3
#define TEMPERATURE_SENSOR A2
#define TEMPERATURE_GND A1

void setup(){
	Serial.begin(9600);
}

void loop(){
	float temp = read_temperature();
	Serial.println(temp);
}

float read_temperature(){
	pinMode(TEMPERATURE_POWER, OUTPUT);
	pinMode(TEMPERATURE_SENSOR, INPUT);
	pinMode(TEMPERATURE_GND, OUTPUT);
	digitalWrite(TEMPERATURE_POWER, HIGH);
	digitalWrite(TEMPERATURE_GND, LOW);
    int analog_value = analogRead(TEMPERATURE_SENSOR);
    float voltage = analog_value * (3.3140 / 1024.0); //calculate voltage
    /* The output of the temperature sensor can be approximated
    * linearly over a range of temperatures T1 to T2:
    *
    * V-V1 = (V2-V1)/(T2-T1) * (T-T1)
    *
    * V = (V2-V1)/(T2-T1) * (T-T1) + V1
    *
    * T = (V-V1) * (T2-T1)/(V2-V1) + T1
    *
    * Beware integer arithmatic
    */
    float temperature = ((voltage-V1) * ((T2-T1)/(V2-V1))) + T1;
    return temperature;
}
